
using GR
include("../ssbeziertoolsjl/beziertools.jl")
using SSBezierTools

tmin = 0.
tmax = 1.

t = tmin:0.001:tmax
p = 3
GR.figure()
for a = 1:p+1
    b = [SSBezierTools.DelBernsteinBasis(a,p,t[i],dmin=tmin,dmax=tmax) for i = 1:length(t)]
    GR.plot(t,b)
    GR.hold(true)
end
GR.hold(false)
GR.ylim([-3,3])
