
using PyPlot
include("../igadynexp.jl")

#1./3  2./3

#=
Note about creating a Bezier Curve, where knot vector is strictly 0s and 1s:
    Four things have to be consistent with each other:
        degree
        spans
        control points
        knot vector.
    These are the rules:
        degree = p
        length(control_pts) = n
        n >= p+1 (for knot_v has just 0s and 1s, n == p+1)
        spans = n .- 1
        length(knot_v) = p+n+1
        multiplicity(knot_v[1]) == multiplicity(knot_v[end])
        multiplicity(knot_v[1]), multiplicity(knot_v[end]) > max(multiplicity of interior) <- ???
=#

dim_p = 1
dim_s = 2
p = 3
n = 5
c_pts = [[0,0] [0,1] [1,1] [2,0] [2.5,2]]
knot_v = [0,0,0,0,.5,1,1,1,1]
weights = ones(size(c_pts)[2])
geometry = Nurbs( dim_p, dim_s, [p], Array[knot_v], [n-1], c_pts, weights )
X,t,h = draw(geometry,500)

using PyPlot

#Original nurbs spline
figure()
scatter(X[1,:],X[2,:])
scatter(geometry.control_pts[1,:],geometry.control_pts[2,:],color="r")
title("Original nurbs spline")
cpts_span = maximum(c_pts) - minimum(c_pts)
xlim([minimum(c_pts)-0.1*cpts_span,maximum(c_pts)+0.1*cpts_span])
ylim([minimum(c_pts)-0.1*cpts_span,maximum(c_pts)+0.1*cpts_span])
