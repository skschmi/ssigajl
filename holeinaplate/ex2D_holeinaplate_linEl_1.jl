# Example - 2D IGA
# Basic 2D linear elastic problem in IGA

# Plate with a hole in the center (Circle center)

# LINEAR ELEMENTS -- First, hand it a clean NURBS, and specify the
#                    resolution of linear elements desired.


include("../sslinearelastic.jl")
using SSLinearElastic
using SSIGA2D
import SSIGA2D.IGAGrid2D
import SSIGA2D.delBasis
import SSIGA2D.Solve
import SSIGA2D.GetStrainDispMatB
import SSIGA2D.solution
import SSNurbsTools.Nurbs
import SSNurbsTools.draw
import SSNurbsTools.basis
import SSNurbsTools.Jacobian
import SSNurbsTools.domain_X
import SSNurbsTools.domain_dXdxi
import SSNurbsTools.domain_dXdsdxi
import SSGaussQuad.GaussQuadIntegrate
import SSGaussQuad.NumGaussPts
import SSLinearElastic.compute_integral_strain_energy
using LinearAlgebra




##############
##############
### Exact Geometry
##############
##############

# Exact-geometry set-up
dim_p = 2
dim_s = 2
p = [2,2]
n = [4,3]
span = n .- 1
knot_v = Array[[0., 0, 0, 0.5, 1, 1, 1],[0., 0, 0, 1, 1, 1]]
nel = [2,1]

# Init the control points
c_pts = Array{Float64}(undef,dim_s,n[1],n[2])
# Set the control points
dom_w = dom_h = 100.0
c_pts[:,1,1] = [0.75*dom_w, 0.0]
c_pts[:,2,1] = [0.75*dom_w, 0.103553*dom_h]
c_pts[:,3,1] = [0.896447*dom_w, 0.25*dom_h]
c_pts[:,4,1] = [dom_w, 0.25*dom_h]
c_pts[:,1,2] = [0.375*dom_w,0.0]
c_pts[:,2,2] = [0.375*dom_w,0.175*dom_h]
c_pts[:,3,2] = [0.825*dom_w,0.625*dom_h]
c_pts[:,4,2] = [dom_w,0.625*dom_h]
c_pts[:,1,3] = [0.0, 0.0]
c_pts[:,2,3] = [0.0, dom_h]
c_pts[:,3,3] = [0.0, dom_h]
c_pts[:,4,3] = [dom_w,dom_h]

# Init the weights
weights = Array{Array{Float64}}(undef,2)
weights[1] = Array{Float64}(undef,4)
weights[2] = Array{Float64}(undef,3)
# Set the weights
# First dir
weights[1][1] = 1.
weights[1][2] = 0.8535533905932737
weights[1][3] = 0.8535533905932737
weights[1][4] = 1.
# Second dir
weights[2][1] = 1.
weights[2][2] = 1.
weights[2][3] = 1.

# Create the exact geometry
exact_geometry = Nurbs(dim_p, dim_s, p, knot_v, span, c_pts, weights)





##############
##############
### Linear-elements Geometry
##############
##############

function hasDesignParam(index, n)
    # Bottom edge is constrained in the y-dir
    if( index[1] == 2 && index[2] == 1 )
        return false
    end
    # Right edge is constrained in the x-dir
    if( index[1] == 1 && index[2] == n[1] )
        return false
    end
    # Left edge
    if( index[1] == 1 && index[2] <= div(n[1],2)+1 && index[3] == n[2] )
        return false
    end
    # Top edge
    if( index[1] == 2 && index[2] >= div(n[1],2)+1 && index[3] == n[2] )
        return false
    end
    # Everything else does have a design parameter
    return true
end

# Linear geometry set-up
dim_p = 2
dim_s = 2
p = [1,1]
n = [11,11]
span = n .- 1
knot_v = Array[[0.;collect(linspace(0.,1.,n[1]));1.],
               [0.;collect(linspace(0.,1.,n[2]));1.]]
nel = copy(span)

# Compute the linear-element mesh
X,t,h = draw(exact_geometry,n)

# Create the linear mesh control points
fe_nodes = reshape(X,dim_s,n[1],n[2])

# Turn them into functions of the design params
# Step 1: Figure out how many design params to have
design_params_count = 0
for i = 1:dim_p
    for j = 1:n[1]
        for k = 1:n[2]
            if(hasDesignParam([i,j,k],n))
                design_params_count += 1
            end
        end
    end
end
s0 = zeros(design_params_count)

#Step 2: Create the array of control point functions
s_index = 1
fe_cpf = Array{Function}(undef,size(fe_nodes))
for i = 1:dim_p
    for j = 1:n[1]
        for k = 1:n[2]
            if(hasDesignParam([i,j,k],n))
                let
                    global func
                    const const_index = s_index
                    func = function(s)
                        return s[const_index]
                    end
                end
                fe_cpf[i,j,k] = func
                s0[s_index] = fe_nodes[i,j,k]
                s_index += 1
            else
                let
                    global func
                    const val = fe_nodes[i,j,k]
                    func = function(s)
                        return val
                    end
                end
                fe_cpf[i,j,k] = func
            end
        end
    end
end

# Create the linear mesh weights (all ones)
fe_weights = Array{Array{Float64}}(undef,dim_p)
for i = 1:dim_p
    fe_weights[i] = ones(n[i])
end





##############
##############
### Init and Solve
##############
##############

dirichletConstraint =
    function(self::IGAGrid2D,e::Array{Int64},a::Array{Int64},dofi::Int64)
        # bottom side (left side of xi) restricted in the y-dir
        if(e[1] == 1 && a[1] == 1 && dofi == 2)
            return true,0.0
        end
        # right side (right side of xi) restricted in the x-dir
        if(e[1] == self.nelements[1] && a[1] == self.nnodesperel[1] && dofi == 1)
            return true,0.0
        end
        return false,0.0
    end
#

neumannBCForElementFace =
    function(self::IGAGrid2D,e::Array{Int64},face::Tuple{Int64,Int64},xi::Array{Float64})
        # This returns the tractions for the specified face of the element, at position xi
        # Paramters:
        # e: Element index
        # face[1]: Parameter direction: 1={xi=-1,xi=1} faces.  2={eta=-1,eta=1} faces
        # face[2]: bottom or top face:  1={xi=-1 face} 2={xi=1 face}
        # xi: The xi,eta coordinates along the face we are looking at
        # Returns:
        # x_dir_traction, y_dir_traction

        # Left side
        if( e[2] == self.nelements[2] &&
            e[1] <= self.nelements[1]/2 &&
            face[1] == 2 &&
            face[2] == 2)

            return -2.5,0.0
        end
        # Top side
        if( e[2] == self.nelements[2] &&
            e[1] > self.nelements[1]/2 &&
            face[1] == 2 &&
            face[2] == 2)

            return 0.0,2.5
        end
        return 0.0,0.0
    end
#

approx_geometry = Nurbs(dim_p, dim_s, p, knot_v, span, fe_cpf, fe_weights; s=s0)
YoungsModE = 210.0
PoissonsRatNu = 0.3
ndofpernode = 2
self = IGAGrid2D(approx_geometry,
                nel,
                ndofpernode,
                YoungsModE,
                PoissonsRatNu,
                dirichletConstraint,
                neumannBCForElementFace,
                SSLinearElastic.neumannBCDsForElementFace_Zeros,
                SSLinearElastic.bodyForceForElement_Zeros,
                SSLinearElastic.FextForElement,
                SSLinearElastic.FextDsForElement,
                SSLinearElastic.NForElement,
                SSLinearElastic.NDsForElement,
                SSLinearElastic.consistentTangentForElement,
                SSLinearElastic.consistentTangentDsForElement;
                nloadsteps=0,
                runtimeoutput=true)

#####
#=
Solve(self)

str_en = dot(self.Fext[:],self.d[:])
@show str_en
material_volume = SSIGA2D.compute_volume(self)
@show material_volume
=#






##############
##############
### Plot, Visualize
##############
##############

using PyPlot; plt = PyPlot;
closeplots = false


p1 = nothing
p2 = nothing
p3 = nothing
p4 = nothing
p5 = nothing
plt.figure()
res = [51,51]
X,t,h = draw(self.mesh,res)
p1 = plt.scatter(X[1,:],X[2,:],color="b")
plt.hold(true)
p2 = plt.scatter(self.mesh.control_pts[1,:,:],self.mesh.control_pts[2,:,:],color="r")
plt.hold(false)
#plt.xlim(-(7/16)*dom_w,(23/16)*dom_w)
#plt.ylim(-(1/4)*dom_h,(5/4)*dom_h)
plt.xlim(-(7/16)*dom_w,(28/16)*dom_w)
plt.ylim(-(1/16)*dom_h,(6.5/4)*dom_h)
plt.xlabel("x")
plt.ylabel("y")
plt.title("Geometry - Linear Elements")
plt.hold(true)
for j = 1:n[1]
    for k = 1:n[2]
        if( hasDesignParam([1,j,k],n) )
            p3 = plt.scatter(self.mesh.control_pts[1,j,k],self.mesh.control_pts[2,j,k],color="c")
        end
        if( hasDesignParam([2,j,k],n) )
            p4 = plt.scatter(self.mesh.control_pts[1,j,k],self.mesh.control_pts[2,j,k],color="g")
        end
        if( hasDesignParam([1,j,k],n) && hasDesignParam([2,j,k],n) )
            p5 = plt.scatter(self.mesh.control_pts[1,j,k],self.mesh.control_pts[2,j,k],color="y")
        end
    end
end
plt.hold(false)
plt.legend(handles=[p1,p2,p3,p4,p5],labels=["Body",
                                            "Fixed Node",
                                            "Design Variable Node, Free in x-direction",
                                            "Design Variable Node, Free in y-direction",
                                            "Design Variable Node, Free in both x-,y-directions"])
plt.savefig("plots_ignore/plot_2Dprob_geom02.pdf")
if(closeplots)
    plt.close()
end



#=
res_per_element = [div(100,nel[1]),div(100,nel[2])]

X,Y,F = solution(self,1,res_per_element)
plt.figure()
plt.pcolor(X,Y,F)
plt.colorbar()
plt.title("disp_x")
plt.savefig("plots_ignore/plot_2Dprob_disp01.pdf")
if(closeplots)
    plt.close()
end

X,Y,F = solution(self,2,res_per_element)
plt.figure()
plt.pcolor(X,Y,F)
plt.colorbar()
plt.title("disp_y")
plt.savefig("plots_ignore/plot_2Dprob_disp02.pdf")
if(closeplots)
    plt.close()
end

X,Y,Fx = solution(self,1,res_per_element)
X,Y,Fy = solution(self,2,res_per_element)
normF = zeros(size(Fx))
for i = 1:length(Fx[:])
    normF[i] = sqrt(Fx[i]^2 + Fy[i]^2)
end
plt.figure()
plt.pcolor(X,Y,normF)
plt.colorbar()
plt.title("disp_norm")
plt.savefig("plots_ignore/plot_2Dprob_disp03.pdf")
if(closeplots)
    plt.close()
end

=#
