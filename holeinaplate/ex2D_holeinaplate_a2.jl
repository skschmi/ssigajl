# Example - 2D IGA
# Basic 2D linear elastic problem in IGA

# Plate with a hole in the center (diamond center; flat-edge)

include("../sslinearelastic.jl")
using .SSLinearElastic
import .SSLinearElastic.compute_integral_strain_energy
using .SSLinearElastic.SSIGA2D
import .SSLinearElastic.SSIGA2D.IGAGrid2D
import .SSLinearElastic.SSIGA2D.delBasis
import .SSLinearElastic.SSIGA2D.Solve
import .SSLinearElastic.SSIGA2D.GetStrainDispMatB
import .SSLinearElastic.SSIGA2D.solution
import .SSLinearElastic.SSIGA2D.SSNurbsTools.Nurbs
import .SSLinearElastic.SSIGA2D.SSNurbsTools.draw
import .SSLinearElastic.SSIGA2D.SSNurbsTools.basis
import .SSLinearElastic.SSIGA2D.SSNurbsTools.Jacobian
import .SSLinearElastic.SSIGA2D.SSNurbsTools.domain_X
import .SSLinearElastic.SSIGA2D.SSNurbsTools.domain_dXdxi
import .SSLinearElastic.SSIGA2D.SSNurbsTools.domain_dXdsdxi
import .SSLinearElastic.SSIGA2D.SSGaussQuad.GaussQuadIntegrate
import .SSLinearElastic.SSIGA2D.SSGaussQuad.NumGaussPts
using LinearAlgebra


dirichletConstraint =
    function(self::IGAGrid2D,e::Array{Int64},a::Array{Int64},dofi::Int64)
        # bottom side (left side of xi) restricted in the y-dir
        if(e[1] == 1 && a[1] == 1 && dofi == 2)
            return true,0.0
        end
        # right side (right side of xi) restricted in the x-dir
        if(e[1] == self.nelements[1] && a[1] == self.nnodesperel[1] && dofi == 1)
            return true,0.0
        end
        return false,0.0
    end

neumannBCForElementFace =
    function(self::IGAGrid2D,e::Array{Int64},face::Tuple{Int64,Int64},xi::Array{Float64})
        # This returns the tractions for the specified face of the element, at position xi
        # Paramters:
        # e: Element index
        # face[1]: Parameter direction: 1={xi=-1,xi=1} faces.  2={eta=-1,eta=1} faces
        # face[2]: bottom or top face:  1={xi=-1 face} 2={xi=1 face}
        # xi: The xi,eta coordinates along the face we are looking at
        # Returns:
        # x_dir_traction, y_dir_traction

        # Left side
        if( e[2] == self.nelements[2] &&
            e[1] <= self.nelements[1]/2 &&
            face[1] == 2 &&
            face[2] == 2)

            return -2.5,0.0
        end
        # Top side
        if( e[2] == self.nelements[2] &&
            e[1] > self.nelements[1]/2 &&
            face[1] == 2 &&
            face[2] == 2)

            return 0.0,2.5
        end
        return 0.0,0.0
    end

# Problem set-up
dim_p = 2
dim_s = 2
p = [2,2]
n = [4,3]
span = n .- 1
knot_v = Array[[0., 0, 0, 0.5, 1, 1, 1],[0., 0, 0, 1, 1, 1]]
YoungsModE = 210.0
PoissonsRatNu = 0.3
nel = [2,1]
ndofpernode = 2


dom_w = dom_h = 100.0
r = 0.25
a = sqrt(pi*r^2/2)

#c_pts = zeros(dim_s,n[1],n[2])
c_pts = Array{Function}(undef,dim_s,n[1],n[2])

#### c_pts[:,1,1] = [3.0, 0.0]
c_pts[1,1,1] = function(s)
    return (1.0-a)*dom_w
end
c_pts[2,1,1] = function(s)
    return 0.0
end
#### c_pts[:,2,1] = [3.0, 0.3]
c_pts[1,2,1] = function(s)
    return ((1.0-a) + 0.1 - 0.1*s[1])*dom_w
end
c_pts[2,2,1] = function(s)
    return (0.1 + 0.1*s[1])*dom_h
end
#### c_pts[:,3,1] = [3.7, 1.0]
c_pts[1,3,1] = function(s)
    return (1.0 - 0.1 - 0.1*s[1])*dom_w
end
c_pts[2,3,1] = function(s)
    return (a - 0.1 + 0.1*s[1])*dom_h
end
#### c_pts[:,4,1] = [4.0, 1.0]
c_pts[1,4,1] = function(s)
    return dom_w
end
c_pts[2,4,1] = function(s)
    return a*dom_h
end


#### c_pts[:,1,2] = [1.5, 0.0]
c_pts[1,1,2] = function(s)
    return 0.375*dom_w
end
c_pts[2,1,2] = function(s)
    return 0.0
end
#### c_pts[:,2,2] = [1.5, 0.7]
c_pts[1,2,2] = function(s)
    return 0.375*dom_w
end
c_pts[2,2,2] = function(s)
    return 0.175*dom_h
end
#### c_pts[:,3,2] = [3.3, 2.5]
c_pts[1,3,2] = function(s)
    return 0.825*dom_w
end
c_pts[2,3,2] = function(s)
    return 0.625*dom_h
end
#### c_pts[:,4,2] = [4.0, 2.5]
c_pts[1,4,2] = function(s)
    return dom_w
end
c_pts[2,4,2] = function(s)
    return 0.625*dom_h
end


#### c_pts[:,1,3] = [0.0, 0.0]
c_pts[1,1,3] = function(s)
    return 0.0
end
c_pts[2,1,3] = function(s)
    return 0.0
end
#### c_pts[:,2,3] = [0.0, 4.0]
c_pts[1,2,3] = function(s)
    return 0.0
end
c_pts[2,2,3] = function(s)
    return dom_h
end
#### c_pts[:,3,3] = [0.0, 4.0]
c_pts[1,3,3] = function(s)
    return 0.0
end
c_pts[2,3,3] = function(s)
    return dom_h
end
#### c_pts[:,4,3] = [4.0, 4.0]
c_pts[1,4,3] = function(s)
    return dom_w
end
c_pts[2,4,3] = function(s)
    return dom_h
end


# Init the weights
weights = Array{Array{Function}}(undef,2)
weights[1] = Array{Function}(undef,4)
weights[2] = Array{Function}(undef,3)
# Set the weights
# First dir
weights[1][1] = function(s) 1. end
weights[1][2] = function(s)
            return 1.
        end
weights[1][3] = function(s)
            return 1.
        end
weights[1][4] = function(s) 1. end
# Second dir
weights[2][1] = function(s) 1. end
weights[2][2] = function(s) 1. end
weights[2][3] = function(s) 1. end


shape_p = Array{Float64}([0.0])

geometry = Nurbs(dim_p, dim_s, p, knot_v, span, c_pts, weights; s=shape_p)
self = IGAGrid2D(geometry,
                nel,
                ndofpernode,
                YoungsModE,
                PoissonsRatNu,
                dirichletConstraint,
                neumannBCForElementFace,
                SSLinearElastic.neumannBCDsForElementFace_Zeros,
                SSLinearElastic.bodyForceForElement_Zeros,
                SSLinearElastic.FextForElement,
                SSLinearElastic.FextDsForElement,
                SSLinearElastic.NForElement,
                SSLinearElastic.NDsForElement,
                SSLinearElastic.consistentTangentForElement,
                SSLinearElastic.consistentTangentDsForElement;
                nloadsteps=0,
                runtimeoutput=false)
#####
Solve(self)

str_en = dot(self.Fext[:],self.d[:])
@show str_en
material_volume = SSIGA2D.compute_volume(self)
@show material_volume

using PyPlot; plt = PyPlot;
closeplots = false

plt.figure()

res = [80,80]
X,t,h = draw(self.mesh,res)
plt.scatter(X[1,:],X[2,:],color="b")
plt.scatter(self.mesh.control_pts[1,:,:],self.mesh.control_pts[2,:,:],color="r")
plt.xlim(-(7/16)*dom_w,(23/16)*dom_w)
plt.ylim(-(1/4)*dom_h,(5/4)*dom_h)
plt.xlabel("x")
plt.ylabel("y")
plt.title("Geometry")
plt.savefig("plots_ignore/plot_2Dprob_geom02.pdf")
if(closeplots)
    plt.close()
end

X,Y,F = solution(self,1,[50,50])
plt.figure()
plt.pcolor(X,Y,F)
plt.colorbar()
plt.title("disp_x")
plt.savefig("plots_ignore/plot_2Dprob_disp01.pdf")
if(closeplots)
    plt.close()
end

X,Y,F = solution(self,2,[50,50])
plt.figure()
plt.pcolor(X,Y,F)
plt.colorbar()
plt.title("disp_y")
plt.savefig("plots_ignore/plot_2Dprob_disp02.pdf")
if(closeplots)
    plt.close()
end

X,Y,Fx = solution(self,1,[50,50])
X,Y,Fy = solution(self,2,[50,50])
normF = zeros(size(Fx))
for i = 1:length(Fx[:])
    normF[i] = sqrt(Fx[i]^2 + Fy[i]^2)
end
plt.figure()
plt.pcolor(X,Y,normF)
plt.colorbar()
plt.title("disp_norm")
plt.savefig("plots_ignore/plot_2Dprob_disp03.pdf")
if(closeplots)
    plt.close()
end

X,Y,F = solution(self,1,[50,50],self.Fext)
plt.figure()
plt.pcolor(X,Y,F)
plt.colorbar()
plt.title("f_x")
#plt.savefig("plots_ignore/plot_2Dprob_fvec01.pdf")
if(closeplots)
    plt.close()
end

X,Y,F = solution(self,2,[50,50],self.Fext)
plt.figure()
plt.pcolor(X,Y,F)
plt.colorbar()
plt.title("f_y")
#plt.savefig("plots_ignore/plot_2Dprob_fvec02.pdf")
if(closeplots)
    plt.close()
end
