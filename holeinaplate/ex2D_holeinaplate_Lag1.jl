# Example - 2D IGA
# Basic 2D linear elastic problem in IGA

# Plate with a hole in the center.

# Show that the lagrantian is zero when we compute the correct solution
# We plug in the correct solution and compute the Lagrangian (from lagrange multpliers)

include("../sslinearelastic.jl")
include("../ssoptalgorithms.jl")

using .SSLinearElastic

using .SSLinearElastic.SSIGA2D
import .SSLinearElastic.SSIGA2D.IGAGrid2D
import .SSLinearElastic.SSIGA2D.delBasis
import .SSLinearElastic.SSIGA2D.Solve
import .SSLinearElastic.SSIGA2D.GetStrainDispMatB
import .SSLinearElastic.SSIGA2D.solution

import .SSLinearElastic.SSIGA2D.SSNurbsTools.Nurbs
import .SSLinearElastic.SSIGA2D.SSNurbsTools.draw
import .SSLinearElastic.SSIGA2D.SSNurbsTools.basis
import .SSLinearElastic.SSIGA2D.SSNurbsTools.Jacobian
import .SSLinearElastic.SSIGA2D.SSNurbsTools.domain_X
import .SSLinearElastic.SSIGA2D.SSNurbsTools.domain_dXdxi
import .SSLinearElastic.SSIGA2D.SSNurbsTools.domain_dXdsdxi

import .SSLinearElastic.SSIGA2D.SSGaussQuad.GaussQuadIntegrate
import .SSLinearElastic.SSIGA2D.SSGaussQuad.NumGaussPts

using .SSGradientAscent

using LinearAlgebra

dirichletConstraint =
    function(self::IGAGrid2D,e::Array{Int64},a::Array{Int64},dofi::Int64)
        # bottom side (left side of xi) restricted in the y-dir
        if(e[1] == 1 && a[1] == 1 && dofi == 2)
            return true,0.0
        end
        # right side (right side of xi) restricted in the x-dir
        if(e[1] == self.nelements[1] && a[1] == self.nnodesperel[1] && dofi == 1)
            return true,0.0
        end
        return false,0.0
    end

neumannBCForElementFace =
    function(self::IGAGrid2D,e::Array{Int64},face::Tuple{Int64,Int64},xi::Array{Float64})
        # This returns the tractions for the specified face of the element, at position xi
        # Paramters:
        # e: Element index
        # face[1]: Parameter direction: 1={xi=-1,xi=1} faces.  2={eta=-1,eta=1} faces
        # face[2]: bottom or top face:  1={xi=-1 face} 2={xi=1 face}
        # xi: The xi,eta coordinates along the face we are looking at
        # Returns:
        # x_dir_traction, y_dir_traction

        # Left side
        if( e[2] == self.nelements[2] &&
            e[1] <= self.nelements[1]/2 &&
            face[1] == 2 &&
            face[2] == 2)

            return -2.5,0.0
        end
        # Top side
        if( e[2] == self.nelements[2] &&
            e[1] > self.nelements[1]/2 &&
            face[1] == 2 &&
            face[2] == 2)

            return 0.0,2.5
        end
        return 0.0,0.0
    end


neumannBCDsForElementFace =
    function(self::IGAGrid2D,e::Array{Int64},face::Tuple{Int64,Int64},xi::Array{Float64},s_dir::Int64)
        # This returns the tractions for the specified face of the element, at position xi
        # Paramters:
        # e: Element index
        # face[1]: Parameter direction: 1={xi=-1,xi=1} faces.  2={eta=-1,eta=1} faces
        # face[2]: bottom or top face:  1={xi=-1 face} 2={xi=1 face}
        # xi: The xi,eta coordinates along the face we are looking at
        # Returns:
        # x_dir_traction, y_dir_traction
        return 0.0,0.0
    end


dim_p = 2
dim_s = 2
p = [2,2]
n = [4,3]
span = n .- 1

knot_v = Array[[0., 0, 0, 0.5, 1, 1, 1],[0., 0, 0, 1, 1, 1]]

dom_w = 100.0
dom_h = 100.0

#c_pts = zeros(dim_s,n[1],n[2])
c_pts = Array{Function}(undef,dim_s,n[1],n[2])

#### c_pts[:,1,1] = [3.0, 0.0]
c_pts[1,1,1] = function(s)
    return s[1]
end
c_pts[2,1,1] = function(s)
    0.0
end
#### c_pts[:,2,1] = [3.0, 0.3]
c_pts[1,2,1] = function(s)
    return s[2]
end
c_pts[2,2,1] = function(s)
    return s[3]
end
#### c_pts[:,3,1] = [3.7, 1.0]
c_pts[1,3,1] = function(s)
    return s[4]
end
c_pts[2,3,1] = function(s)
    return s[5]
end
#### c_pts[:,4,1] = [4.0, 1.0]
c_pts[1,4,1] = function(s)
    return dom_w
end
c_pts[2,4,1] = function(s)
    return s[6]
end


#### c_pts[:,1,2] = [1.5, 0.0]
c_pts[1,1,2] = function(s)
    return 0.375*dom_w
end
c_pts[2,1,2] = function(s)
    return 0.0
end
#### c_pts[:,2,2] = [1.5, 0.7]
c_pts[1,2,2] = function(s)
    return 0.375*dom_w
end
c_pts[2,2,2] = function(s)
    return 0.175*dom_h
end
#### c_pts[:,3,2] = [3.3, 2.5]
c_pts[1,3,2] = function(s)
    return 0.825*dom_w
end
c_pts[2,3,2] = function(s)
    return 0.625*dom_h
end
#### c_pts[:,4,2] = [4.0, 2.5]
c_pts[1,4,2] = function(s)
    return dom_w
end
c_pts[2,4,2] = function(s)
    return 0.625*dom_h
end


#### c_pts[:,1,3] = [0.0, 0.0]
c_pts[1,1,3] = function(s)
    return 0.0
end
c_pts[2,1,3] = function(s)
    return 0.0
end
#### c_pts[:,2,3] = [0.0, 4.0]
c_pts[1,2,3] = function(s)
    return 0.0
end
c_pts[2,2,3] = function(s)
    return dom_h
end
#### c_pts[:,3,3] = [0.0, 4.0]
c_pts[1,3,3] = function(s)
    return 0.0
end
c_pts[2,3,3] = function(s)
    return dom_h
end
#### c_pts[:,4,3] = [4.0, 4.0]
c_pts[1,4,3] = function(s)
    return dom_w
end
c_pts[2,4,3] = function(s)
    return dom_h
end


# Init the weights
weights = Array{Array{Function}}(undef,2)
weights[1] = Array{Function}(undef,4)
weights[2] = Array{Function}(undef,3)
# Set the weights
# First dir
weights[1][1] = function(s)
    return 1.
end
weights[1][2] = function(s)
    #return 1.
    return s[7]
end
weights[1][3] = function(s)
    #return 1.
    return s[8]
end
weights[1][4] = function(s)
    return 1.
end
# Second dir
weights[2][1] = function(s) 1. end
weights[2][2] = function(s) 1. end
weights[2][3] = function(s) 1. end

shape_p = Array{Float64}([0.75*dom_w,
                          0.75*dom_w,
                          0.103553*dom_h,
                          0.896447*dom_w,
                          0.25*dom_h,
                          0.25*dom_h,
                          0.8535533905932737,
                          0.8535533905932737])
geometry = Nurbs(dim_p, dim_s, p, knot_v, span, c_pts, weights; s=shape_p)
YoungsModE = 210.0
PoissonsRatNu = 0.3
nel = [2,1]
ndofpernode = 2
self = IGAGrid2D(geometry,
                nel,
                ndofpernode,
                YoungsModE,
                PoissonsRatNu,
                dirichletConstraint,
                neumannBCForElementFace,
                neumannBCDsForElementFace,
                SSLinearElastic.bodyForceForElement_Zeros,
                SSLinearElastic.FextForElement,
                SSLinearElastic.FextDsForElement,
                SSLinearElastic.NForElement,
                SSLinearElastic.NDsForElement,
                SSLinearElastic.consistentTangentForElement,
                SSLinearElastic.consistentTangentDsForElement;
                nloadsteps=0,
                runtimeoutput=false)

#####

Solve(self)

str_energy = dot(self.Fext[:],self.d[:])
str_energy_ds = Array{Float64}([dot(self.Fext[:],self.d_ds[jj][:])+dot(self.Fext_ds[jj][:],self.d[:]) for jj=1:self.mesh.ndesignvars])
material_volume = SSIGA2D.compute_volume(self)
material_volume_ds = SSIGA2D.compute_volume_ds(self)

# The correct volume
vol_const = 9509.126147876592

# Find the value of lambda that minimizes the lagrangian
lambda_collect = collect(0.22:0.000001:0.36)
NORM_GRADL = zeros(length(lambda_collect))
for i = 1:length(lambda_collect)
    lambda = lambda_collect[i]
    g = material_volume - vol_const
    g_ds = material_volume_ds
    f = str_energy
    f_ds = str_energy_ds
    gradL = zeros(self.mesh.ndesignvars+1)
    gradL[1:end-1] = f_ds + lambda * g_ds
    gradL[end] = g
    norm_gradL = norm(gradL)
    println("norm_gradL: ", norm_gradL)
    NORM_GRADL[i] = norm_gradL
end

println("When lambda = 0.2855")
lambda = 0.2855
g = 0.0
g_ds = material_volume_ds
f = str_energy
f_ds = str_energy_ds
gradL = zeros(self.mesh.ndesignvars+1)
gradL[1:end-1] = f_ds + lambda * g_ds
gradL[end] = g
@show gradL

using PyPlot; plt = PyPlot;
closeplots = false

plt.figure()
plt.plot(lambda_collect,NORM_GRADL)
plt.title("Norm(grad_L) as function of lambda")
plt.xlabel("lambda")
plt.ylabel("norm(grad_L)")


plt.figure()
res = [80,80]
X,t,h = draw(self.mesh,res)
plt.scatter(X[1,:],X[2,:],color="b")
plt.hold(true)
plt.scatter(self.mesh.control_pts[1,:,:],self.mesh.control_pts[2,:,:],color="r")
plt.hold(false)
plt.xlim(-(7/16)*dom_w,(23/16)*dom_w)
plt.ylim(-(1/4)*dom_h,(5/4)*dom_h)
plt.xlabel("x")
plt.ylabel("y")
plt.title("Geometry")


#=
res = [80,80]
X,t,h = draw(self.mesh,res)
plt.figure()
plt.scatter(X[1,:],X[2,:],color="b")
plt.hold(true)
plt.scatter(self.mesh.control_pts[1,:,:],self.mesh.control_pts[2,:,:],color="r")
plt.hold(false)
plt.xlim(-(7/16)*dom_w,(23/16)*dom_w)
plt.ylim(-(1/4)*dom_h,(5/4)*dom_h)
plt.xlabel("x")
plt.ylabel("y")
plt.title("Optimized Geometry")


X,Y,F = solution(self,1,[50,50])
plt.figure()
plt.pcolor(X,Y,F)
plt.colorbar()
plt.title("disp_x")
plt.savefig("plots_ignore/plot_2Dprob_disp01.pdf")
if(closeplots)
    plt.close()
end

X,Y,F = solution(self,2,[50,50])
plt.figure()
plt.pcolor(X,Y,F)
plt.colorbar()
plt.title("disp_y")
plt.savefig("plots_ignore/plot_2Dprob_disp02.pdf")
if(closeplots)
    plt.close()
end

X,Y,Fx = solution(self,1,[50,50])
X,Y,Fy = solution(self,2,[50,50])
normF = zeros(size(Fx))
for i = 1:length(Fx[:])
    normF[i] = sqrt(Fx[i]^2 + Fy[i]^2)
end
plt.figure()
plt.pcolor(X,Y,normF)
plt.colorbar()
plt.title("disp_norm")
plt.savefig("plots_ignore/plot_2Dprob_disp03.pdf")
if(closeplots)
    plt.close()
end
=#
