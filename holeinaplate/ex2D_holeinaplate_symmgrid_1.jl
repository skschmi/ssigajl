# Example - 2D IGA
# Basic 2D linear elastic problem in IGA

# Plate with a hole in the center.
#  ---> Symmetric along the diagonal, only 3 without the weights
#  ---> Making plot to show strain energy as function of s[1],s[2].  s[3] is
#          constrained on s[1] and s[2] to maintain volume

include("../sslinearelastic.jl")
using SSLinearElastic
using SSIGA2D
import SSIGA2D.IGAGrid2D
import SSIGA2D.delBasis
import SSIGA2D.Solve
import SSIGA2D.GetStrainDispMatB
import SSIGA2D.solution
import SSNurbsTools.Nurbs
import SSNurbsTools.draw
import SSNurbsTools.basis
import SSNurbsTools.Jacobian
import SSNurbsTools.domain_X
import SSNurbsTools.domain_dXdxi
import SSNurbsTools.domain_dXdsdxi
import SSGaussQuad.GaussQuadIntegrate
import SSGaussQuad.GaussQuadTable
import SSGaussQuad.NumGaussPts
using LinearAlgebra

dirichletConstraint =
    function(self::IGAGrid2D,e::Array{Int64},a::Array{Int64},dofi::Int64)
        # bottom side (left side of xi) restricted in the y-dir
        if(e[1] == 1 && a[1] == 1 && dofi == 2)
            return true,0.0
        end
        # right side (right side of xi) restricted in the x-dir
        if(e[1] == self.nelements[1] && a[1] == self.nnodesperel[1] && dofi == 1)
            return true,0.0
        end
        return false,0.0
    end

neumannBCForElementFace =
    function(self::IGAGrid2D,e::Array{Int64},face::Tuple{Int64,Int64},xi::Array{Float64})
        # This returns the tractions for the specified face of the element, at position xi
        # Paramters:
        # e: Element index
        # face[1]: Parameter direction: 1={xi=-1,xi=1} faces.  2={eta=-1,eta=1} faces
        # face[2]: bottom or top face:  1={xi=-1 face} 2={xi=1 face}
        # xi: The xi,eta coordinates along the face we are looking at
        # Returns:
        # x_dir_traction, y_dir_traction

        # Left side
        if( e[2] == self.nelements[2] &&
            e[1] <= self.nelements[1]/2 &&
            face[1] == 2 &&
            face[2] == 2)

            return -2.5,0.0
        end
        # Top side
        if( e[2] == self.nelements[2] &&
            e[1] > self.nelements[1]/2 &&
            face[1] == 2 &&
            face[2] == 2)

            return 0.0,2.5
        end
        return 0.0,0.0
    end

# Problem set-up
dim_p = 2
dim_s = 2
p = [2,2]
n = [4,3]
span = n .- 1
knot_v = Array[[0., 0, 0, 0.5, 1, 1, 1],[0., 0, 0, 1, 1, 1]]
YoungsModE = 210.0
PoissonsRatNu = 0.3
#nel = [2,1]
nel = [4,4]
ndofpernode = 2
dom_w = 100.0
dom_h = 100.0

#c_pts = zeros(dim_s,n[1],n[2])
c_pts = Array{Function}(undef,dim_s,n[1],n[2])

#### c_pts[:,1,1] = [3.0, 0.0]
c_pts[1,1,1] = function(s)
    return s[1]
end
c_pts[2,1,1] = function(s)
    0.0
end
#### c_pts[:,2,1] = [3.0, 0.3]
c_pts[1,2,1] = function(s)
    return s[2]
end
c_pts[2,2,1] = function(s)
    return s[3]
end
#### c_pts[:,3,1] = [3.7, 1.0]
c_pts[1,3,1] = function(s)
    return dom_w-s[3]
end
c_pts[2,3,1] = function(s)
    return dom_h-s[2]
end
#### c_pts[:,4,1] = [4.0, 1.0]
c_pts[1,4,1] = function(s)
    return dom_w
end
c_pts[2,4,1] = function(s)
    return dom_h-s[1]
end


#### c_pts[:,1,2] = [1.5, 0.0]
c_pts[1,1,2] = function(s)
    return 0.375*dom_w
end
c_pts[2,1,2] = function(s)
    return 0.0
end
#### c_pts[:,2,2] = [1.5, 0.7]
c_pts[1,2,2] = function(s)
    return 0.375*dom_w
end
c_pts[2,2,2] = function(s)
    return 0.175*dom_h
end
#### c_pts[:,3,2] = [3.3, 2.5]
c_pts[1,3,2] = function(s)
    return 0.825*dom_w
end
c_pts[2,3,2] = function(s)
    return 0.625*dom_h
end
#### c_pts[:,4,2] = [4.0, 2.5]
c_pts[1,4,2] = function(s)
    return dom_w
end
c_pts[2,4,2] = function(s)
    return 0.625*dom_h
end


#### c_pts[:,1,3] = [0.0, 0.0]
c_pts[1,1,3] = function(s)
    return 0.0
end
c_pts[2,1,3] = function(s)
    return 0.0
end
#### c_pts[:,2,3] = [0.0, 4.0]
c_pts[1,2,3] = function(s)
    return 0.0
end
c_pts[2,2,3] = function(s)
    return dom_h
end
#### c_pts[:,3,3] = [0.0, 4.0]
c_pts[1,3,3] = function(s)
    return 0.0
end
c_pts[2,3,3] = function(s)
    return dom_h
end
#### c_pts[:,4,3] = [4.0, 4.0]
c_pts[1,4,3] = function(s)
    return dom_w
end
c_pts[2,4,3] = function(s)
    return dom_h
end


# Init the weights
weights = Array{Array{Function}}(undef,2)
weights[1] = Array{Function}(undef,4)
weights[2] = Array{Function}(undef,3)
# Set the weights
# First dir
weights[1][1] = function(s)
    return 1.
end
weights[1][2] = function(s)
    return (1 + (1/sqrt(2)))/2
end
weights[1][3] = function(s)
    return (1 + (1/sqrt(2)))/2
end
weights[1][4] = function(s)
    return 1.
end
# Second dir
weights[2][1] = function(s) 1. end
weights[2][2] = function(s) 1. end
weights[2][3] = function(s) 1. end

self = nothing

function create_geometry_and_solve(shape_params)
    global self
    self = IGAGrid2D(Nurbs(dim_p, dim_s, p, knot_v, span, c_pts, weights; s=shape_params),
                    nel,
                    ndofpernode,
                    YoungsModE,
                    PoissonsRatNu,
                    dirichletConstraint,
                    neumannBCForElementFace,
                    SSLinearElastic.neumannBCDsForElementFace_Zeros,
                    SSLinearElastic.bodyForceForElement_Zeros,
                    SSLinearElastic.FextForElement,
                    SSLinearElastic.FextDsForElement,
                    SSLinearElastic.NForElement,
                    SSLinearElastic.NDsForElement,
                    SSLinearElastic.consistentTangentForElement,
                    SSLinearElastic.consistentTangentDsForElement;
                    nloadsteps=0,
                    max_poly_degree=maximum(p)+2,
                    runtimeoutput=false)
    #####
    Solve(self)
    return self
end

function just_create_nurbs_only(shape_params)
    geom = Nurbs(dim_p, dim_s, p, knot_v, span, c_pts, weights; s=shape_params)
    return geom
end




function compute_nurbs_volume(self::Nurbs)
    # Number of gauss points
    ngp = NumGaussPts( (maximum(self.degree)+1)^2 )
    GQT = GaussQuadTable(ngp,ngp)

    volume = 0.0
    # Loop over elements
    for e1 = 1:self.nknotspans[1]
        for e2 = 1:self.nknotspans[2]
            jacobianf = function(params)
                jac = Jacobian(self,[e1,e2],params)
                return det(jac)
            end
            f = function(params)
                R = basis(self,[e1,e2],params)
                val = 0.0
                for i = 1:length(R)
                    val += R[i]
                end
                return val
            end
            # Integrate over the element
            volume += GaussQuadIntegrate(f,GQT,jacobianf)
        end
    end
    return volume
end



######################
######################
######################
######################
######################
######################
######################
######################
######################
######################



function find_s1(s2,s3,vol_const)
    s0 = zeros(3)
    s0[2] = s2
    s0[3] = s3
    start_s1 = (0.50*dom_w)
    end_s1 = (0.9*dom_w)
    println("Finding s_1 for constant volume...")
    println("Note: vol_const = $vol_const")
    h = 0.02*dom_w
    while (h > 1e-10)
        #println("h = $h")
        new_start_s1 = nothing
        new_end_s1 = nothing
        for s_1 = start_s1:h:end_s1
            s0[1] = s_1
            geom = just_create_nurbs_only(s0)
            material_volume = compute_nurbs_volume(geom)
            if( material_volume < vol_const )
                new_start_s1 = s_1
            end
            if( material_volume > vol_const )
                new_end_s1 = s_1
                break
            end
        end
        start_s1 = new_start_s1
        end_s1 = new_end_s1
        h = h / 2
    end
    s0[1] = (start_s1 + end_s1)/2
    geom = just_create_nurbs_only(s0)
    material_volume = compute_nurbs_volume(geom)
    println("Found s1: ",s0[1])
    println("material_volume: ",material_volume)
    return s0[1]
end


vol_const = dom_w*dom_h - (pi*(dom_w/4)*(dom_h/4))/4  #9509.126147876592
s0 = zeros(3)

#s0[2] = 0.75*dom_w
#s0[3] = 0.103553*dom_h

#h = 0.01
h = 0.005
#h = 0.0016
s2 = collect(0.73*dom_w:(h*dom_w):0.77*dom_w)
s3 = collect(0.08*dom_h:(h*dom_h):0.12*dom_h)
STREN = zeros(length(s2),length(s3))
S1 = zeros(length(s2),length(s3))

for s2_i = 1:length(s2)
    for s3_i = 1:length(s3)
        println("-------")
        @show s2_i, s3_i
        s0[2] = s2[s2_i]
        s0[3] = s3[s3_i]
        s1_found = find_s1(s0[2],s0[3],vol_const)
        s0[1] = s1_found
        self = create_geometry_and_solve(s0)
        str_energy = dot(self.Fext[:],self.d[:])
        @show str_energy
        STREN[s2_i,s3_i] = str_energy
        S1[s2_i,s3_i] = s1_found
        println("-------")
    end
end

################################
###  Plotting the Solution   ###
################################

S2 = Array{Float64,2}([s2[i] for i = 1:length(s2), j = 1:length(s3)])
S3 = Array{Float64,2}([s3[j] for i = 1:length(s2), j = 1:length(s3)])

using PyPlot; plt = PyPlot;
plt.figure()
plt.pcolor(S2,S3,STREN)
plt.colorbar()
plt.title("Strain Energy as function of (s2,s3)")
plt.xlabel("s2")
plt.ylabel("s3")
plt.savefig("plots_ignore/plot_STREN_fs2s3.pdf")

plt.figure()
plt.pcolor(S2,S3,S1)
plt.colorbar()
plt.title("s1 parameter as function of (s2,s3)")
plt.xlabel("s2")
plt.ylabel("s3")
plt.savefig("plots_ignore/plot_S1_fs2s3.pdf")

# Save the data to disk using numpy
using PyCall
@pyimport numpy
numpy.savetxt("plots_ignore/data_STREN.txt",numpy.array(STREN))
numpy.savetxt("plots_ignore/data_S1.txt",numpy.array(S1))
numpy.savetxt("plots_ignore/data_S2.txt",numpy.array(S2))
numpy.savetxt("plots_ignore/data_S3.txt",numpy.array(S3))
