# Example - 2D IGA
# Basic 2D linear elastic problem in IGA

# Plate with a hole in the center (one shape parameter, carefully constructed)

include("../sslinearelastic.jl")
using .SSLinearElastic
using .SSLinearElastic.SSIGA2D
import .SSLinearElastic.SSIGA2D.IGAGrid2D
import .SSLinearElastic.SSIGA2D.delBasis
import .SSLinearElastic.SSIGA2D.Solve
import .SSLinearElastic.SSIGA2D.GetStrainDispMatB
import .SSLinearElastic.SSIGA2D.solution
import .SSLinearElastic.SSIGA2D.SSNurbsTools.Nurbs
import .SSLinearElastic.SSIGA2D.SSNurbsTools.draw
import .SSLinearElastic.SSIGA2D.SSNurbsTools.basis
import .SSLinearElastic.SSIGA2D.SSNurbsTools.Jacobian
import .SSLinearElastic.SSIGA2D.SSNurbsTools.domain_X
import .SSLinearElastic.SSIGA2D.SSNurbsTools.domain_dXdxi
import .SSLinearElastic.SSIGA2D.SSNurbsTools.domain_dXdsdxi
import .SSLinearElastic.SSIGA2D.SSGaussQuad.GaussQuadIntegrate
import .SSLinearElastic.SSIGA2D.SSGaussQuad.NumGaussPts
using LinearAlgebra
using PyPlot; plt = PyPlot;


#=
optimizationConstraint_ds =
    function(self::IGAGrid2D)
        # For this problem, the optimization constraint is that the
        # material volume must remain constant.
        # In this function, we compute the derivative of the volume with
        # respect to the design paramters (s).
        return compute_volume_ds(self)
    end
=#

dirichletConstraint =
    function(self::IGAGrid2D,e::Array{Int64},a::Array{Int64},dofi::Int64)
        # bottom side (left side of xi) restricted in the y-dir
        if(e[1] == 1 && a[1] == 1 && dofi == 2)
            return true,0.0
        end
        # right side (right side of xi) restricted in the x-dir
        if(e[1] == self.nelements[1] && a[1] == self.nnodesperel[1] && dofi == 1)
            return true,0.0
        end
        return false,0.0
    end

neumannBCForElementFace =
    function(self::IGAGrid2D,e::Array{Int64},face::Tuple{Int64,Int64},xi::Array{Float64})
        # This returns the tractions for the specified face of the element, at position xi
        # Paramters:
        # e: Element index
        # face[1]: Parameter direction: 1={xi=-1,xi=1} faces.  2={eta=-1,eta=1} faces
        # face[2]: bottom or top face:  1={xi=-1 face} 2={xi=1 face}
        # xi: The xi,eta coordinates along the face we are looking at
        # Returns:
        # x_dir_traction, y_dir_traction

        # Left side
        if( e[2] == self.nelements[2] &&
            e[1] <= self.nelements[1]/2 &&
            face[1] == 2 &&
            face[2] == 2)

            return -2.5,0.0
        end
        # Top side
        if( e[2] == self.nelements[2] &&
            e[1] > self.nelements[1]/2 &&
            face[1] == 2 &&
            face[2] == 2)

            return 0.0,2.5
        end
        return 0.0,0.0
    end

# Problem set-up
dim_p = 2
dim_s = 2
p = [2,2]
n = [4,3]
span = n .- 1
knot_v = Array[[0., 0, 0, 0.5, 1, 1, 1],[0., 0, 0, 1, 1, 1]]
YoungsModE = 210.0
PoissonsRatNu = 0.3
nel = [2,1]
ndofpernode = 2


dom_w = 100.0
dom_h = 100.0

a = 0.103553
b = (1/4)*sqrt(pi/2)-(1/4)
x0 = (3+16*a - 4*b)/(4*(1+4*a))
y0 = x0 - ((3/4)-b)
c = a - y0
d = x0 - (3/4)

#c_pts = zeros(dim_s,n[1],n[2])
c_pts = Array{Function}(undef,dim_s,n[1],n[2])

#### c_pts[:,1,1] = [3.0, 0.0]
c_pts[1,1,1] = function(s)
    return ((3/4) - b + b*s[1])*dom_w
end
c_pts[2,1,1] = function(s)
    return 0.0*dom_h
end
#### c_pts[:,2,1] = [3.0, 0.3]
c_pts[1,2,1] = function(s)
    return ((3/4) + d - d*s[1])*dom_w
end
c_pts[2,2,1] = function(s)
    return (a - c + c*s[1])*dom_h
end
#### c_pts[:,3,1] = [3.7, 1.0]
c_pts[1,3,1] = function(s)
    return (1-a+c-c*s[1])*dom_w
end
c_pts[2,3,1] = function(s)
    return ((1/4)-d+d*s[1])*dom_h
end
#### c_pts[:,4,1] = [4.0, 1.0]
c_pts[1,4,1] = function(s)
    return (1.0)*dom_w
end
c_pts[2,4,1] = function(s)
    return ((1/4)+b-b*s[1])*dom_h
end

#### c_pts[:,1,2] = [1.5, 0.0]
c_pts[1,1,2] = function(s)
    return 0.375*dom_w
end
c_pts[2,1,2] = function(s)
    return 0.0
end
#### c_pts[:,2,2] = [1.5, 0.7]
c_pts[1,2,2] = function(s)
    return 0.375*dom_w
end
c_pts[2,2,2] = function(s)
    return 0.175*dom_h
end
#### c_pts[:,3,2] = [3.3, 2.5]
c_pts[1,3,2] = function(s)
    return 0.825*dom_w
end
c_pts[2,3,2] = function(s)
    return 0.625*dom_h
end
#### c_pts[:,4,2] = [4.0, 2.5]
c_pts[1,4,2] = function(s)
    return dom_w
end
c_pts[2,4,2] = function(s)
    return 0.625*dom_h
end

#### c_pts[:,1,3] = [0.0, 0.0]
c_pts[1,1,3] = function(s)
    return 0.0
end
c_pts[2,1,3] = function(s)
    return 0.0
end
#### c_pts[:,2,3] = [0.0, 4.0]
c_pts[1,2,3] = function(s)
    return 0.0
end
c_pts[2,2,3] = function(s)
    return dom_h
end
#### c_pts[:,3,3] = [0.0, 4.0]
c_pts[1,3,3] = function(s)
    return 0.0
end
c_pts[2,3,3] = function(s)
    return dom_h
end
#### c_pts[:,4,3] = [4.0, 4.0]
c_pts[1,4,3] = function(s)
    return dom_w
end
c_pts[2,4,3] = function(s)
    return dom_h
end

# Init the weights
weights = Array{Array{Function}}(undef,2)
weights[1] = Array{Function}(undef,4)
weights[2] = Array{Function}(undef,3)
# Set the weights
# First dir
weights[1][1] = function(s) 1. end
weights[1][2] = function(s)
            return (0.8535533905932737 - 1.0)*s[1] + 1.0
        end
weights[1][3] = function(s)
            return (0.8535533905932737 - 1.0)*s[1] + 1.0
        end
weights[1][4] = function(s) 1. end
# Second dir
weights[2][1] = function(s) 1. end
weights[2][2] = function(s) 1. end
weights[2][3] = function(s) 1. end


start_shape_p = 0.0
end_shape_p = 2.0

geometry = Nurbs(dim_p, dim_s, p, knot_v, span, c_pts, weights; s=Array{Float64}([start_shape_p]))
self = IGAGrid2D(geometry,
                nel,
                ndofpernode,
                YoungsModE,
                PoissonsRatNu,
                dirichletConstraint,
                neumannBCForElementFace,
                SSLinearElastic.neumannBCDsForElementFace_Zeros,
                SSLinearElastic.bodyForceForElement_Zeros,
                SSLinearElastic.FextForElement,
                SSLinearElastic.FextDsForElement,
                SSLinearElastic.NForElement,
                SSLinearElastic.NDsForElement,
                SSLinearElastic.consistentTangentForElement,
                SSLinearElastic.consistentTangentDsForElement;
                nloadsteps=0,
                runtimeoutput=false)
#####
Solve(self)

plt.figure()
res = [80,80]
X,t,h = draw(self.mesh,res)
plt.scatter(X[1,:],X[2,:],color="b")
plt.scatter(self.mesh.control_pts[1,:,:],self.mesh.control_pts[2,:,:],color="r")
plt.xlim(-(7/16)*dom_w,(23/16)*dom_w)
plt.ylim(-(1/4)*dom_h,(5/4)*dom_h)
plt.xlabel("x")
plt.ylabel("y")
plt.title("Start Geometry")



##########
# Build Plot of Strain Energy as function of s
##########

self = nothing
norm_dfds = 0.
str_energy = 0.
str_energy_ds = 0.
values = collect(start_shape_p:0.01:end_shape_p)

INTSTREN = zeros(size(values))
STREN = zeros(size(values))
STREN_DS = zeros(size(values))
VOL = zeros(size(values))
VOL_DS = zeros(size(values))

for loop_i = 1:length(values)

    global geometry
    global self
    global str_energy
    global str_energy_ds
    global norm_dfds
    global res
    global X
    global t
    global h

    shape_p = Array{Float64}([values[loop_i]])

    geometry = Nurbs(dim_p, dim_s, p, knot_v, span, c_pts, weights; s=shape_p)
    self = IGAGrid2D(geometry,
                    nel,
                    ndofpernode,
                    YoungsModE,
                    PoissonsRatNu,
                    dirichletConstraint,
                    neumannBCForElementFace,
                    SSLinearElastic.neumannBCDsForElementFace_Zeros,
                    SSLinearElastic.bodyForceForElement_Zeros,
                    SSLinearElastic.FextForElement,
                    SSLinearElastic.FextDsForElement,
                    SSLinearElastic.NForElement,
                    SSLinearElastic.NDsForElement,
                    SSLinearElastic.consistentTangentForElement,
                    SSLinearElastic.consistentTangentDsForElement;
                    nloadsteps=0,
                    runtimeoutput=false)
    #####
    Solve(self)
    int_str_energy = SSLinearElastic.compute_integral_strain_energy(self)
    str_energy = dot(self.Fext[:],self.d[:])
    str_energy_ds = Array{Float64}([dot(self.Fext[:],self.d_ds[jj][:])+dot(self.Fext_ds[jj][:],self.d[:]) for jj=1:self.mesh.ndesignvars])
    norm_dfds = norm(str_energy_ds)
    material_volume = SSIGA2D.compute_volume(self)
    material_volume_ds = SSIGA2D.compute_volume_ds(self)[1]
    GC.gc()
    INTSTREN[loop_i] = int_str_energy
    STREN[loop_i] = str_energy
    STREN_DS[loop_i] = str_energy_ds[1]
    VOL[loop_i] = material_volume
    VOL_DS[loop_i] = material_volume_ds

    println("----------")
    println("loop_i: ",loop_i)
    println("  shape_p: ", shape_p)
    println("  norm_dfds: ", norm_dfds)
    println("  int_str_energy: ", int_str_energy)
    println("  str_energy: ", str_energy)
    println("  str_energy_ds: ",str_energy_ds)
    println("  mat_vol: ",material_volume)
    println("  mat_vol_ds: ",material_volume_ds)
    println("----------")

    if(shape_p[1] == 1.0)
        plt.figure()
        res = [80,80]
        X,t,h = draw(self.mesh,res)
        plt.scatter(X[1,:],X[2,:],color="b")
        plt.scatter(self.mesh.control_pts[1,:,:],self.mesh.control_pts[2,:,:],color="r")
        plt.xlim(-(7/16)*dom_w,(23/16)*dom_w)
        plt.ylim(-(1/4)*dom_h,(5/4)*dom_h)
        plt.xlabel("x")
        plt.ylabel("y")
        plt.title("Middle Geometry (s=1.0)")
    end

end

plt.figure()
plt.plot(collect(values),INTSTREN)
plt.legend(["Strain Energy (Integral)"])
plt.xlabel("Design parameter (s)")
plt.ylabel("Strain Energy (Integral)")
plt.title("Strain Energy (Integral) vs. Design Parameter (s)")

plt.figure()
plt.plot(collect(values),STREN)
plt.legend(["Strain Energy"])
plt.xlabel("Design parameter (s)")
plt.ylabel("Strain Energy")
plt.title("Strain Energy vs. Design Parameter (s)")

plt.figure()
plt.plot(collect(values),STREN_DS,"r")
plt.legend(["d_STREN/ds"])
plt.xlabel("Design parameter (s)")
plt.ylabel("d_STREN/ds")
plt.title("Derivative of Strain Energy w.r.t. design parameter (s)")

plt.figure()
plt.plot(collect(values),VOL,"r")
plt.legend(["Material Volume"])
plt.xlabel("Design parameter (s)")
plt.ylabel("Volume")
plt.title("Material Volume as function of design parameter (s)")

plt.figure()
plt.plot(collect(values),VOL_DS,"r")
plt.legend(["Volume_ds"])
plt.xlabel("Design parameter (s)")
plt.ylabel("Volume_ds")
plt.title("Derivative of Material Volume w.r.t. design parameter (s)")

plt.figure()
res = [80,80]
X,t,h = draw(self.mesh,res)
plt.scatter(X[1,:],X[2,:],color="b")
plt.scatter(self.mesh.control_pts[1,:,:],self.mesh.control_pts[2,:,:],color="r")
plt.xlim(-(7/16)*dom_w,(23/16)*dom_w)
plt.ylim(-(1/4)*dom_h,(5/4)*dom_h)
plt.xlabel("x")
plt.ylabel("y")
plt.title("End Geometry")
