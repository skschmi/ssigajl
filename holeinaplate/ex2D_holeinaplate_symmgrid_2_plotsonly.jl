



subfolder = "01_1_two_elements"
#subfolder = "01_2_sixteen_elements"
#subfolder = "01_3_thirtysix_elements"

using PyCall
@pyimport numpy
STREN = numpy.loadtxt("plots_ignore/"*subfolder*"/data_STREN.txt")
CPX = numpy.loadtxt("plots_ignore/"*subfolder*"/data_CPX.txt")
CPY = numpy.loadtxt("plots_ignore/"*subfolder*"/data_CPY.txt")
S1 = numpy.loadtxt("plots_ignore/"*subfolder*"/data_S1.txt")
sdiag = numpy.loadtxt("plots_ignore/"*subfolder*"/data_sdiag.txt")

using PyPlot; plt = PyPlot;


plt.figure()
plt.scatter(CPX,CPY,c=STREN,marker="o",s=50)
plt.colorbar()
plt.title("Strain Energy as function of (s2,s3)")
plt.xlabel("s2")
plt.ylabel("s3")
plt.savefig("plots_ignore/"*subfolder*"/plot_STREN_fs2s3.pdf")

plt.figure()
plt.scatter(CPX,CPY,c=S1,marker="o",s=50)
plt.colorbar()
plt.title("s1 parameter as function of (s2,s3)")
plt.xlabel("s2")
plt.ylabel("s3")
plt.savefig("plots_ignore/"*subfolder*"/plot_S1_fs2s3.pdf")

plt.figure()
plt.plot(sdiag,STREN)
plt.title("Strain Energy as function of sdiag")
plt.xlabel("sdiag")
plt.ylabel("Strain Energy")
plt.savefig("plots_ignore/"*subfolder*"/plot_STREN_fsdiag.pdf")
