# Example - 2D IGA
# Basic 2D linear elastic problem in IGA

# Plate with a hole in the center.
# Using "NLOpt" to optimize the problem
#  ---> Symmetric along the diagonal, so only 4 degrees of freedom (3 without the weights)

include("../sslinearelastic.jl")
using .SSLinearElastic
using .SSLinearElastic.SSIGA2D
import .SSLinearElastic.SSIGA2D.IGAGrid2D
import .SSLinearElastic.SSIGA2D.delBasis
import .SSLinearElastic.SSIGA2D.Solve
import .SSLinearElastic.SSIGA2D.GetStrainDispMatB
import .SSLinearElastic.SSIGA2D.solution
import .SSLinearElastic.SSIGA2D.SSNurbsTools.Nurbs
import .SSLinearElastic.SSIGA2D.SSNurbsTools.draw
import .SSLinearElastic.SSIGA2D.SSNurbsTools.basis
import .SSLinearElastic.SSIGA2D.SSNurbsTools.Jacobian
import .SSLinearElastic.SSIGA2D.SSNurbsTools.domain_X
import .SSLinearElastic.SSIGA2D.SSNurbsTools.domain_dXdxi
import .SSLinearElastic.SSIGA2D.SSNurbsTools.domain_dXdsdxi
import .SSLinearElastic.SSIGA2D.SSGaussQuad.GaussQuadIntegrate
import .SSLinearElastic.SSIGA2D.SSGaussQuad.NumGaussPts
using NLopt
using LinearAlgebra

dirichletConstraint =
    function(self::IGAGrid2D,e::Array{Int64},a::Array{Int64},dofi::Int64)
        # bottom side (left side of xi) restricted in the y-dir
        if(e[1] == 1 && a[1] == 1 && dofi == 2)
            return true,0.0
        end
        # right side (right side of xi) restricted in the x-dir
        if(e[1] == self.nelements[1] && a[1] == self.nnodesperel[1] && dofi == 1)
            return true,0.0
        end
        return false,0.0
    end

neumannBCForElementFace =
    function(self::IGAGrid2D,e::Array{Int64},face::Tuple{Int64,Int64},xi::Array{Float64})
        # This returns the tractions for the specified face of the element, at position xi
        # Paramters:
        # e: Element index
        # face[1]: Parameter direction: 1={xi=-1,xi=1} faces.  2={eta=-1,eta=1} faces
        # face[2]: bottom or top face:  1={xi=-1 face} 2={xi=1 face}
        # xi: The xi,eta coordinates along the face we are looking at
        # Returns:
        # x_dir_traction, y_dir_traction

        # Left side
        if( e[2] == self.nelements[2] &&
            e[1] <= self.nelements[1]/2 &&
            face[1] == 2 &&
            face[2] == 2)

            return -2.5,0.0
        end
        # Top side
        if( e[2] == self.nelements[2] &&
            e[1] > self.nelements[1]/2 &&
            face[1] == 2 &&
            face[2] == 2)

            return 0.0,2.5
        end
        return 0.0,0.0
    end

# Problem set-up
dim_p = 2
dim_s = 2
p = [2,2]
n = [4,3]
span = n .- 1
knot_v = Array[[0., 0, 0, 0.5, 1, 1, 1],[0., 0, 0, 1, 1, 1]]
YoungsModE = 210.0
PoissonsRatNu = 0.3
#nel = [2,1]
nel = [6,6]
ndofpernode = 2
dom_w = 100.0
dom_h = 100.0

#c_pts = zeros(dim_s,n[1],n[2])
c_pts = Array{Function}(undef,dim_s,n[1],n[2])

#### c_pts[:,1,1] = [3.0, 0.0]
c_pts[1,1,1] = function(s)
    return s[1]
end
c_pts[2,1,1] = function(s)
    0.0
end
#### c_pts[:,2,1] = [3.0, 0.3]
c_pts[1,2,1] = function(s)
    return s[2]
end
c_pts[2,2,1] = function(s)
    return s[3]
end
#### c_pts[:,3,1] = [3.7, 1.0]
c_pts[1,3,1] = function(s)
    return dom_w-s[3]
end
c_pts[2,3,1] = function(s)
    return dom_h-s[2]
end
#### c_pts[:,4,1] = [4.0, 1.0]
c_pts[1,4,1] = function(s)
    return dom_w
end
c_pts[2,4,1] = function(s)
    return dom_h-s[1]
end


#### c_pts[:,1,2] = [1.5, 0.0]
c_pts[1,1,2] = function(s)
    return 0.375*dom_w
end
c_pts[2,1,2] = function(s)
    return 0.0
end
#### c_pts[:,2,2] = [1.5, 0.7]
c_pts[1,2,2] = function(s)
    return 0.375*dom_w
end
c_pts[2,2,2] = function(s)
    return 0.175*dom_h
end
#### c_pts[:,3,2] = [3.3, 2.5]
c_pts[1,3,2] = function(s)
    return 0.825*dom_w
end
c_pts[2,3,2] = function(s)
    return 0.625*dom_h
end
#### c_pts[:,4,2] = [4.0, 2.5]
c_pts[1,4,2] = function(s)
    return dom_w
end
c_pts[2,4,2] = function(s)
    return 0.625*dom_h
end


#### c_pts[:,1,3] = [0.0, 0.0]
c_pts[1,1,3] = function(s)
    return 0.0
end
c_pts[2,1,3] = function(s)
    return 0.0
end
#### c_pts[:,2,3] = [0.0, 4.0]
c_pts[1,2,3] = function(s)
    return 0.0
end
c_pts[2,2,3] = function(s)
    return dom_h
end
#### c_pts[:,3,3] = [0.0, 4.0]
c_pts[1,3,3] = function(s)
    return 0.0
end
c_pts[2,3,3] = function(s)
    return dom_h
end
#### c_pts[:,4,3] = [4.0, 4.0]
c_pts[1,4,3] = function(s)
    return dom_w
end
c_pts[2,4,3] = function(s)
    return dom_h
end


# Init the weights
weights = Array{Array{Function}}(undef,2)
weights[1] = Array{Function}(undef,4)
weights[2] = Array{Function}(undef,3)
# Set the weights
# First dir
weights[1][1] = function(s)
    return 1.
end
weights[1][2] = function(s)
    return (1 + (1/sqrt(2)))/2
    #return s[4]
end
weights[1][3] = function(s)
    return (1 + (1/sqrt(2)))/2
    #return s[4]
end
weights[1][4] = function(s)
    return 1.
end
# Second dir
weights[2][1] = function(s) 1. end
weights[2][2] = function(s) 1. end
weights[2][3] = function(s) 1. end

self = nothing

function create_geometry_and_solve(shape_params)
    global self
    self = IGAGrid2D(Nurbs(dim_p, dim_s, p, knot_v, span, c_pts, weights; s=shape_params),
                    nel,
                    ndofpernode,
                    YoungsModE,
                    PoissonsRatNu,
                    dirichletConstraint,
                    neumannBCForElementFace,
                    SSLinearElastic.neumannBCDsForElementFace_Zeros,
                    SSLinearElastic.bodyForceForElement_Zeros,
                    SSLinearElastic.FextForElement,
                    SSLinearElastic.FextDsForElement,
                    SSLinearElastic.NForElement,
                    SSLinearElastic.NDsForElement,
                    SSLinearElastic.consistentTangentForElement,
                    SSLinearElastic.consistentTangentDsForElement;
                    nloadsteps=0,
                    max_poly_degree=maximum(p)+2,
                    runtimeoutput=false)
    #####
    Solve(self)
    return self
end


######################
######################
######################
######################
######################
######################
######################
######################
######################
######################



################################
### Optimization using NLopt ###
################################

feval_count = 0 # keep track of # function evaluations

function myfunc(x::Vector, grad::Vector)
    global self
    self = create_geometry_and_solve(x)
    str_energy = dot(self.Fext[:],self.d[:])
    @show str_energy
    str_energy_ds = Array{Float64}([dot(self.Fext[:],self.d_ds[jj][:])+dot(self.Fext_ds[jj][:],self.d[:]) for jj=1:self.mesh.ndesignvars])

    if length(grad) > 0
        grad[:] = str_energy_ds[:]
    end

    global feval_count
    feval_count::Int += 1
    println("f_$count($x)")

    return str_energy
end

function myconstraint(x::Vector, grad::Vector, vol_const)
    global self

    material_volume = SSIGA2D.compute_volume(self)
    @show material_volume
    material_volume_ds = SSIGA2D.compute_volume_ds(self)

    if length(grad) > 0
        grad[:] = material_volume_ds[:]
    end
    return material_volume - vol_const
end


# Flat Diagonal Line Geometry
a = 0.103553
b = (1/4)*sqrt(pi/2)-(1/4)
x0 = (3+16*a - 4*b)/(4*(1+4*a))
y0 = x0 - ((3/4)-b)
c = a - y0
d = x0 - (3/4)
s0 = Array{Float64}([((3/4) - b)*dom_w,
                          ((3/4) + d)*dom_w,
                          (a - c)*dom_h])

#=
s0 = Array{Float64}([0.75*dom_w,
                     0.75*dom_w,
                     0.103553*dom_h,
                     (1 + (1/sqrt(2)))/2])
=#

n_vars = length(s0)
vol_const = dom_w*dom_h - (pi*(dom_w/4)*(dom_h/4))/4   #9509.126147876592

opt = Opt(:LD_MMA, n_vars)
lower_bounds!(opt, zeros(n_vars))
xtol_rel!(opt,1e-4)

min_objective!(opt, myfunc)
inequality_constraint!(opt, (x,g) -> myconstraint(x,g,vol_const), 1e-8)

(minf,minx,ret) = optimize(opt, s0)
println("got $minf at\n$minx\nafter $feval_count iterations (returned $ret)")
println("Constraint: ")
material_volume = SSIGA2D.compute_volume(self)
@show material_volume


################################
###  Plotting the Solution   ###
################################

using PyPlot; plt = PyPlot;
closeplots = false

self = create_geometry_and_solve(s0)

res = [80,80]
X,t,h = draw(self.geom,res)
plt.figure()
plt.scatter(X[1,:],X[2,:],color="b")
plt.hold(true)
plt.scatter(self.geom.control_pts[1,:,:],self.geom.control_pts[2,:,:],color="r")
plt.hold(false)
plt.xlim(-(7/16)*dom_w,(23/16)*dom_w)
plt.ylim(-(1/4)*dom_h,(5/4)*dom_h)
plt.xlabel("x")
plt.ylabel("y")
plt.title("Beginning Geometry")
plt.savefig("plots_ignore/plot_2Dprob_geom01.pdf")

res = [80,80]
X,t,h = draw(self.mesh,res)
plt.figure()
plt.scatter(X[1,:],X[2,:],color="b")
plt.hold(true)
plt.scatter(self.mesh.control_pts[1,:,:],self.mesh.control_pts[2,:,:],color="r")
plt.hold(false)
plt.xlim(-(7/16)*dom_w,(23/16)*dom_w)
plt.ylim(-(1/4)*dom_h,(5/4)*dom_h)
plt.xlabel("x")
plt.ylabel("y")
plt.title("Beginning Geometry - Refined")
plt.savefig("plots_ignore/plot_2Dprob_geom02.pdf")

self = create_geometry_and_solve(minx)

res = [80,80]
X,t,h = draw(self.geom,res)
plt.figure()
plt.scatter(X[1,:],X[2,:],color="b")
plt.hold(true)
plt.scatter(self.geom.control_pts[1,:,:],self.geom.control_pts[2,:,:],color="r")
plt.hold(false)
plt.xlim(-(7/16)*dom_w,(23/16)*dom_w)
plt.ylim(-(1/4)*dom_h,(5/4)*dom_h)
plt.xlabel("x")
plt.ylabel("y")
plt.title("Optimized Geometry")
plt.savefig("plots_ignore/plot_2Dprob_geom01_opt.pdf")

res = [80,80]
X,t,h = draw(self.mesh,res)
plt.figure()
plt.scatter(X[1,:],X[2,:],color="b")
plt.hold(true)
plt.scatter(self.mesh.control_pts[1,:,:],self.mesh.control_pts[2,:,:],color="r")
plt.hold(false)
plt.xlim(-(7/16)*dom_w,(23/16)*dom_w)
plt.ylim(-(1/4)*dom_h,(5/4)*dom_h)
plt.xlabel("x")
plt.ylabel("y")
plt.title("Optimized Geometry - Refined")
plt.savefig("plots_ignore/plot_2Dprob_geom02_opt.pdf")

X,Y,F = solution(self,1,[50,50])
plt.figure()
plt.pcolor(X,Y,F)
plt.colorbar()
plt.title("disp_x")
plt.savefig("plots_ignore/plot_2Dprob_disp01.pdf")
if(closeplots)
    plt.close()
end

X,Y,F = solution(self,2,[50,50])
plt.figure()
plt.pcolor(X,Y,F)
plt.colorbar()
plt.title("disp_y")
plt.savefig("plots_ignore/plot_2Dprob_disp02.pdf")
if(closeplots)
    plt.close()
end

X,Y,Fx = solution(self,1,[50,50])
X,Y,Fy = solution(self,2,[50,50])
normF = zeros(size(Fx))
for i = 1:length(Fx[:])
    normF[i] = sqrt(Fx[i]^2 + Fy[i]^2)
end
plt.figure()
plt.pcolor(X,Y,normF)
plt.colorbar()
plt.title("disp_norm")
plt.savefig("plots_ignore/plot_2Dprob_disp03.pdf")
if(closeplots)
    plt.close()
end
