
using PyCall
@pyimport numpy
STREN = numpy.loadtxt("plots_ignore/data_STREN.txt")
S1 = numpy.loadtxt("plots_ignore/data_S1.txt")
S2 = numpy.loadtxt("plots_ignore/data_S2.txt")
S3 = numpy.loadtxt("plots_ignore/data_S3.txt")

using PyPlot; plt = PyPlot;

plt.figure()
plt.pcolor(S2,S3,STREN)
plt.colorbar()
plt.title("Strain Energy as function of (s2,s3)")
plt.xlabel("s2")
plt.ylabel("s3")
plt.savefig("plots_ignore/plot_STREN_fs2s3.pdf")

plt.figure()
plt.pcolor(S2,S3,S1)
plt.colorbar()
plt.title("s1 parameter as function of (s2,s3)")
plt.xlabel("s2")
plt.ylabel("s3")
plt.savefig("plots_ignore/plot_S1_fs2s3.pdf")
