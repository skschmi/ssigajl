

module SSIGA

#using PyCall
#@pyimport numpy.linalg as linalg

# Library for NURBS
include("./ssnurbstoolsjl/nurbstools.jl")
using .SSNurbsTools
import .SSNurbsTools.Nurbs
import .SSNurbsTools.UniqueKnotArray
import .SSNurbsTools.bernstein_basis
import .SSNurbsTools.del_bernstein_basis
import .SSNurbsTools.basis
import .SSNurbsTools.delBasis
import .SSNurbsTools.nurbs_basis
import .SSNurbsTools.domain_dXdt
import .SSNurbsTools.domain_X
import .SSNurbsTools.ConvertToBernsteinMesh
import .SSNurbsTools.RefineMesh
import .SSNurbsTools.e_xi_for_t
import .SSNurbsTools.e_a_xi_for_gb_t

# Library for Gaussian Quadrature
include("./ssgaussquadjl/gaussquad.jl")
using .SSGaussQuad
import .SSGaussQuad.GaussQuadTable
import .SSGaussQuad.GaussQuadIntegrate
import .SSGaussQuad.NumGaussPts

# Library for indexing
using .SSNurbsTools.SSIndexingTools
import .SSNurbsTools.SSIndexingTools.IndexForRowCol
import .SSNurbsTools.SSIndexingTools.RowColForIndex
import .SSNurbsTools.SSIndexingTools.NodesPerDimension
import .SSNurbsTools.SSIndexingTools.LagrangeIEN
import .SSNurbsTools.SSIndexingTools.IsogeometricIEN

# Linear algebra library
using LinearAlgebra


"""
IGAGrid1D struct
==============

# Attributes:
* **geom**::Nurbs The original geometry
* **mesh**::Nurbs The mesh that defines the geometry, refined basis functions
* **bmesh**::Nurbs The mesh after bezier decompostion (repeated internal knots)
        that has a bernstein basis over each element domain
* **p**::Int64 Polynomial degree
* **uniqueknots**::Array{Float64} Array of parameter values of element boundaries
* **xmin**::Float64 Minimum x value of the phyisical domain
* **xmax**::Float64 Maximum x value of the physical domain

* **nelements**::Int64 Number of elements
* **nnodesperel**::Int64 Number of nodes per element (equals p+1)
* **ndofpernode**::Int64 Number of degrees of freedom per node

* **nnodes**::Int64 The number of nodes (or coefficients)
* **ID**::Array{Int64,2} Dimension ndofpernode-by-nnodes matrix, each column
        containing the index for each degree-of-freedom for each node. If
        the value is 0, then that dof is constrained.  Otherwise, it is the
        index of the column/row into the K matrix of that dof.
* **d**::Array{Float64} Dimension ndofpernode-by-nnodes array, The first row
        contains the final x-direction displacement for each node

* **dof**::Int64 Number of dof in the system (after BC are applied)

* **K**::Array{Int64,2} K matrix: Size dof-by-dof matrix
* **R**::Array{Int64,2} Residual vector: Length dof column-vector
* **old_R**::Array{Int64,2} Length dof column-vector
* **d_vec**::Array{Int64,2} Length dof column-vector: contains only
        those displacements that are dof.
* **del_d**::Array{Int64,2} Length dof column-vector: The change in displacements
* **Fext**::Array{Int64,2} Length dof column-vector
* **Nvec**::Array{Int64,2} Length dof column-vector

* **GQT1D**::Array{GaussQuadTable} For Gaussian Quadrature

* **dirichletConstraint**::Function Callback function for boundary conditions.
    Signature (grid::IGAGrid1D,e::Int64,a::Int64,dofi::Int64),
    **grid**::IGAGrid1D Pointer to the grid,
    **e**::Int64 The element index,
    **a**::Int64 The node index in the element,
    **dofi**::Int64 The degree of freedom, 1=x, etc.
    Returns: A tuple of type (Boolean,Float64). The boolean is true if the
    displacement is constrained for that dof, and is false otherwise. The
    Float64 value is the displacement for that dof, or ignored if the boolean
    is false.

* **neumannBCForElement**::Function Callback function for boundary conditions.
    Signature (grid::IGAGrid1D,e::Int64),
    **grid**::IGAGrid1D Pointer to the grid,
    **e**::Int64 The element index,
    Returns: Array of dimension ndofpernode-by-nnodesperel, each column refers to a node, and contains the
    tractions on the surface for each dof of that node. (A column for each node in the element is returned;
    Only nodes on the surface should be non-zero).

* **epsilon**::Float64 Important for newton-raphson
* **nloadsteps**::Int64 Important for newton-raphson
* **load_step_i**::Int64 Important for newton-raphson
* **max_correction_loops**::Int64 Important for newton-raphson

* **FextForElement**::Function Callback function for Newton-Raphson.
    Signature: (grid::IGAGrid1D,e::Int64),
    **grid**::IGAGrid1D Pointer to the grid,
    **e**::Int64: The element index,
    Returns: Array of dimension ndofpernode-by-nnodesperel,
    containing the value of Fext for the element.

* **NForElement**::Function Callback function for Newton-Raphson.
    Signature (grid::IGAGrid1D,e::Int64),
    **grid**::IGAGrid1D Pointer to the grid,
    **e**::Int64 The element index,
    Returns: Array of dimension ndofpernode-by-nnodesperel,
    containing the value of N (or Fint) for the element.
    Review of Newton-Raphson:
    K*d == F
    K*(d0+del_d) == F
    K*d0 + K*del_d == F
    K*del_d == F-K*d0
    K*del_d == F-N  (because N == K*d0 == Fint)
    K*del_d = R (the residual)
    Then, solve for del_d.

* **consistentTangentForElement**::Function Callback function for Newton-Raphson.
    Signature (grid::IGAGrid1D,e::Int64),
    **grid**::IGAGrid1DPointer to the grid,
    **e**: The element index,
    Returns: The value of the consistent tangent (slope) of the load-displacement
    curve, for element e.  Return type: A nnodesperel-by-nnodesperel type
    Array{Array{Float64}} matrix, containing at each index a size
    ndofpernode-by-ndofpernode type Array{Float64} matrix.

# Constructor parameters:
* **geometry**::Nurbs
* **nelements**::Int64
* **ndofpernode**::Int64
* **dirichletConstraint**::Function
* **neumannBCForElement**::Function
* **FextForElement**::Function
* **NForElement**::Function
* **consistentTangentForElement**::Function

# Constructor named parameters:
* **epsilon**=1e-12
* **nloadsteps**=5
* **max_correction_loops**=50
* **max_poly_degree**=mesh.p+1: The maximum polynomial degree needed
    for gaussian quadrature.  Pts and wts for degrees between 1 and this
    value will be available for use.
"""
mutable struct IGAGrid1D

    # Geometry
    geom::Nurbs
    mesh::Nurbs
    #bmesh::Nurbs
    p::Int64 # polynomial degree of elements
    uniqueknots::Array{Float64}
    xmin::Float64
    xmax::Float64

    # Info for each element
    nelements::Int64
    nnodesperel::Int64
    ndofpernode::Int64

    # Info for each node:
    nnodes::Int64
    ID::Array{Int64,2} #Dof for each node
    d::Array{Float64,2}  #Contains all displacements, including BC displacements

    # Number of dof in the system (after BC are applied)
    dof::Int64

    #Values we save here for efficiency
    K::Array{Float64,2}      # Size dof-by-dof matrix
    R::Array{Float64}      # Length dof array
    old_R::Array{Float64}  # Length dof array
    d_vec::Array{Float64}  # Length dof array: contains only
                           #  those displacements that are dof.
    del_d::Array{Float64}  # Length dof array: The change in displacements
    Fext::Array{Float64}   # Length dof array
    Nvec::Array{Float64}   # Length dof array

    # For Gaussian Quadrature:
    max_poly_degree::Int64
    GQT1D::Array{GaussQuadTable}

    # *** Callback functions for boundary conditions ***
    dirichletConstraint::Function
    neumannBCForElement::Function

    # Important for newton-raphson:
    epsilon::Float64
    nloadsteps::Int64
    load_step_i::Int64
    max_correction_loops::Int64

    # *** Callback functions for Newton-Raphson algorithm ***
    FextForElement::Function
    NForElement::Function
    consistentTangentForElement::Function

    function IGAGrid1D(geometry::Nurbs,
                       nelements::Int64,
                       ndofpernode::Int64,
                       dirichletConstraint::Function,
                       neumannBCForElement::Function,
                       FextForElement::Function,
                       NForElement::Function,
                       consistentTangentForElement::Function;
                       epsilon=1e-12,
                       nloadsteps=5,
                       max_correction_loops=50,
                       max_poly_degree=-1)
        self = new()

        # The original geometry
        self.geom = Nurbs(geometry)

        # Refine the Geometry
        self.mesh = Nurbs(geometry)
        RefineMesh(self.mesh,[nelements])

        # Create Bernstein Mesh (performs bezier decomposition
        #    and creates new mesh)
        #self.bmesh = Nurbs(self.mesh)
        #ConvertToBernsteinMesh(self.bmesh)

        self.uniqueknots = UniqueKnotArray(self.mesh.knot_v[1])
        self.xmin = self.mesh.control_pts[1]
        self.xmax = self.mesh.control_pts[end]
        self.p = self.mesh.degree[1]

        if(max_poly_degree >= 1)
            self.max_poly_degree = max_poly_degree
        else
            self.max_poly_degree=self.p+1
            max_poly_degree = self.p+1
        end

        self.nelements = nelements
        self.nnodesperel = self.p+1
        self.ndofpernode = ndofpernode
        self.nnodes = length(self.mesh.control_pts)

        # Tracking load-step, correction loops iterations
        self.epsilon = epsilon
        self.max_correction_loops = max_correction_loops
        self.nloadsteps = nloadsteps
        self.load_step_i = 0

        # Constraints information
        self.ID = Array{Int64,2}(undef,0,0) # to be set later (needs constraints info)

        # Displacement array (all nodes)
        self.d = zeros(self.ndofpernode,self.nnodes)

        # Number of dof in the system (after BC are applied)
        self.dof = 0                      # to be set later

        self.K = Array{Float64,2}(undef,0,0)         # to be set later
        self.R = Array{Float64,2}(undef,0,0)         # to be set later
        self.old_R = Array{Float64,2}(undef,0,0)     # to be set later
        self.d_vec = Array{Float64,2}(undef,0,0)     # to be set later
        self.del_d = Array{Float64,2}(undef,0,0)     # to be set later
        self.Fext = Array{Float64,2}(undef,0,0)      # to be set later
        self.Nvec = Array{Float64,2}(undef,0,0)      # to be set later

        # For Gaussian Quadrature:
        self.GQT1D = Array{GaussQuadTable}(undef,NumGaussPts(self.max_poly_degree))
        for k = 1:NumGaussPts(self.max_poly_degree)
            self.GQT1D[k] = GaussQuadTable(k)
        end

        # Callback functions for boundary conditions
        self.dirichletConstraint = dirichletConstraint
        self.neumannBCForElement = neumannBCForElement

        # Callback functions for Fext, N, consistent tangent
        self.FextForElement = FextForElement
        self.NForElement = NForElement
        self.consistentTangentForElement = consistentTangentForElement

        return self
    end

end






"""
function tmin(self::IGAGrid1D,e)
========
Returns the minimum t value for element e

# Parameters
* self:
* e:
"""
function param_t_min(self::IGAGrid1D,e::Int64)
    return self.uniqueknots[e]
end




"""
function tmax(self::IGAGrid1D,e)
========
Returns the maximum t value for element e

# Parameters
* self:
* e:
"""
function param_t_max(self::IGAGrid1D,e::Int64)
    return self.uniqueknots[e+1]
end





"""
function processBoundaryConditions(self::IGAGrid1D)
===========

# This function does the following:
1. Assembles the "ID" array, which contains a table which maps
       the nodal index to its corresponding dof indices.
2. Determines the number of degrees of freedom.
3. Sets the constrained nodal displacements, and zeros-out the rest.

# Parameters
* self: Pointer to the grid

# Returns:
* Reference to the ID array, the number of dof, and the nodal d array
"""
function processBoundaryConditions(self::IGAGrid1D)

    # --------------------------------------
    # Building the ID array, Setting BC info
    # --------------------------------------

    # Step 1.1: Init the ID array, set all values to 1
    # Step 1.2: Also, init the solution vector d
    self.ID = ones(Int64,self.ndofpernode,self.nnodes)
    fill!(self.d,0.0)

    # Step 2.1: Mark the constrained dof by looping over elements,
    #         and settting the ID array to 0 for constrained elements
    # Step 2.2: Also, init the displacement BC in the final displacement vector d.
    for e = 1:self.nelements
        for a = 1:self.nnodesperel
            for dofi = 1:self.ndofpernode
                hasDisplacement,d_val = self.dirichletConstraint(self,e,a,dofi)
                if( hasDisplacement )
                    self.ID[dofi,self.mesh.IEN[1][a,e]] = 0
                    self.d[dofi,self.mesh.IEN[1][a,e]] = d_val
                end
            end
        end
    end

    # Step 3: Specify the dof index for each non-constrained dof.
    dof_count = 0
    for node = 1:self.nnodes
        for dofi = 1:self.ndofpernode
            if( self.ID[dofi,node] == 1 )
                dof_count += 1
                self.ID[dofi,node] = dof_count
            end
        end
    end

    # Step 4: Set the number of degrees of freedom
    self.dof = dof_count

    return self.ID,self.dof,self.d

end




"""
function ElementDofLookup(self,e,a,dofi)
==================
Sometimes called "LM array".

# Parameters:
* e: The element index
* a: The node of the element
* dofi: The index of the degree of freedom (1=x, 2=y)

# Returns:
* The index of the degree of freedom of a node in an element
"""
function ElementDofLookup(self::IGAGrid1D,e::Int64,a::Int64,dofi::Int64)
    return self.ID[dofi,self.mesh.IEN[1][a,e]]
end





"""
function updateD(self::IGAGrid1D)
=======
Assumes that the most recent displacements
are in "d_vec", and sets the corresponding
values in "d" to the values in "d_vec".
"""
function updateD(self::IGAGrid1D)
    for e = 1:self.nelements
        for a = 1:self.nnodesperel
            for dofi = 1:self.ndofpernode
                glob_dof_i = ElementDofLookup(self,e,a,dofi)
                if(glob_dof_i > 0)
                    self.d[dofi,self.mesh.IEN[1][a,e]] = self.d_vec[glob_dof_i]
                end
            end
        end
    end
end






"""
function assembleGlobalVector(self::IGAGrid1D,
                              vector::Array{Float64},
                              VecForElement::Function)
=======
Assembles a global vector using function 'VecForElement' (1D)

# Parameters:
* self: Pointer to the grid
* vector: Pointer to the vector to be populated.
* VecForElement: A function with signature (self::IGAGrid1D,e::Int64)
                 for grid "self" and element index "e".
"""
function assembleGlobalVector(self::IGAGrid1D,
                              vector::Array{Float64},
                              VecForElement::Function)
    # Assumes the "vector" array already exists

    # Initialize the residual array
    fill!(vector,0.0)

    # Loop over each element
    for e = 1:self.nelements
        # (1) Call "VecForElement"
        # (2) Add the result into the global value.
        vec_el = VecForElement(self,e)
        for a = 1:self.nnodesperel
            for dofi = 1:self.ndofpernode
                glob_dof_i = ElementDofLookup(self,e,a,dofi)
                if(glob_dof_i > 0)
                    vector[glob_dof_i] += vec_el[dofi,a]
                end
            end
        end
    end

    return vector
end






"""
function assembleGlobalFext(self::IGAGrid1D)
=========
Assembles the Global Fext vector.
"""
function assembleGlobalFext(self::IGAGrid1D)
    # Assumes the "Fext" array already exists
    # Initialize the residual array
    fill!(self.Fext,0.0)
    assembleGlobalVector(self,self.Fext,self.FextForElement)
    return self.Fext
end





"""
function assembleGlobalN(self::IGAGrid1D)
=========
Assembles the global N vector
"""
function assembleGlobalN(self::IGAGrid1D)
    # Assumes the "Nvec" array already exists
    # Initialize the residual array
    fill!(self.Nvec,0.0)
    assembleGlobalVector(self,self.Nvec,self.NForElement)
    return self.Nvec
end






"""
function assembleGlobalResidual(self::IGAGrid1D)
=====
Assembles the Global Residual vector (1D)
"""
function assembleGlobalResidual(self::IGAGrid1D)
    # Assumes the "R" array already exists

    # Initialize the residual array
    fill!(self.R,0.0)

    assembleGlobalFext(self)
    assembleGlobalN(self)

    # Use a fraction of the actual Fext, depending on the load-step
    loadloopmult = 1.0
    if(self.nloadsteps > 0)
        loadloopmult = (self.load_step_i/self.nloadsteps)
    end

    self.R = self.Fext*loadloopmult - self.Nvec

    return self.R
end






"""
function assembleGlobalConsistentTangent(self::IGAGrid1D)
=====
Assembles the Global Consistent Tangent vector (1D)
"""
function assembleGlobalConsistentTangent(self::IGAGrid1D)
    # Assumes the "K" matrix already exists

    # Initialize the K matrix
    fill!(self.K,0.0)

    # Loop over each of element
    for e = 1:self.nelements
        K_el = self.consistentTangentForElement(self,e)
        for ai = 1:self.nnodesperel
            for aj = 1:self.nnodesperel
                for nd_dof_i = 1:self.ndofpernode
                    for nd_dof_j = 1:self.ndofpernode
                        k_i = ElementDofLookup(self,e,ai,nd_dof_i)
                        k_j = ElementDofLookup(self,e,aj,nd_dof_j)
                        if(k_i > 0 && k_j > 0)
                            self.K[k_i,k_j] += K_el[ai,aj][nd_dof_i,nd_dof_j]
                        end
                    end
                end
            end
        end
    end

    return self.K
end


"""
function solvePrep(self::IGAGrid1D)
====
Does everything up to doing the first
matrix solve in the Solve function, for testing.
"""
function solve_prep(self::IGAGrid1D)

    # Zeroing-out the "d" array
    fill!(self.d,0.0)

    # Process the Boundary Conditions, determining global number of dof
    # Also, sets the values of self.d on boundaries.
    processBoundaryConditions(self)

    # Finish initializing all the other important arrays
    self.d_vec = zeros(self.dof)
    self.del_d = zeros(self.dof)
    self.R = zeros(self.dof)
    self.old_R = zeros(self.dof)
    self.K = zeros(self.dof,self.dof)
    self.Nvec = zeros(self.dof)
    self.Fext = zeros(self.dof)

    MAX_corrloops = self.max_correction_loops

    updateD(self)

    # Initialize the residual (R)
    assembleGlobalResidual(self)

    # Assemble the global consistent tangent (K)
    assembleGlobalConsistentTangent(self)

end


"""
function Solve(self::IGAGrid1D)
======
Solve the IGA system using Newton-Raphson (1D)
"""
function Solve(self::IGAGrid1D)

    # Zeroing-out the "d" array
    fill!(self.d,0.0)

    # Process the Boundary Conditions, determining global number of dof
    # Also, sets the values of self.d on boundaries.
    # println("processing boundary conditions..")
    processBoundaryConditions(self)

    # Finish initializing all the other important arrays
    self.d_vec = zeros(self.dof)
    self.del_d = zeros(self.dof)
    self.R = zeros(self.dof)
    self.old_R = zeros(self.dof)
    self.K = zeros(self.dof,self.dof)
    self.Nvec = zeros(self.dof)
    self.Fext = zeros(self.dof)

    MAX_corrloops = self.max_correction_loops

    # Load-step loop
    self.load_step_i = 0
    while(self.load_step_i <= self.nloadsteps)

        println("Load step: ",self.load_step_i," out of ",self.nloadsteps)

        #Copy the most recent 'd_vec' values into 'd'.
        #println("updating D..")
        updateD(self)

        # Initialize the residual (R)
        #println("assembling global residual..")
        assembleGlobalResidual(self)
        println("     Correction step: ","0", "   R:     ",maximum(abs.(self.R)))

        # Correction loop.
        corrloop_k = 1
        while(true)

            # Assemble the global consistent tangent (K)
            # println("     assembling global consistent tangent..")
            assembleGlobalConsistentTangent(self)

            #println("K:\n",self.K,"\n"); println("self.dof:",self.dof);
            #println("     det(self.K):",det(self.K))

            self.del_d[:] = self.K\self.R     # run = rise/(rise/run)
            #self.del_d[:] = pinv(self.K)*self.R     # run = rise/(rise/run)
            #self.del_d[:] = linalg.inv(self.K)*self.R

            self.d_vec[:] += self.del_d[:]    # Update displacement guess

            # Copy the most recent 'd_vec' values into 'd'.
            updateD(self)

            # Update the residual
            #self.old_R[:] = R[:]  #"old_R" used for convergence tests.
            # println("     assembling global residual..")
            assembleGlobalResidual(self)

            println("     Correction step: ",corrloop_k, "   R:     ",maximum(abs.(self.R)))

            # Break out of loop if residual is small enough.
            if(maximum(abs.(self.R)) <= self.epsilon || corrloop_k >= MAX_corrloops)
                break
            end

            corrloop_k += 1
        end
        self.load_step_i += 1
    end

    println("Solve done!")

end


"""
function solution(self::IGAGrid1D,dof::Int64,samplesperel::Int64)
=====
Returns:
* X: The x-values of each data point returned
* Y: The function values at each point X.
"""
function solution(self::IGAGrid1D,dof::Int64,samplesperel::Int64)
    Y = zeros(samplesperel*self.nelements)
    X = zeros(samplesperel*self.nelements)
    val = 0.0
    # Loop over elements
    for e = 1:self.nelements
        # Loop over sample points
        for sp = 1:samplesperel
            #We aim for the centers of each element segment.
            #[-h--h-][-h--h-][-h--h-] for npernode=3
            h = 2.0/(2*samplesperel)
            #[  *    *    *  ] <-- Those are the sample points, for npernode=3
            xi = -1.0 + h + 2*h*(sp-1)

            # compute the value
            N = basis(self.mesh,[e],[xi])
            val = dot(N[:],self.d[dof,self.mesh.IEN[1][1,e]:self.mesh.IEN[1][self.p+1,e]][:])

            #set the values in the array
            cpmin = self.mesh.IEN[1][1,e]
            cpmax = self.mesh.IEN[1][self.p+1,e]
            X[(e-1)*samplesperel + sp] = domain_X(self.mesh,[e],reshape([xi],1,1))[1]
            Y[(e-1)*samplesperel + sp] = val
        end
    end
    return X,Y
end







#***
#***********
#********************
#********************************
#**********************************************


#***************************************************************
#***************************************************************
#        FUNCTIONS NOT NEEDED, BUT MIGHT BE USEFUL LATER
#***************************************************************
#***************************************************************


"""
function nurbsBasis(self::IGAGrid1D,i::Int64,t)
==============
* **self**::IGAGrid1D Pointer to the Nurbs object
* **gb**::Int64 Global node index
* **t** Array of floats: The parameter values where the
    function is evaluated
* Returns: The value of the NURBS basis evaluated at values t.

# NOTE: this function not needed?
"""
function nurbsBasis(self::IGAGrid1D,gb::Int64,t)
    ans = zeros(length(t))
    for k = 1:length(t)
        e,a,xi = e_a_xi_for_gb_t(self.mesh,[gb],[t[k]])
        if( minimum(a) != 0 )
            ans[k] = basis(self.mesh,e,a,xi)
        else
            ans[k] = 0.0
        end
    end
    return ans
end



"""
function nurbsBasis(self::IGAGrid1D,e::Int64,a::Int64,t)
==============
* self::IGAGrid1D Pointer to the Nurbs object
* e: Element index
* a: Element node index
* t: Array of floats: The parameter values where the function is evaluated
* Returns: The value of the NURBS basis evaluated at values t.

# NOTE: this function not needed?
"""
function nurbsBasis(self::IGAGrid1D,e::Int64,a::Int64,t)
    return nurbsBasis(self,self.mesh.IEN[1][a,e],t)
end



#***************************************************************
#***************************************************************






end # module SSIGA
