
include("../ssnurbstoolsjl/nurbstools.jl")

using SSNurbsTools
import SSNurbsTools.Nurbs
import SSNurbsTools.draw
import SSNurbsTools.basis
import SSNurbsTools.Jacobian
import SSNurbsTools.e_xi_for_t
import SSNurbsTools.domain_X
import SSNurbsTools.ConvertToBernsteinMesh
import SSNurbsTools.e_a_xi_for_gb_t

# Drawing a basic b-spline to generate images for presentation.

dim_p = 1
dim_s = 1
p = [3]
n = [7]
spans = n .- 1

knot_v = Array[[0., 0, 0, 0, 1, 2, 3, 4, 4, 4, 4]]

c_pts = reshape([ (knot_v[1][2] + knot_v[1][3] + knot_v[1][4])/3,
                  (knot_v[1][3] + knot_v[1][4] + knot_v[1][5])/3,
                  (knot_v[1][4] + knot_v[1][5] + knot_v[1][6])/3,
                  (knot_v[1][5] + knot_v[1][6] + knot_v[1][7])/3,
                  (knot_v[1][6] + knot_v[1][7] + knot_v[1][8])/3,
                  (knot_v[1][7] + knot_v[1][8] + knot_v[1][9])/3,
                  (knot_v[1][8] + knot_v[1][9] + knot_v[1][10])/3 ],1,7)
weights = Array{Array{Float64}}(undef,1)
weights[1] = Array{Float64}([1.,1.,1.,1.,1.,1.,1.])
geometry = Nurbs(dim_p, dim_s, p, knot_v, spans, c_pts, weights)
self = geometry
bern = Nurbs(self)
ConvertToBernsteinMesh(bern)


println("here we are!")

using PyPlot; plt = PyPlot;

path_prefix2 = "~/Documents/phd/Writings/26_UsplinePresentation/figures/"
println(  Base.Filesystem.expanduser(path_prefix2) )

path_prefix = "/Users/sschmidt/Documents/phd/Writings/26_UsplinePresentation/figures/"
figure_size = (8, 2)

for gb_to_show = 1:geometry.spans[1]+1

    # Plotting the geometry's basis functions
    plt.figure(figsize=figure_size)
    t = collect(minimum(knot_v[1]):0.01:maximum(knot_v[1]))
    for a = 1:4
        B = zeros(length(t))
        for i = 1:length(t)
            tval = t[i]
            e,xi = e_xi_for_t(geometry,reshape([tval],1,1))
            B[i] = basis(geometry,e,[a],xi)
        end
        plt.plot(t,B)
    end
    # Plot one of the global basis functions on top
    t = collect( minimum(geometry.knot_v[1]):0.01:maximum(geometry.knot_v[1]) )
    B = zeros(length(t))
    for gb = 1:geometry.spans[1]+1
        for i = 1:length(t)
            e,a,xi = e_a_xi_for_gb_t(geometry,[gb],[t[i]])
            if(a[1]>0)
                B[i] = basis(geometry,e,a,xi)
            else
                B[i] = 0.0
            end
        end
        if( gb == gb_to_show )
            plt.plot(t,B,linewidth=4,color="r")
        end
    end
    plt.title("Constructed B-Spline basis functions over each Bezier curve.")
    plt.savefig(path_prefix*"simple_000123444_extracted_bspl_basisf_"*lpad(gb_to_show,2,0)*".pdf")
end
