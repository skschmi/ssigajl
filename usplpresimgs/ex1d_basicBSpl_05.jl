
include("../ssnurbstoolsjl/nurbstools.jl")

using SSNurbsTools
import SSNurbsTools.Nurbs
import SSNurbsTools.draw
import SSNurbsTools.basis
import SSNurbsTools.Jacobian
import SSNurbsTools.e_xi_for_t
import SSNurbsTools.domain_X
import SSNurbsTools.ConvertToBernsteinMesh
import SSNurbsTools.e_a_xi_for_gb_t

# Drawing a basic b-spline to generate images for presentation.

#=
dim_p = 1
dim_s = 1
p = [2]
n = [3]
spans = n .- 1
knot_v = Array[[0.,   0, 0, 1, 1,   1]]
c_pts = reshape([ (knot_v[1][2] + knot_v[1][3])/2,
                  (knot_v[1][3] + knot_v[1][4])/2,
                  (knot_v[1][4] + knot_v[1][5])/2 ], 1, n[1])
weights = Array{Array{Float64}}(undef,1)
weights[1] = Array{Float64}([1.,1.,1.])
geometry = Nurbs(dim_p, dim_s, p, knot_v, spans, c_pts, weights)
self = geometry
bern = Nurbs(self)
ConvertToBernsteinMesh(bern)
=#


dim_p = 1
dim_s = 1
p = [1]
n = [2]
spans = n .- 1
knot_v = Array[[0., 0, 1, 1]]
c_pts = reshape([ (knot_v[1][2])/1,
                  (knot_v[1][3])/1  ], 1, n[1])
weights = Array{Array{Float64}}(undef,1)
weights[1] = Array{Float64}([1.,1.])
geometry = Nurbs(dim_p, dim_s, p, knot_v, spans, c_pts, weights)
self = geometry
bern = Nurbs(self)
ConvertToBernsteinMesh(bern)



using PyPlot; plt = PyPlot;


figure_size = (8, 2)

# Drawing the curve
res = [50]
X,t,h = draw(geometry,res)
closeplots = false
plt.figure(figsize=figure_size)
plt.scatter(X[1,:],zeros(length(X)),color="b")
plt.scatter(geometry.control_pts[1,:],zeros(length(geometry.control_pts)),color="r")
#plt.xlim(-0.1,2.1)
#plt.ylim(-0.5,1.5)
plt.title("Geometry")


# Plotting the geometry's basis functions
plt.figure(figsize=figure_size)
t = collect(minimum(knot_v[1]):0.01:maximum(knot_v[1]))
for a = 1:p[1]+1
    B = zeros(length(t))
    for i = 1:length(t)
        tval = t[i]
        e,xi = e_xi_for_t(geometry,reshape([tval],1,1))
        B[i] = basis(geometry,e,[a],xi)
    end
    plt.plot(t,B)
end
plt.title("Constructed B-Spline basis functions over each Bezier curve.")


# Plot the global basis functions separately (instead of element-by-element)
plt.figure(figsize=figure_size)
t = collect( minimum(geometry.knot_v[1]):0.01:maximum(geometry.knot_v[1]) )
B = zeros(length(t))
for gb = 1:geometry.spans[1]+1
    for i = 1:length(t)
        e,a,xi = e_a_xi_for_gb_t(geometry,[gb],[t[i]])
        if(a[1]>0)
            B[i] = basis(geometry,e,a,xi)
        else
            B[i] = 0.0
        end
    end
    plt.plot(t,B)
end
plt.title("Global B-Spline basis functions")


# Plot the bernstein mesh basis functions:
plt.figure(figsize=figure_size)
t = collect(minimum(knot_v[1]):0.01:maximum(knot_v[1]))
for a = 1:p[1]+1
    B = zeros(length(t))
    for i = 1:length(t)
        tval = t[i]
        e,xi = e_xi_for_t(bern,reshape([tval],1,1))
        B[i] = basis(bern,e,[a],xi)
    end
    plt.plot(t,B)
end
plt.title("Bernstein basis functions over each Bezier curve")
