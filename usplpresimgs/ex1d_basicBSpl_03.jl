
include("../ssnurbstoolsjl/nurbstools.jl")

using SSNurbsTools
import SSNurbsTools.Nurbs
import SSNurbsTools.draw
import SSNurbsTools.basis
import SSNurbsTools.Jacobian
import SSNurbsTools.e_xi_for_t
import SSNurbsTools.domain_X
import SSNurbsTools.ConvertToBernsteinMesh
import SSNurbsTools.e_a_xi_for_gb_t

# Drawing a basic b-spline to generate images for presentation.

dim_p = 1
dim_s = 1
p = [3]
n = [8]
spans = n .- 1

knot_v = Array[[0., 0, 0, 0, 1, 2, 3, 4, 5, 5, 5, 5]]

c_pts = reshape([ (knot_v[1][2] + knot_v[1][3] + knot_v[1][4])/3,
                  (knot_v[1][3] + knot_v[1][4] + knot_v[1][5])/3,
                  (knot_v[1][4] + knot_v[1][5] + knot_v[1][6])/3,
                  (knot_v[1][5] + knot_v[1][6] + knot_v[1][7])/3,
                  (knot_v[1][6] + knot_v[1][7] + knot_v[1][8])/3,
                  (knot_v[1][7] + knot_v[1][8] + knot_v[1][9])/3,
                  (knot_v[1][8] + knot_v[1][9] + knot_v[1][10])/3,
                  (knot_v[1][9] + knot_v[1][10] + knot_v[1][11])/3,],1,8)
weights = Array{Array{Float64}}(undef,1)
weights[1] = Array{Float64}([1.,1.,1.,1.,1.,1.,1.,1.])
geometry = Nurbs(dim_p, dim_s, p, knot_v, spans, c_pts, weights)
self = geometry
bern = Nurbs(self)
ConvertToBernsteinMesh(bern)


println("here we are!")

using PyPlot; plt = PyPlot;

path_prefix2 = "~/Documents/phd/Writings/26_UsplinePresentation/figures/"
println(  Base.Filesystem.expanduser(path_prefix2) )

path_prefix = "/Users/sschmidt/Documents/phd/Writings/26_UsplinePresentation/figures/"
figure_size = (8, 2)

# Drawing the curve
res = [50]
X,t,h = draw(geometry,res)
closeplots = false
plt.figure(figsize=figure_size)
plt.scatter(X[1,:],zeros(length(X)),color="b")
plt.scatter(geometry.control_pts[1,:],zeros(length(geometry.control_pts)),color="r")
#plt.xlim(-0.1,2.1)
#plt.ylim(-0.5,1.5)
plt.title("Geometry")
plt.savefig(path_prefix*"simple_0001234555_bspl_geometry.pdf")


# Plotting the geometry's basis functions
plt.figure(figsize=figure_size)
t = collect(minimum(knot_v[1]):0.01:maximum(knot_v[1]))
for a = 1:4
    B = zeros(length(t))
    for i = 1:length(t)
        tval = t[i]
        e,xi = e_xi_for_t(geometry,reshape([tval],1,1))
        B[i] = basis(geometry,e,[a],xi)
    end
    plt.plot(t,B)
end
plt.title("Constructed B-Spline basis functions over each Bezier curve.")
plt.savefig(path_prefix*"simple_0001234555_extracted_bspl_basisf.pdf")


# Plot the global basis functions separately (instead of element-by-element)
plt.figure(figsize=figure_size)
t = collect( minimum(geometry.knot_v[1]):0.01:maximum(geometry.knot_v[1]) )
B = zeros(length(t))
for gb = 1:geometry.spans[1]+1
    for i = 1:length(t)
        e,a,xi = e_a_xi_for_gb_t(geometry,[gb],[t[i]])
        if(a[1]>0)
            B[i] = basis(geometry,e,a,xi)
        else
            B[i] = 0.0
        end
    end
    plt.plot(t,B)
end
plt.title("Global B-Spline basis functions")
plt.savefig(path_prefix*"simple_0001234555_global_bspl_basisf.pdf")


# Plot the bernstein mesh basis functions:
plt.figure(figsize=figure_size)
t = collect(minimum(knot_v[1]):0.01:maximum(knot_v[1]))
for a = 1:4
    B = zeros(length(t))
    for i = 1:length(t)
        tval = t[i]
        e,xi = e_xi_for_t(bern,reshape([tval],1,1))
        B[i] = basis(bern,e,[a],xi)
    end
    plt.plot(t,B)
end
plt.title("Bernstein basis functions over each Bezier curve")
plt.savefig(path_prefix*"simple_0001234555_bernstein_bezier_basisf.pdf")
