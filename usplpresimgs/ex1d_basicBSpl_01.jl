
include("../ssnurbstoolsjl/nurbstools.jl")

using SSNurbsTools
import SSNurbsTools.Nurbs
import SSNurbsTools.draw
import SSNurbsTools.basis
import SSNurbsTools.Jacobian
import SSNurbsTools.e_xi_for_t
import SSNurbsTools.domain_X
import SSNurbsTools.ConvertToBernsteinMesh
import SSNurbsTools.e_a_xi_for_gb_t

# Drawing a basic b-spline to generate images for presentation.

dim_p = 1
dim_s = 1
p = [3]
n = [7]
spans = n .- 1

knot_v = Array[[0., 0, 0, 0, 1, 2, 3, 4, 4, 4, 4]]

c_pts = reshape([ (knot_v[1][2] + knot_v[1][3] + knot_v[1][4])/3,
                  (knot_v[1][3] + knot_v[1][4] + knot_v[1][5])/3,
                  (knot_v[1][4] + knot_v[1][5] + knot_v[1][6])/3,
                  (knot_v[1][5] + knot_v[1][6] + knot_v[1][7])/3,
                  (knot_v[1][6] + knot_v[1][7] + knot_v[1][8])/3,
                  (knot_v[1][7] + knot_v[1][8] + knot_v[1][9])/3,
                  (knot_v[1][8] + knot_v[1][9] + knot_v[1][10])/3 ],1,7)
weights = Array{Array{Float64}}(undef,1)
weights[1] = Array{Float64}([1.,1.,1.,1.,1.,1.,1.])
geometry = Nurbs(dim_p, dim_s, p, knot_v, spans, c_pts, weights)
self = geometry
bern = Nurbs(self)
ConvertToBernsteinMesh(bern)


println("here we are!")

using PyPlot; plt = PyPlot;

path_prefix2 = "~/Documents/phd/Writings/26_UsplinePresentation/figures/"
println(  Base.Filesystem.expanduser(path_prefix2) )

path_prefix = "/Users/sschmidt/Documents/phd/Writings/26_UsplinePresentation/figures/"
figure_size = (8, 2)

# Drawing the curve
res = [50]
X,t,h = draw(geometry,res)
closeplots = false
plt.figure(figsize=figure_size)
plt.scatter(X[1,:],zeros(length(X)),color="b")
plt.scatter(geometry.control_pts[1,:],zeros(length(geometry.control_pts)),color="r")
#plt.xlim(-0.1,2.1)
#plt.ylim(-0.5,1.5)
plt.title("Geometry")
plt.savefig(path_prefix*"simple_000123444_bspl_geometry.pdf")


# Plotting the geometry's basis functions
plt.figure(figsize=figure_size)
t = collect(minimum(knot_v[1]):0.01:maximum(knot_v[1]))
for a = 1:4
    B = zeros(length(t))
    for i = 1:length(t)
        tval = t[i]
        e,xi = e_xi_for_t(geometry,reshape([tval],1,1))
        B[i] = basis(geometry,e,[a],xi)
    end
    plt.plot(t,B)
end
plt.title("Constructed B-Spline basis functions over each Bezier curve.")
plt.savefig(path_prefix*"simple_000123444_extracted_bspl_basisf.pdf")


# Plot the global basis functions separately (instead of element-by-element)
plt.figure(figsize=figure_size)
t = collect( minimum(geometry.knot_v[1]):0.01:maximum(geometry.knot_v[1]) )
B = zeros(length(t))
for gb = 1:geometry.spans[1]+1
    for i = 1:length(t)
        e,a,xi = e_a_xi_for_gb_t(geometry,[gb],[t[i]])
        if(a[1]>0)
            B[i] = basis(geometry,e,a,xi)
        else
            B[i] = 0.0
        end
    end
    plt.plot(t,B)
end
plt.title("Global B-Spline basis functions")
plt.savefig(path_prefix*"simple_000123444_global_bspl_basisf.pdf")


# Plot the bernstein mesh basis functions:
plt.figure(figsize=figure_size)
t = collect(minimum(knot_v[1]):0.01:maximum(knot_v[1]))
for a = 1:4
    B = zeros(length(t))
    for i = 1:length(t)
        tval = t[i]
        e,xi = e_xi_for_t(bern,reshape([tval],1,1))
        B[i] = basis(bern,e,[a],xi)
    end
    plt.plot(t,B)
end
plt.title("Bernstein basis functions over each Bezier curve")
plt.savefig(path_prefix*"simple_000123444_bernstein_bezier_basisf.pdf")



##########
########## Building one more curve that's more interesting to look at.

begin
    dim_p = 1
    dim_s = 2
    p = [3]
    n = [7]
    spans = n .- 1
    knot_v = Array[[0., 0, 0, 0, 1, 2, 3, 4, 4, 4, 4]]
    c_pts = reshape([
            0., 0,
            1, 0,
            2.0, 0.75,
            1.5, 2,
            1.25, 3,
            1.5, 4,
            2, 5 ],2,7)
    weights = Array{Array{Float64}}(undef,1)
    weights[1] = Array{Float64}([1.,1.,1.,1.,1.,1.,1.])
    geometry = Nurbs(dim_p, dim_s, p, knot_v, spans, c_pts, weights)

    # Drawing the curve
    res = [100]
    X,t,h = draw(geometry,res)
    closeplots = false
    plt.figure(figsize=(5,5))
    plt.scatter(X[1,:],X[2,:],color="b")
    plt.scatter(geometry.control_pts[1,:],geometry.control_pts[2,:],color="r")
    #plt.xlim(-0.1,2.1)
    #plt.ylim(-0.5,1.5)
    plt.title("Curvy Geometry")
    plt.savefig(path_prefix*"simple_000123444_bspl_geometry_curvy.pdf")
end

##########
########## Building the Bezier-extracted version!
begin
    dim_p = 1
    dim_s = 2
    p = [3]
    n = [7]
    spans = n .- 1
    knot_v = Array[[0., 0, 0, 0, 1, 2, 3, 4, 4, 4, 4]]
    c_pts = reshape([
            0., 0,
            1, 0,
            2.0, 0.75,
            1.5, 2,
            1.25, 3,
            1.5, 4,
            2, 5 ],2,7)
    weights = Array{Array{Float64}}(undef,1)
    weights[1] = Array{Float64}([1.,1.,1.,1.,1.,1.,1.])
    geometry = Nurbs(dim_p, dim_s, p, knot_v, spans, c_pts, weights)
    bern = ConvertToBernsteinMesh(geometry)

    # Drawing the curve
    res = [100]
    X,t,h = draw(bern,res)
    closeplots = false
    plt.figure(figsize=(5,5))

    f1 = function(x)
       	x>=0.0 && x < 1.0
    end
    X1x = X[1,find(f1,t)]
	X1y = X[2,find(f1,t)]

	f2 = function(x)
       	x>=1.0 && x < 2.0
    end
	X2x = X[1,find(f2,t)]
	X2y = X[2,find(f2,t)]

	f3 = function(x)
       	x>=2.0 && x < 3.0
    end
	X3x = X[1,find(f3,t)]
	X3y = X[2,find(f3,t)]

	f4 = function(x)
       	x>=3.0 && x < 4.0
    end
	X4x = X[1,find(f4,t)]
	X4y = X[2,find(f4,t)]


    #plt.scatter(X[1,:],X[2,:],color="b")
	plt.scatter(X1x,X1y,color="g")
	plt.scatter(X2x,X2y,color="b")
	plt.scatter(X3x,X3y,color="y")
	plt.scatter(X4x,X4y,color="c")
    plt.scatter(bern.control_pts[1,:],bern.control_pts[2,:],color="r")
    #plt.xlim(-0.1,2.1)
    #plt.ylim(-0.5,1.5)
    plt.title("Bezier-Extracted Curvy Geometry")
    plt.savefig(path_prefix*"simple_000123444_bspl_geometry_bezex_curvy.pdf")
end
