# Example - 2D IGA
# Patch Test - Translation - Linear Elastic model

include("../sslinearelastic.jl")

using .SSLinearElastic

using .SSLinearElastic.SSIGA2D
import .SSLinearElastic.SSIGA2D.IGAGrid2D
import .SSLinearElastic.SSIGA2D.delBasis
import .SSLinearElastic.SSIGA2D.Solve
import .SSLinearElastic.SSIGA2D.GetStrainDispMatB
import .SSLinearElastic.SSIGA2D.solution

import .SSLinearElastic.SSIGA2D.SSNurbsTools.Nurbs
import .SSLinearElastic.SSIGA2D.SSNurbsTools.draw
import .SSLinearElastic.SSIGA2D.SSNurbsTools.basis
import .SSLinearElastic.SSIGA2D.SSNurbsTools.Jacobian

import .SSLinearElastic.SSIGA2D.SSGaussQuad.GaussQuadIntegrate
import .SSLinearElastic.SSIGA2D.SSGaussQuad.NumGaussPts

dirichletConstraint =
    function(self::IGAGrid2D,e::Array{Int64},a::Array{Int64},dofi::Int64)
        # Left edge
        if(e[1] == 1 && a[1] == 1)
            return true,0.1
        end
        # Right edge
        if(e[1] == self.nelements[1] && a[1] == self.nnodesperel[1])
            return true,0.1
        end
        # Botttom edge
        if(e[2] == 1 && a[2] == 1)
            return true,0.1
        end
        # Top edge
        if(e[2] == self.nelements[2] && a[2] == self.nnodesperel[2])
            return true,0.1
        end
        return false,0.0
    end

dim_p = 2
dim_s = 2
p = [2,2]
n = [3,3]
span = n .- 1
knot_v = Array[[0., 0, 0, 1, 1, 1],[0., 0, 0, 1, 1, 1]]
c_pts = zeros(dim_s,n[1],n[2])
c_pts[:,1,1] = [0.0, 0.0]
c_pts[:,2,1] = [0.5, 0.0]
c_pts[:,3,1] = [1.0, 0.0]
c_pts[:,1,2] = [0.0, 0.5]
c_pts[:,2,2] = [0.5, 0.5]
c_pts[:,3,2] = [1.0, 0.5]
c_pts[:,1,3] = [0.0, 1.0]
c_pts[:,2,3] = [0.5, 1.0]
c_pts[:,3,3] = [1.0, 1.0]
weights = Array{Array{Float64}}(undef,2)
weights[1] = Array{Float64}([1.,1.,1.])
weights[2] = Array{Float64}([1.,1.,1.])
geometry = Nurbs(dim_p, dim_s, p, knot_v, span, c_pts, weights)
YoungsModE = 5.0e2
PoissonsRatNu = 0.3


nel = [4,4]
ndofpernode = 2
self = IGAGrid2D(geometry,
                nel,
                ndofpernode,
                YoungsModE,
                PoissonsRatNu,
                dirichletConstraint,
                SSLinearElastic.neumannBCForElementFace_Zeros,
                SSLinearElastic.neumannBCDsForElementFace_Zeros,
                SSLinearElastic.bodyForceForElement_Zeros,
                SSLinearElastic.FextForElement,
                SSLinearElastic.FextDsForElement,
                SSLinearElastic.NForElement,
                SSLinearElastic.NDsForElement,
                SSLinearElastic.consistentTangentForElement,
                SSLinearElastic.consistentTangentDsForElement;
                nloadsteps=0)

Solve(self)

using PyPlot; plt = PyPlot;
closeplots = false

res = [25,25]
X,t,h = draw(self.mesh,res)
plt.figure()
plt.scatter(X[1,:],X[2,:],color="b")
plt.scatter(self.mesh.control_pts[1,:,:],self.mesh.control_pts[2,:,:],color="r")
plt.xlim(-0.1,2.1)
plt.ylim(-0.5,1.5)
plt.title("Geometry")
plt.savefig("plots_ignore/plot_2D_patchtest_geom01.pdf")
if(closeplots)
    plt.close()
end

newmesh = Nurbs(self.mesh)
newmesh.control_pts = newmesh.control_pts + self.d
res = [25,25]
X,t,h = draw(newmesh,res)
plt.figure()
plt.scatter(X[1,:],X[2,:],color="b")
plt.scatter(newmesh.control_pts[1,:,:],newmesh.control_pts[2,:,:],color="r")
plt.xlim(-0.1,2.1)
plt.ylim(-0.5,1.5)
plt.title("With Deflection")
plt.savefig("plots_ignore/plot_2D_patchtest_geom02.pdf")
if(closeplots)
    plt.close()
end

X,Y,F = solution(self,1,[20,20])
plt.figure()
plt.pcolor(X,Y,F)
plt.colorbar()
plt.title("disp_x")
plt.savefig("plots_ignore/plot_2D_patchtest_disp01.pdf")

X,Y,F = solution(self,2,[20,20])
plt.figure()
plt.pcolor(X,Y,F)
plt.colorbar()
plt.title("disp_y")
plt.savefig("plots_ignore/plot_2D_patchtest_disp02.pdf")
