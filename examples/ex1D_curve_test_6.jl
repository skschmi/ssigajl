
include("../ssnurbstoolsjl/nurbstools.jl")

using .SSNurbsTools
import .SSNurbsTools.Nurbs
import .SSNurbsTools.draw
import .SSNurbsTools.basis
import .SSNurbsTools.Jacobian
import .SSNurbsTools.e_xi_for_t
import .SSNurbsTools.domain_X
import .SSNurbsTools.domain_dXdt

dim_p = 1
dim_s = 2
p = [3]
n = [4]
span = n .- 1

knot_v1 = Array[[0.,0,0,0,1,1,1,1]]
knot_v2 = Array[[0.,0,0,0,2,2,2,2]]

c_pts = zeros(dim_s,n[1])
c_pts[:,1] = [ 0., 0.]
c_pts[:,2] = [ 3., 4.]
c_pts[:,3] = [ 9., 2.]
c_pts[:,4] = [12.,10.]
weights = Array{Array{Float64}}(undef,1)
weights[1] = Array{Float64}([1.,1.,1.,1.])
bez1 = Nurbs(dim_p, dim_s, p, knot_v1, span, c_pts, weights)
bez2 = Nurbs(dim_p, dim_s, p, knot_v2, span, c_pts, weights)
@show bez1.knot_v
@show bez2.knot_v

# Eval first curve
t = reshape([0.1],1,1)
deriv0_result = domain_X(bez1,t)
deriv1_result = domain_dXdt(bez1,t)
println("bezier 1, eval at t=",t,":")
@show deriv0_result
@show deriv1_result

# Eval second curve
t = reshape([0.2],1,1)
deriv0_result = domain_X(bez2,t)
deriv1_result = domain_dXdt(bez2,t)
println("bezier 2, eval at t=",t,":")
@show deriv0_result
@show deriv1_result

using PyPlot; plt = PyPlot;

begin
    self = bez1
    res = [50]
    X,t,h = draw(self,res)
    plt.figure()
    plt.scatter(X[1,:],X[2,:],color="b")
    plt.scatter(self.control_pts[1,:],self.control_pts[2,:],color="r")
    plt.xlim(-5,20)
    plt.ylim(-5,15)
    plt.title("bez1")
    bmesh = Nurbs(self)
end

begin
    self = bez2
    res = [50]
    X,t,h = draw(self,res)
    plt.figure()
    plt.scatter(X[1,:],X[2,:],color="b")
    plt.scatter(self.control_pts[1,:],self.control_pts[2,:],color="r")
    plt.xlim(-5,20)
    plt.ylim(-5,15)
    plt.title("bez1")
    @show self.knot_v
    bmesh = Nurbs(self)
end
