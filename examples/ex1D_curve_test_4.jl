#=
The purpose of this test is to try to fix the problem where
the bezier-extraction-operator method of plotting a basis function
fails to properly plot the case where the first control point polar-label
is not all the same number.

Aka, the first control point is
0 0 2
instead of the usual
0 0 0

with a n+p+1 knot vector
0 0 0 2 3 4 4 4 4
the first control point 0 0 2 should have a basis function that
goes from 0 until 3, but right now things fail, and we need to figure out
how to fix it.

=#



include("../ssnurbstoolsjl/nurbstools.jl")
using .SSNurbsTools
import .SSNurbsTools.Nurbs
import .SSNurbsTools.draw
import .SSNurbsTools.basis
import .SSNurbsTools.Jacobian
import .SSNurbsTools.e_xi_for_t
import .SSNurbsTools.domain_X
import .SSNurbsTools.ConvertToBernsteinMesh
import .SSNurbsTools.e_a_xi_for_gb_t

using Statistics

dim_p = 1
dim_s = 1
p = [3]
n = [5]
span = n .- 1
knot_v = Array[[0.,   0.,0.,2.,3.,4.,4.,4.,   4,]]
c_pts = reshape([mean(knot_v[1][2:4]),
                    mean(knot_v[1][3:5]),
                    mean(knot_v[1][4:6]),
                    mean(knot_v[1][5:7]),
                    mean(knot_v[1][6:8]),],1,5)
#c_pts = reshape([1.0,2.0,3.0,4.0,5.0],1,5)
weights = Array{Array{Float64}}(undef,1)
weights[1] = Array{Float64}([1.,1.,1.,1.,1.])
nurb_1 = Nurbs(dim_p, dim_s, p, knot_v, span, c_pts, weights)
bern_1 = Nurbs(nurb_1)
ConvertToBernsteinMesh(bern_1)


dim_p = 1
dim_s = 1
p = [3]
n = [6]
span = n .- 1
knot_v = Array[[0.,   0.,0.,0.,2.,3.,4.,4.,4.,   4,]]
c_pts = reshape([mean(knot_v[1][2:4]),
                    mean(knot_v[1][3:5]),
                    mean(knot_v[1][4:6]),
                    mean(knot_v[1][5:7]),
                    mean(knot_v[1][6:8]),
                    mean(knot_v[1][6:8]),],1,6)
#c_pts = reshape([1.0,2.0,3.0,4.0,5.0,6.0],1,6)
weights = Array{Array{Float64}}(undef,1)
weights[1] = Array{Float64}([1.,1.,1.,1.,1.,1.])
nurb_2 = Nurbs(dim_p, dim_s, p, knot_v, span, c_pts, weights)
bern_2 = Nurbs(nurb_2)
ConvertToBernsteinMesh(bern_2)


using PyPlot; plt = PyPlot;

function plot_nurb(nurb,name)
    # Plotting the geometry's basis functions
    plt.figure()
    t = collect(minimum(nurb.knot_v[1]):0.01:maximum(nurb.knot_v[1]))
    for a = 1:4
        B = zeros(length(t))
        for i = 1:length(t)
            tval = t[i]
            e,xi = e_xi_for_t(nurb,reshape([tval],1,1))
            B[i] = basis(nurb,e,[a],xi)
        end
        plt.plot(t,B)
    end
    plt.title(name*": Geometry Basis Functions")

    # Plot the global basis functions separately (instead of element-by-element)
    plt.figure()
    t = collect( minimum(nurb.knot_v[1]):0.01:maximum(nurb.knot_v[1]) )
    B = zeros(length(t))
    for gb = 1:nurb.spans[1]+1
        for i = 1:length(t)
            e,a,xi = e_a_xi_for_gb_t(nurb,[gb],[t[i]])
            #@show e,t[i],xi
            if(a[1]>0)
                B[i] = basis(nurb,e,a,xi)
            else
                B[i] = 0.0
            end
        end
        plt.plot(t,B)
    end
    plt.title(name*": Global Basis functions for the 1D Nurbs")
end

function plot_bern(bern,name)
    # Plot the bernstein mesh basis functions:
    plt.figure()
    t = collect(minimum(bern.knot_v[1]):0.01:maximum(bern.knot_v[1]))
    for a = 1:4
        B = zeros(length(t))
        for i = 1:length(t)
            tval = t[i]
            e,xi = e_xi_for_t(bern,reshape([tval],1,1))
            B[i] = basis(bern,e,[a],xi)
        end
        plt.plot(t,B)
    end
    plt.title(name*": Bernstein Geometry Basis Functions")
end

function draw_curve(nurb,name)
    # Drawing the curve
    res = [50]
    X,t,h = draw(nurb,res)
    closeplots = false
    plt.figure()
    plt.scatter(X[1,:],zeros(length(X)),color="b")
    plt.scatter(nurb.control_pts[1,:],zeros(length(nurb.control_pts)),color="r")
    #plt.xlim(-0.1,2.1)
    #plt.ylim(-0.5,1.5)
    plt.title(name*": Geometry")
end

plot_nurb(nurb_1,"Curve_1")
plot_bern(bern_1,"Curve_1")
draw_curve(nurb_1,"Curve_1 nurb")
draw_curve(bern_1,"Curve_1 bern")

plot_nurb(nurb_2,"Curve_2")
plot_bern(bern_2,"Curve_2")
draw_curve(nurb_2,"Curve_2 nurb")
draw_curve(bern_2,"Curve_2 bern")
