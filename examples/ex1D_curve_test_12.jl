
include("../ssnurbstoolsjl/nurbstools.jl")
using .SSNurbsTools
import .SSNurbsTools.Nurbs
import .SSNurbsTools.draw
import .SSNurbsTools.basis
import .SSNurbsTools.Jacobian
import .SSNurbsTools.e_xi_for_t
import .SSNurbsTools.domain_X

dim_p = 1
dim_s = 2
p = [3]
n = [5]
span = n .- 1
knot_v = Array[[0.,    0, 0, 0, 0.5, 1, 1, 1,     1]]
c_pts = zeros(dim_s,n[1])
c_pts[:,1] = [0.,0.]
c_pts[:,2] = [0.,1.]
c_pts[:,3] = [1.,1.]
c_pts[:,4] = [2.,1.]
c_pts[:,5] = [2.,0.]
weights = Array{Array{Float64}}(undef,1)
weights[1] = Array{Float64}([1.,  sqrt(2)/3,  1.,   sqrt(2)/3,  1.])
geometry = Nurbs(dim_p, dim_s, p, knot_v, span, c_pts, weights)



res = [50]
X,t,h = draw(geometry,res)

using PyPlot; plt = PyPlot;
closeplots = false

plt.figure()
#plt.scatter(X[1,:],X[2,:],color="b")
plt.plot(X[1,:],X[2,:],color="b")
plt.scatter(geometry.control_pts[1,:],geometry.control_pts[2,:],color="r")
#plt.xlim(-0.1,2.1)
#plt.ylim(-0.5,1.5)
plt.title("Geometry")
#plt.savefig("plots_ignore/plot_2Dprob_geom02.pdf")

e,xi = e_xi_for_t(geometry,[1/3])
xval = domain_X(geometry,e,reshape(xi,1,1))
@show xval
