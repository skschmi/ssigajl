# Example - 2D IGA
# Basic 2D linear elastic problem in IGA

# My First Optimization problem - 1x1 elements, Beam, pinned on left side.

include("../sslinearelastic.jl")

using .SSLinearElastic

using .SSLinearElastic.SSIGA2D
import .SSLinearElastic.SSIGA2D.IGAGrid2D
import .SSLinearElastic.SSIGA2D.delBasis
import .SSLinearElastic.SSIGA2D.Solve
import .SSLinearElastic.SSIGA2D.GetStrainDispMatB
import .SSLinearElastic.SSIGA2D.GetStrainDispMatB_ds
import .SSLinearElastic.SSIGA2D.solution

using .SSLinearElastic.SSIGA2D.SSNurbsTools
import .SSLinearElastic.SSIGA2D.SSNurbsTools.Nurbs
import .SSLinearElastic.SSIGA2D.SSNurbsTools.draw
import .SSLinearElastic.SSIGA2D.SSNurbsTools.basis
import .SSLinearElastic.SSIGA2D.SSNurbsTools.basis_ds
import .SSLinearElastic.SSIGA2D.SSNurbsTools.Jacobian
import .SSLinearElastic.SSIGA2D.SSNurbsTools.Jacobian_ds
import .SSLinearElastic.SSIGA2D.SSNurbsTools.domain_dXdsdxi

import .SSLinearElastic.SSIGA2D.SSGaussQuad.GaussQuadIntegrate
import .SSLinearElastic.SSIGA2D.SSGaussQuad.NumGaussPts

using Printf

dirichletConstraint =
    function(self::IGAGrid2D,e::Array{Int64},a::Array{Int64},dofi::Int64)
        if(e[1] == 1 && a[1] == 1)
            return true,0.0
        end
        return false,0.0
    end

neumannBCForElementFace =
    function(self::IGAGrid2D,e::Array{Int64},face::Tuple{Int64,Int64},xi::Array{Float64})
        # This returns the tractions for the specified face of the element, at position xi
        # Paramters:
        # e: Element index
        # face[1]: Parameter direction: 1={xi=-1,xi=1} faces.  2={eta=-1,eta=1} faces
        # face[2]: bottom or top face:  1={xi=-1 face} 2={xi=1 face}
        # xi: The xi,eta coordinates along the face we are looking at
        # Returns:
        # x_dir_traction, y_dir_traction
        if(e[2] == 1 && face[1] == 2 && face[2] == 1)
            return 0.0,-3.0
        end
        return 0.0,0.0
    end

bodyForceForElement =
    function(self::IGAGrid2D,e::Array{Int64})
        # Returns g_x, g_y
        return 0.0, -20.0
    end



dim_p = 2
dim_s = 2
p = [2,2]
n = [3,3]
span = n .- 1
knot_v = Array[[0., 0, 0, 1, 1, 1],[0., 0, 0, 1, 1, 1]]

#weights = Array{Array{Float64}}(undef,2)
#for i = 1:length(weights)
#    weights[i] = ones(3)
#end

weights = Array{Array{Function}}(undef,2)
for i = 1:length(weights)
    weights[i] = Array{Function}(undef,3)
end
weights[1][1] = function(s) 1. end
weights[1][2] = function(s)
            (1.0-s[1])+1.0
        end
weights[1][3] = function(s) 1. end
weights[2][1] = function(s) 1. end
weights[2][2] = function(s)
            (1.0-s[1])+1.0
        end
weights[2][3] = function(s) 1. end




c_pts = Array{Function}(undef,dim_s,n[1],n[2])

#************ c_pts[:,1,1] = [0.,0.0]
c_pts[1,1,1] = function(s) 0. end
c_pts[2,1,1] = function(s) 0. end

#************ c_pts[:,2,1] = [1.,0.0]
c_pts[1,2,1] = function(s) 1. end
c_pts[2,2,1] = function(s) 0. end

#************ c_pts[:,3,1] = [2.,0.0]
c_pts[1,3,1] = function(s) 2. end
c_pts[2,3,1] = function(s) 0. end

#************ c_pts[:,1,2] = [0.,0.5]
c_pts[1,1,2] = function(s) 0. end
c_pts[2,1,2] = function(s) 0.5 end

#************ c_pts[:,2,2] = [1.,0.5]
c_pts[1,2,2] = function(s) 1. end
c_pts[2,2,2] = function(s)
            (s[1] + 0.5*(1.0-s[1]))*0.5
        end

#************ c_pts[:,3,2] = [2.,0.5]
c_pts[1,3,2] = function(s) 2. end
c_pts[2,3,2] = function(s)
            s[1]*0.5
        end

#************ c_pts[:,1,3] = [0.,1.0]
c_pts[1,1,3] = function(s) 0. end
c_pts[2,1,3] = function(s) 1. end

#************ c_pts[:,2,3] = [1.,1.0]
c_pts[1,2,3] = function(s) 1. end
c_pts[2,2,3] = function(s)
            (s[1]+0.5*(1.0-s[1]))*1.0
        end

#************ c_pts[:,3,3] = [2.,1.0]
c_pts[1,3,3] = function(s) 2. end
c_pts[2,3,3] = function(s)
    s[1]*1.0
end




self = nothing

values_h = 0.01
#values = 0.30:values_h:1.0
values = 1.0:-values_h:0.30

#values_h = 1.0e-9
#values = [0.7,0.7+values_h]

# Check the displacement_ds
disp_arr = zeros(length(values))
disp_ds_arr = zeros(length(values))
disp_ds_fd_arr = zeros(length(values))

#Check the K_ds matrix
Kmat_arr = Array{Any}(undef,length(values))
Kmat_ds_arr = Array{Any}(undef,length(values))

#Check the dx_ds_dt function
dx_dxi_arr = Array{Any}(undef,length(values))
dx_ds_dxi_arr = Array{Any}(undef,length(values))

#Check the nurbs_basis_ds function
nurbsbasis_arr = Array{Any}(undef,length(values))
nurbsbasis_ds_arr = Array{Any}(undef,length(values))

#Check the nurbs_basis_ds 2D function
nurbsbasis2D_arr = Array{Any}(undef,length(values))
nurbsbasis2D_ds_arr = Array{Any}(undef,length(values))

#Check the del_nurbs_basis_ds_dt function
delnurbsbasis_arr = Array{Any}(undef,length(values))
delnurbsbasis_ds_arr = Array{Any}(undef,length(values))

#Check the del_nurbs_basis_ds_dt 2D function
delnurbsbasis2D_arr = Array{Any}(undef,length(values))
delnurbsbasis2D_ds_arr = Array{Any}(undef,length(values))

for i = 1:length(values)
    shape_p = values[i]

    geometry = Nurbs(dim_p, dim_s, p, knot_v, span, c_pts, weights; s=Array{Float64}([shape_p]))
    YoungsModE = 1.0e4
    PoissonsRatNu = 0.3
    nel = [1,1]
    ndofpernode = 2
    global self
    self = IGAGrid2D(geometry,
                    nel,
                    ndofpernode,
                    YoungsModE,
                    PoissonsRatNu,
                    dirichletConstraint,
                    neumannBCForElementFace,
                    SSLinearElastic.neumannBCDsForElementFace_Zeros,
                    bodyForceForElement,
                    SSLinearElastic.FextForElement,
                    SSLinearElastic.FextDsForElement,
                    SSLinearElastic.NForElement,
                    SSLinearElastic.NDsForElement,
                    SSLinearElastic.consistentTangentForElement,
                    SSLinearElastic.consistentTangentDsForElement;
                    nloadsteps=0,
                    runtimeoutput=false)
    GC.gc()
    Solve(self)

    # Check the displacement_ds
    disp = self.d[2,3,2]
    disp_ds = self.d_ds[1][2,3,2]
    disp_arr[i] = disp
    disp_ds_arr[i] = disp_ds
    if(i > 1)
        disp_ds_fd_arr[i] = (disp_arr[i]-disp_arr[i-1])/(values[i]-values[i-1])
    end
    @printf "%0.16f    %0.8f     %0.8f     %0.8f     %0.8f\n" shape_p disp disp_ds disp_ds_fd_arr[i] (disp_ds-disp_ds_fd_arr[i])

    # Check dx_ds_dt function
    dx_dxi_arr[i] = SSNurbsTools.domain_dXdxi(self.mesh,[1,1],reshape([0.345,0.123],2,1))
    dx_ds_dxi_arr[i] = SSNurbsTools.domain_dXdsdxi(self.mesh,[1,1],reshape([0.345,0.123],2,1),1)

    # Check the K_ds matrix
    Kmat_arr[i] = copy(self.K)
    Kmat_ds_arr[i] = copy(self.K_ds[1])

    # Check nurbs_basis_ds
    e = [1,1]
    C_e = SSNurbsTools.bezExtOpForEl(self.mesh,e)
    control_pts_e = SSNurbsTools.controlPtsForEl(self.mesh,e,self.mesh.control_pts)
    control_pts_e_ds = SSNurbsTools.controlPtsForEl(self.mesh,e,self.mesh.control_pts_ds[1])
    weights_e = SSNurbsTools.weightsForEl(self.mesh,e)
    weights_e_ds = SSNurbsTools.weightsForEl_ds(self.mesh,e,1)
    nurbsbasis_arr[i] = SSNurbsTools.nurbs_basis(1,self.mesh.degree[2],C_e[2],weights_e[2],0.345)
    nurbsbasis_ds_arr[i] = SSNurbsTools.nurbs_basis_ds(1,self.mesh.degree[2],C_e[2],weights_e[2],weights_e_ds[2],0.345)

    # Check nurbs_basis_ds 2D
    nurbsbasis2D_arr[i] = SSNurbsTools.nurbs_basis([1,1],
                                                    self.mesh.degree,
                                                    C_e,
                                                    weights_e,
                                                    [0.345,0.123])
    nurbsbasis2D_ds_arr[i] = SSNurbsTools.nurbs_basis_ds([1,1],
                                                          self.mesh.degree,
                                                          C_e,
                                                          weights_e,
                                                          weights_e_ds,
                                                          [0.345,0.123])

    # Check del_nurbs_basis_ds_dt
    delnurbsbasis_arr[i] = SSNurbsTools.del_nurbs_basis(1,self.mesh.degree[2],C_e[2],weights_e[2],0.345)
    delnurbsbasis_ds_arr[i] = SSNurbsTools.del_nurbs_basis_ds(1,self.mesh.degree[2],C_e[2],weights_e[2],weights_e_ds[2],0.345)


    # Check del_nurbs_basis_ds_dt 2D
    cpsup = Array{Array{}}(undef,2)
    cpsup[1] = collect(1:self.mesh.spans[1]+1)
    cpsup[2] = collect(1:self.mesh.spans[2]+1)
    delnurbsbasis2D_arr[i] = SSNurbsTools.del_nurbs_basis([1,1],
                                                    self.mesh.degree,
                                                    C_e,
                                                    weights_e,
                                                    [0.345,0.123])
    delnurbsbasis2D_ds_arr[i] = SSNurbsTools.del_nurbs_basis_ds([1,1],
                                                          self.mesh.degree,
                                                          C_e,
                                                          weights_e,
                                                          weights_e_ds,
                                                          [0.345,0.123])


end

showtests = false
if(showtests)
    #######################################
    #### View the checked K_ds matrix #####
    #######################################
    K_ds_fd = (Kmat_arr[2]-Kmat_arr[1])./values_h
    K_ds_calc = Kmat_ds_arr[1]
    K_ds_error = K_ds_fd - K_ds_calc
    println("full(K_ds_error) = ")
    println(full(K_ds_error))
    println("")

    #############################################
    #### View the checked dx_ds_dt function #####
    #############################################
    dx_ds_dxi_fd = (dx_dxi_arr[2] - dx_dxi_arr[1])/values_h
    dx_ds_dxi_calc = dx_ds_dxi_arr[1]
    dx_ds_dxi_error = dx_ds_dxi_fd - dx_ds_dxi_calc
    @show dx_ds_dxi_fd
    @show dx_ds_dxi_calc
    @show dx_ds_dxi_error
    println("")

    ###################################################
    #### View the checked nurbs_basis_ds function #####
    ###################################################
    nurbsbasis_ds_fd = (nurbsbasis_arr[2] - nurbsbasis_arr[1])/values_h
    nurbsbasis_ds_calc = nurbsbasis_ds_arr[1]
    nurbsbasis_ds_error = nurbsbasis_ds_fd - nurbsbasis_ds_calc
    @show nurbsbasis_ds_fd
    @show nurbsbasis_ds_calc
    @show nurbsbasis_ds_error
    println("")

    ######################################################
    #### View the checked nurbs_basis_ds 2D function #####
    ######################################################
    nurbsbasis2D_ds_fd = (nurbsbasis2D_arr[2] - nurbsbasis2D_arr[1])/values_h
    nurbsbasis2D_ds_calc = nurbsbasis2D_ds_arr[1]
    nurbsbasis2D_ds_error = nurbsbasis2D_ds_fd - nurbsbasis2D_ds_calc
    @show nurbsbasis2D_ds_fd
    @show nurbsbasis2D_ds_calc
    @show nurbsbasis2D_ds_error
    println("")

    ##########################################################
    #### View the checked del_nurbs_basis_ds_dt function #####
    ##########################################################
    delnurbsbasis_ds_fd = (delnurbsbasis_arr[2] - delnurbsbasis_arr[1])/values_h
    delnurbsbasis_ds_calc = delnurbsbasis_ds_arr[1]
    delnurbsbasis_ds_error = delnurbsbasis_ds_fd - delnurbsbasis_ds_calc
    @show delnurbsbasis_ds_fd
    @show delnurbsbasis_ds_calc
    @show delnurbsbasis_ds_error
    println("")

    ##########################################################
    #### View the checked del_nurbs_basis_ds_dt 2D function #####
    ##########################################################
    delnurbsbasis2D_ds_fd = (delnurbsbasis2D_arr[2] - delnurbsbasis2D_arr[1])/values_h
    delnurbsbasis2D_ds_calc = delnurbsbasis2D_ds_arr[1]
    delnurbsbasis2D_ds_error = delnurbsbasis2D_ds_fd - delnurbsbasis2D_ds_calc
    @show delnurbsbasis2D_ds_fd
    @show delnurbsbasis2D_ds_calc
    @show delnurbsbasis2D_ds_error
    println("")
end


using PyPlot; plt = PyPlot;
closeplots = false

plt.plot(collect(values),disp_arr)
plt.legend(["Displacement"])
plt.xlabel("Design parameter (s)")
plt.ylabel("y-displacement at node (3,2)")
plt.title("Maximum y-displacement vs. design parameter (s)")
plt.savefig("plots_ignore/plot_2Dprob_disp-vs-shapep.pdf")

plt.figure()
plt.plot(collect(values),disp_ds_arr,"r")
plt.plot(collect(values)[2:end],disp_ds_fd_arr[2:end],"g")
plt.legend(["Analytical","Finite Diff"])
plt.xlabel("Design parameter (s)")
plt.ylabel("dd/ds")
plt.title("Derivative of y-displacement w.r.t. design parameter (s)")
plt.savefig("plots_ignore/plot_2Dprob_dd_ds_finitediff_vs_analytical.pdf")

res = [50,25]
X,t,h = draw(self.mesh,res)
plt.figure()
plt.scatter(X[1,:],X[2,:],color="b")
plt.scatter(self.mesh.control_pts[1,:,:],self.mesh.control_pts[2,:,:],color="r")
plt.xlim(-0.1,2.1)
plt.ylim(-0.5,1.5)
plt.title("Geometry")
plt.savefig("plots_ignore/plot_2Dprob_geom02.pdf")
if(closeplots)
    plt.close()
end

newmesh = Nurbs(self.mesh)
newmesh.control_pts = newmesh.control_pts + self.d
res = [50,25]
X,t,h = draw(newmesh,res)
plt.figure()
plt.scatter(X[1,:],X[2,:],color="b")
plt.scatter(newmesh.control_pts[1,:,:],newmesh.control_pts[2,:,:],color="r")
plt.xlim(-0.1,2.1)
plt.ylim(-0.5,1.5)
plt.title("With Deflection")
plt.savefig("plots_ignore/plot_2Dprob_geom02b.pdf")
if(closeplots)
    plt.close()
end

X,Y,F = solution(self,1,[30,30])
plt.figure()
plt.pcolor(X,Y,F)
plt.colorbar()
plt.title("disp_x")
plt.savefig("plots_ignore/plot_2Dprob_disp01.pdf")

X,Y,F = solution(self,2,[30,30])
plt.figure()
plt.pcolor(X,Y,F)
plt.colorbar()
plt.title("disp_y")
plt.savefig("plots_ignore/plot_2Dprob_disp02.pdf")
