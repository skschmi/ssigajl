
# include("../sslinearelastic.jl")
# # using .SSLinearElastic
# # using .SSIGA2D
# # import .SSIGA2D.IGAGrid2D
# # import .SSIGA2D.delBasis
# # import .SSIGA2D.Solve
# # import .SSIGA2D.GetStrainDispMatB
# # import .SSIGA2D.solution

include("../ssnurbstoolsjl/nurbstools.jl")
import .SSNurbsTools.Nurbs
import .SSNurbsTools.draw
import .SSNurbsTools.basis
import .SSNurbsTools.Jacobian
import .SSNurbsTools.domain_X
import .SSNurbsTools.domain_dXdxi
import .SSNurbsTools.domain_dXdsdxi
import .SSNurbsTools.InsertKnot

# import .SSGaussQuad.GaussQuadIntegrate
# import .SSGaussQuad.NumGaussPts


# Problem set-up
dim_p = 1
dim_s = 2
p = [2]
n = [3]
span = n .- 1
knot_v = Array[[0.0, 0, 0, 1, 1, 1]]

dom_w = 1.0
dom_h = 1.0

c_pts = Array{Function}(undef,dim_s,n[1])

#### c_pts[:,1,1] = [0.0, 0.0]
c_pts[1,1] = function(s)
    return 0.75*dom_w
end
c_pts[2,1] = function(s)
    return 0.0
end

#### c_pts[:,2,1] = [0.0, 1.0]
c_pts[1,2] = function(s)
    return 0.75*dom_w
end
c_pts[2,2] = function(s)
    return 0.25*dom_h
end

#### c_pts[:,3,1] = [1.0, 1.0]
c_pts[1,3] = function(s)
    return dom_w
end
c_pts[2,3] = function(s)
    return 0.25*dom_h
end

## ******************************************************
##---- Weights for the B-spline inexact geometry ------
## ******************************************************

# Init the weights
weights = Array{Array{Function}}(undef,1)
weights[1] = Array{Function}(undef,3)
# Set the weights
# First dir
weights[1][1] = function(s) 1.0 end
weights[1][2] = function(s)
            return 1.0
        end
weights[1][3] = function(s) 1.0 end

self = Nurbs(dim_p, dim_s, p, knot_v, span, c_pts, weights)

## ******************************************************
##---- Weights for the NURBS exact circle geometry ------
## ******************************************************

# Init the weights
weights2 = Array{Array{Function}}(undef,1)
weights2[1] = Array{Function}(undef,3)
# Set the weights
# First dir
weights2[1][1] = function(s) 1.0 end
weights2[1][2] = function(s)
            return 1.0/sqrt(2)
        end
weights2[1][3] = function(s) 1.0 end

self2 = Nurbs(dim_p, dim_s, p, knot_v, span, c_pts, weights2)





using PyPlot; plt = PyPlot;
closeplots = false
figure_size = (4.95, 5)

plt.figure(figsize=figure_size)
res = [80,80]
#### PLOT THE INEXACT B-SPLINE
X,t,h = draw(self,res)
plt.plot(X[1,:],X[2,:],color="b")
plt.scatter(self.control_pts[1,:],self.control_pts[2,:],color="r")
#### PLOT THE EXACT NURBS CIRCLE
X,t,h = draw(self2,res)
plt.plot(X[1,:],X[2,:],color="g")
plt.scatter(self2.control_pts[1,:],self2.control_pts[2,:],color="orange")
#### FINISH
# plt.xlim(-(7/16)*dom_w,(23/16)*dom_w)
# plt.ylim(-(1/4)*dom_h,(5/4)*dom_h)
plt.xlabel("x")
plt.ylabel("y")
plt.title("Geometry")

# REFINE
InsertKnot(self,1,0.5)
InsertKnot(self2,1,0.5)

plt.figure(figsize=figure_size)
res = [80,80]
#### PLOT THE INEXACT B-SPLINE
X,t,h = draw(self,res)
plt.plot(X[1,:],X[2,:],color="b")
plt.scatter(self.control_pts[1,:],self.control_pts[2,:],color="r")
#### PLOT THE EXACT NURBS CIRCLE
X,t,h = draw(self2,res)
plt.plot(X[1,:],X[2,:],color="g")
plt.scatter(self2.control_pts[1,:],self2.control_pts[2,:],color="orange")
#### FINISH
# plt.xlim(-(7/16)*dom_w,(23/16)*dom_w)
# plt.ylim(-(1/4)*dom_h,(5/4)*dom_h)
plt.xlabel("x")
plt.ylabel("y")
plt.title("Geometry 2")
