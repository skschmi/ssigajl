include("../sslinearelastic.jl")
using .SSLinearElastic
using .SSLinearElastic.SSIGA2D
import .SSLinearElastic.SSIGA2D.IGAGrid2D
import .SSLinearElastic.SSIGA2D.delBasis
import .SSLinearElastic.SSIGA2D.Solve
import .SSLinearElastic.SSIGA2D.GetStrainDispMatB
import .SSLinearElastic.SSIGA2D.solution
import .SSLinearElastic.SSIGA2D.SSNurbsTools.Nurbs
import .SSLinearElastic.SSIGA2D.SSNurbsTools.draw
import .SSLinearElastic.SSIGA2D.SSNurbsTools.basis
import .SSLinearElastic.SSIGA2D.SSNurbsTools.Jacobian
import .SSLinearElastic.SSIGA2D.SSNurbsTools.domain_X
import .SSLinearElastic.SSIGA2D.SSNurbsTools.domain_dXdxi
import .SSLinearElastic.SSIGA2D.SSNurbsTools.domain_dXdsdxi
import .SSLinearElastic.SSIGA2D.SSGaussQuad.GaussQuadIntegrate
import .SSLinearElastic.SSIGA2D.SSGaussQuad.NumGaussPts
using NLopt
using LinearAlgebra

dirichletConstraint =
    function(self::IGAGrid2D,e::Array{Int64},a::Array{Int64},dofi::Int64)
        # left side (left side of xi) restricted in the y-dir
        if(e[1] == 1 && a[1] == 1 && (dofi == 1 || dofi == 2))
            return true,0.0
        end
        return false,0.0
    end

neumannBCForElementFace =
    function(self::IGAGrid2D,e::Array{Int64},face::Tuple{Int64,Int64},xi::Array{Float64})
        # This returns the tractions for the specified face of the element, at position xi
        # Paramters:
        # e: Element index
        # face[1]: Parameter direction: 1={xi=-1,xi=1} faces.  2={eta=-1,eta=1} faces
        # face[2]: bottom or top face:  1={xi=-1 face} 2={xi=1 face}
        # xi: The xi,eta coordinates along the face we are looking at
        # Returns:
        # x_dir_traction, y_dir_traction

        # Bottom side of the last element to the right
        if( e[1] == self.nelements[1] &&
            e[2] == 1 &&
            face[1] == 2 &&
            face[2] == 1)

            return 0.0,-10.0
        end
        return 0.0,0.0
    end

# Problem set-up
dim_p = 2
dim_s = 2
p = [2,2]
n = [3,3]
span = n .- 1
knot_v = Array[[0., 0, 0, 1, 1, 1],[0., 0, 0, 1, 1, 1]]
YoungsModE = 210.0e2
PoissonsRatNu = 0.3
nel = [4,2]
ndofpernode = 2


#c_pts = zeros(dim_s,n[1],n[2])
c_pts = Array{Function}(undef,dim_s,n[1],n[2])

#### c_pts[:,1,1] = [3.0, 0.0]
c_pts[1,1,1] = function(s)
    return 0.0
end
c_pts[2,1,1] = function(s)
    return 0.0
end
#### c_pts[:,2,1] = [3.0, 0.3]
c_pts[1,2,1] = function(s)
    return 15.0
end
c_pts[2,2,1] = function(s)
    return 0.0
end
#### c_pts[:,3,1] = [3.7, 1.0]
c_pts[1,3,1] = function(s)
    return 30.0
end
c_pts[2,3,1] = function(s)
    return 0.0
end



#### c_pts[:,1,2] = [1.5, 0.0]
c_pts[1,1,2] = function(s)
    return 0.0
end
c_pts[2,1,2] = function(s)
    return s[1]/2
end
#### c_pts[:,2,2] = [1.5, 0.7]
c_pts[1,2,2] = function(s)
    return 15.0
end
c_pts[2,2,2] = function(s)
    return s[2]/2
end
#### c_pts[:,3,2] = [3.3, 2.5]
c_pts[1,3,2] = function(s)
    return 30.0
end
c_pts[2,3,2] = function(s)
    return s[3]/2
end



#### c_pts[:,1,3] = [0.0, 0.0]
c_pts[1,1,3] = function(s)
    return 0.0
end
c_pts[2,1,3] = function(s)
    return s[1]
end
#### c_pts[:,2,3] = [0.0, 4.0]
c_pts[1,2,3] = function(s)
    return 15.0
end
c_pts[2,2,3] = function(s)
    return s[2]
end
#### c_pts[:,3,3] = [0.0, 4.0]
c_pts[1,3,3] = function(s)
    return 30.0
end
c_pts[2,3,3] = function(s)
    return s[3]
end



# Init the weights
weights = Array{Array{Function}}(undef,2)
weights[1] = Array{Function}(undef,3)
weights[2] = Array{Function}(undef,3)
# Set the weights
# First dir
weights[1][1] = function(s)
    return 1.
end
weights[1][2] = function(s)
    return 1.
end
weights[1][3] = function(s)
    return 1.
end
# Second dir
weights[2][1] = function(s) 1. end
weights[2][2] = function(s) 1. end
weights[2][3] = function(s) 1. end

self = nothing

function create_geometry_and_solve(shape_params)
    global self
    self = IGAGrid2D(Nurbs(dim_p, dim_s, p, knot_v, span, c_pts, weights; s=shape_params),
                    nel,
                    ndofpernode,
                    YoungsModE,
                    PoissonsRatNu,
                    dirichletConstraint,
                    neumannBCForElementFace,
                    SSLinearElastic.neumannBCDsForElementFace_Zeros,
                    SSLinearElastic.bodyForceForElement_Zeros,
                    SSLinearElastic.FextForElement,
                    SSLinearElastic.FextDsForElement,
                    SSLinearElastic.NForElement,
                    SSLinearElastic.NDsForElement,
                    SSLinearElastic.consistentTangentForElement,
                    SSLinearElastic.consistentTangentDsForElement;
                    epsilon=2e-11,
                    nloadsteps=0,
                    max_poly_degree=maximum(p)+1,
                    runtimeoutput=false)
    #####
    Solve(self)
    return self
end


######################
######################
######################
######################
######################
######################
######################
######################
######################
######################



################################
### Optimization using NLopt ###
################################


# Compute the initial volume
s0 = Array{Float64}([7.0,7.0,7.0])

# ANSWER:
minx = Array{Float64}([10.0,9.500000000486505,1.5])

self = create_geometry_and_solve(s0)
vol_const = SSIGA2D.compute_volume(self)





feval_count = 0 # keep track of number of function evaluations
function myfunc(x::Vector, grad::Vector)
    global self
    self = create_geometry_and_solve(x)

    max_disp = -self.d[2,6,1]
    @show max_disp
    max_disp_ds = zeros(self.mesh.ndesignvars)
    for s_dir = 1:self.mesh.ndesignvars
        max_disp_ds[s_dir] = -self.d_ds[s_dir][2,6,1]
    end

    if length(grad) > 0
        grad[:] = max_disp_ds[:]
    end

    global feval_count
    feval_count::Int += 1
    println("f_$count($x)")

    return max_disp
end

function myconstraint(x::Vector, grad::Vector, vol_const)
    global self

    material_volume = SSIGA2D.compute_volume(self)
    @show material_volume
    material_volume_ds = SSIGA2D.compute_volume_ds(self)

    if length(grad) > 0
        grad[:] = material_volume_ds[:]
    end
    return material_volume - vol_const
end

n_vars = length(s0)
opt = Opt(:LD_MMA, n_vars)
lower_bounds!(opt, 1.5*ones(n_vars))
upper_bounds!(opt, 10.0*ones(n_vars))
xtol_rel!(opt,1e-4)
min_objective!(opt, myfunc)
inequality_constraint!(opt, (x,g) -> myconstraint(x,g,vol_const), 1e-8)

(minf,minx,ret) = optimize(opt, s0)
println("")
println("Optimization Complete")
println("Got $minf at\n$minx\nafter $feval_count iterations (returned $ret)")
println("Constraint: ")
material_volume = SSIGA2D.compute_volume(self)
@show material_volume - vol_const





################################
###  Plotting the Solution   ###
################################

println("Plotting...")
using PyPlot; plt = PyPlot;

view_xmin = -2.0
view_xmax = 32.0
view_height = 0.8*(view_xmax-view_xmin)
view_ymin = 5.0-(view_height/2)
view_ymax = 5.0+(view_height/2)

self = create_geometry_and_solve(s0)
closeplots = false
res = [40,40]
X,t,h = draw(self.mesh,res)
plt.figure()
plt.scatter(X[1,:],X[2,:],color="b")
plt.scatter(self.mesh.control_pts[1,:,:],self.mesh.control_pts[2,:,:],color="r")
plt.xlim(view_xmin,view_xmax)
plt.ylim(view_ymin,view_ymax)
plt.xlabel("x")
plt.ylabel("y")
plt.title("Beginning Geometry")
plt.savefig("plots_ignore/plot_2Dprob_geom02.pdf")

self = create_geometry_and_solve(minx)
using PyPlot; plt = PyPlot;
closeplots = false
res = [40,40]
X,t,h = draw(self.mesh,res)
plt.figure()
plt.scatter(X[1,:],X[2,:],color="b")
plt.scatter(self.mesh.control_pts[1,:,:],self.mesh.control_pts[2,:,:],color="r")
plt.xlim(view_xmin,view_xmax)
plt.ylim(view_ymin,view_ymax)
plt.xlabel("x")
plt.ylabel("y")
plt.title("Optimized Geometry")
plt.savefig("plots_ignore/plot_2Dprob_geom02b.pdf")


X,Y,F = solution(self,1,[40,40])
plt.figure()
plt.pcolor(X,Y,F)
plt.xlim(view_xmin,view_xmax)
plt.ylim(view_ymin*1.2,view_ymax*1.2)
plt.colorbar()
plt.title("disp_x")
plt.savefig("plots_ignore/plot_2Dprob_disp01.pdf")
if(closeplots)
    plt.close()
end

X,Y,F = solution(self,2,[40,40])
plt.figure()
plt.pcolor(X,Y,F)
plt.xlim(view_xmin,view_xmax)
plt.ylim(view_ymin*1.2,view_ymax*1.2)
plt.colorbar()
plt.title("disp_y")
plt.savefig("plots_ignore/plot_2Dprob_disp02.pdf")
if(closeplots)
    plt.close()
end




#=
X,Y,Fx = solution(self,1,[40,40])
X,Y,Fy = solution(self,2,[40,40])
normF = zeros(size(Fx))
for i = 1:length(Fx[:])
    normF[i] = sqrt(Fx[i]^2 + Fy[i]^2)
end
plt.figure()
plt.pcolor(X,Y,normF)
plt.xlim(view_xmin,view_xmax)
plt.ylim(view_ymin*1.2,view_ymax*1.2)
plt.colorbar()
plt.title("disp_norm")
plt.savefig("plots_ignore/plot_2Dprob_disp03.pdf")
if(closeplots)
    plt.close()
end
=#
