# Example - 1D IGA
# Basic 1D linear elastic problem in IGA

# The same problem as 'Coding 1' in Finite Elements 507 class.

include("../ssiga.jl")

using SSIGA
import SSIGA.IGAGrid1D
import SSIGA.nurbsBasis
import SSIGA.Solve
import SSIGA.solve_prep
import SSIGA.solution

using SSNurbsTools
import SSNurbsTools.Nurbs
import SSNurbsTools.domain_X
import SSNurbsTools.del_bernstein_basis
import SSNurbsTools.basis
import SSNurbsTools.delBasis
import SSNurbsTools.Jacobian
import SSNurbsTools.draw

using SSGaussQuad
import SSGaussQuad.GaussQuadIntegrate
import SSGaussQuad.NumGaussPts

function f_func(x::Float64)
    if(f_func_i == 1)
        return 1.0
    elseif(f_func_i == 2)
        return x
    elseif(f_func_i == 3)
        return x^2
    else
        return 0.0
    end
end

FextForElement =
    function(self::IGAGrid1D,e::Int64)
        Fext = zeros(self.nnodesperel)
        jacobianf = function(params)
                        ans = Jacobian(self.mesh,[e],params)[1,1]
                        return ans
                    end
        f = function(params)
                xi = params[1]
                return -f_func( domain_X(self.mesh,[e],reshape([xi],1,1))[1] ) * basis(self.mesh,[e],[xi])
            end
        result = GaussQuadIntegrate(f,self.GQT1D[NumGaussPts(self.p+2)],jacobianf)
        for node_a = 1:self.nnodesperel
            Fext[node_a] = result[node_a]
        end
        return reshape(Fext,self.ndofpernode,self.nnodesperel)
    end

NForElement =
    function(self::IGAGrid1D,e::Int64)
        Fint = zeros(self.nnodesperel)
        jacobianf = function(params)
                        ans = Jacobian(self.mesh,[e],params)[1,1]
                        return ans
                    end
        for node_a = 1:self.nnodesperel
            f = function(params)
                    xi = params[1]
                    N = basis(self.mesh,[e],[xi])
                    B = Array{Float64}([delBasis(self.mesh,[e],[nd_a],[xi])[1,1] for nd_a=1:self.nnodesperel])
                    sumBidi = 0.0
                    for node_i = 1:self.nnodesperel
                        sumBidi += B[node_i]*self.d[1,self.mesh.IEN[1][node_i,e]]
                    end
                    ans =  -B[node_a] * sumBidi
                    return ans
                end
            Fint[node_a] = GaussQuadIntegrate(f,self.GQT1D[NumGaussPts(self.p+2)],jacobianf)
        end
        retval = reshape(Fint,self.ndofpernode,self.nnodesperel)
        return retval
    end

function KpqForElement(self::IGAGrid1D,e::Int64,node_p::Int64,node_q::Int64)
    jacobianf = function(params)
                    ans = Jacobian(self.mesh,[e],params)[1,1]
                    return ans
                end
    f = function(params)
            xi = params[1]
            B = Array{Float64}([delBasis(self.mesh,[e],[nd_a],[xi])[1,1] for nd_a=1:self.nnodesperel])
            ans = -B[node_p]*B[node_q]
            return ans
        end
    Kpq = GaussQuadIntegrate(f,self.GQT1D[NumGaussPts(self.p+2)],jacobianf)
    return Kpq
end

dirichletConstraint =
    function(self::IGAGrid1D,e::Int64,a::Int64,dofi::Int64)
        #Setting bc that u(1) = 0
        if( e==self.nelements && a==self.p+1 )
            return true,0.0
        end
        return false,0.0
    end

neumannBCForElement =
    function(self::IGAGrid1D,e::Int64)
        T = zeros(self.p+1)
        return T
    end

consistentTangentForElement =
    function(self::IGAGrid1D,e::Int64)
        Kel = Array{Any}(self.nnodesperel,self.nnodesperel)
        for nd_p = 1:self.nnodesperel
            for nd_q = 1:self.nnodesperel
                entry = Array{Float64}([KpqForElement(self,e,nd_p,nd_q)])
                Kel[nd_p,nd_q] = entry
            end
        end
        return Kel
    end


#Creating the geometry
dim_p = 1
dim_s = 1

p = 1; n = 2; knot_v = [0., 0, 1, 1];
#p = 2; n = 3; knot_v = [0., 0, 0, 1, 1, 1];
#p = 3; n = 4; knot_v = [0., 0, 0, 0, 1, 1, 1, 1];

c_pts = reshape(collect(linspace(0.,1.,n)),1,n)
weights = Array{Array{Float64}}(undef,1)
weights[1] = ones(size(c_pts)[2])
geometry = Nurbs( dim_p, dim_s, [p], Array[knot_v], [n-1], c_pts, weights )
nel = 10 #100 #1000 #10000
ndofpernode = 1

# Creating the grid
println( "Creating the grid" )
self = IGAGrid1D(geometry,nel,ndofpernode,
                dirichletConstraint,
                neumannBCForElement,
                FextForElement,
                NForElement,
                consistentTangentForElement;
                nloadsteps=0,
                max_correction_loops=10,
                epsilon=1e-10)

# 1==linear, 2==x, 3==x^2
f_func_i = 3

println("Running Solve")
Solve(self)

#println("Running 'solve_prep'");
#solve_prep(self)


using PyPlot; plt = PyPlot;

if( self.nelements < 50 )
    ### Plotting the geometry
    res = [50]
    X,t,h = SSNurbsTools.draw(self.mesh,res)
    using PyPlot; plt = PyPlot;
    closeplots = false
    plt.figure()
    plt.scatter(X[1,:],zeros(length(X)),color="b")
    plt.scatter(self.mesh.control_pts[1,:],zeros(self.mesh.control_pts),color="r")
    plt.title("Geometry")

    # Plotting all basis functions as function of global param variable t
    tmin = self.mesh.knot_v[1][1]
    tmax = self.mesh.knot_v[1][end]
    t = collect(tmin:0.001:tmax)
    plt.figure()
    ymax = 1.1
    ymin = -0.1
    for i = 1:self.nnodes
        result = nurbsBasis(self,i,t)
        if (maximum(result) > ymax)
            ymax = maximum(result)
        end
        if (minimum(result) < ymin)
            ymin = minimum(result)
        end
        plt.plot(t,result)
    end
    plt.title("nurbsBasis")
    plt.ylim((ymin,ymax))

    # Plotting one element's basis functions as a function of element param variable xi.
    xi = collect(-1:0.001:1)
    plt.figure()
    ymax = 1.1
    ymin = -0.1
    e = 1
    for a = 1:self.p+1
        result = Array{Float64}([basis(self.mesh,[e],[xi[k]])[a] for k=1:length(xi)])
        if (maximum(result) > ymax)
            ymax = maximum(result)
        end
        if (minimum(result) < ymin)
            ymin = minimum(result)
        end
        plt.plot(xi,result)
    end
    plt.ylim((ymin,ymax))
end

# Plotting the solution
plt.figure()
which_dof = 1
samplesperel = 2*p
if(self.nelements >= 50 )
    samplesperel = 1
end
x,y = solution(self,which_dof,samplesperel)
plt.plot(x,y)
solution_span_y = (maximum(y) - minimum(y))
plt.ylim((minimum(y)-solution_span_y*0.2,maximum(y)+solution_span_y*0.2))
plt.title("Num elements="*string(self.nelements)*", p="*string(self.p))
plt.xlabel("x")
plt.ylabel("y")
