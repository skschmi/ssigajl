
include("../ssnurbstoolsjl/nurbstools.jl")

using .SSNurbsTools
import .SSNurbsTools.Nurbs
import .SSNurbsTools.draw
import .SSNurbsTools.basis
import .SSNurbsTools.Jacobian
import .SSNurbsTools.e_xi_for_t
import .SSNurbsTools.domain_X
import .SSNurbsTools.ConvertToBernsteinMesh
import .SSNurbsTools.e_a_xi_for_gb_t

dim_p = 1
dim_s = 3
p = [3]
n = [8]
spans = n .- 1

knot_v = Array[[0., 0, 0, 0, 1, 2, 3, 4, 5, 5, 5, 5]]

c_pts = zeros(dim_s,n[1])
c_pts[:,1] = [ 0., 10., 0]
c_pts[:,2] = [ 5., 15., 1]
c_pts[:,3] = [10., 15., 2]
c_pts[:,4] = [15.,  5., 3]
c_pts[:,5] = [20.,  0., 2]
c_pts[:,6] = [25.,  0., 1]
c_pts[:,7] = [30.,  0., 2]
c_pts[:,8] = [35.,  0., 3]
weights = Array{Array{Float64}}(undef,1)
weights[1] = Array{Float64}([1.,1.,1.,1.,1.,1.,1.,1.])
self = Nurbs(dim_p, dim_s, p, knot_v, spans, c_pts, weights)

using PyPlot; plt = PyPlot;

# Drawing the curve
res = [50]
X,t,h = draw(self,res)
plt.figure()
plt.scatter3D(X[1,:,:],X[2,:,:],X[3,:,:])
plt.scatter3D(self.control_pts[1,:,:],self.control_pts[2,:,:],self.control_pts[3,:,:],color="r")
plt.xlabel("x")
plt.ylabel("y")
plt.zlabel("z");
plt.title("3D Line")
