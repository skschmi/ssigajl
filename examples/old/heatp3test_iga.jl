
include("../ssiga.jl")

using SSIGA
import SSIGA.IGAGrid1D
import SSIGA.nurbsBasis
import SSIGA.basis
import SSIGA.delBasis
import SSIGA.t_for_xi
import SSIGA.Solve
import SSIGA.solve_prep
import SSIGA.element_dxidx

using SSBezierTools
import SSBezierTools.Nurbs
import SSBezierTools.domain_X
import SSBezierTools.DelBernsteinBasis

using SSGaussQuad
import SSGaussQuad.GaussQuadIntegrate

using GR


function body_heat_f(x::Float64)
    if(bodyheat_i == 1)
        return 1.0
    elseif(bodyheat_i == 2)
        return x
    elseif(bodyheat_i == 3)
        return x^2
    else
        return 0.0
    end
end

# Computing the folowing expression:
#  \int_{\Omega_{e}}N_{i}N_{a}f_{a}d\Omega_{e}
FextForElement =
    function(self::IGAGrid1D,e::Int64)
        Fext = zeros(self.nnodesperel)
        jacobianf = function(params)
                        ans = 1./element_dxidx(self,e,params[1])
                        return ans
                    end
        for node_a = 1:self.nnodesperel
            f = function(params)
                    xi = params[1]
                    N = basis(self,xi)
                    xval = domain_X(self.mesh,t_for_xi(self,e,xi))
                    fofx = body_heat_f(xval)
                    ans = N[node_a]*fofx
                    return ans
                end
            Fext[node_a] = GaussQuadIntegrate(f,self.GQT1Dp,jacobianf)
        end
        return reshape(Fext,self.ndofpernode,self.nnodesperel)
    end

# Computing the folowing expression:
#  \int_{\Omega_{e}}B_{a}B_{i}d_{i}\left(N_{j}d_{j}N_{k}d_{k}+1\right)d\Omega_{e}
NForElement =
    function(self::IGAGrid1D,e::Int64)
        Fint = zeros(self.nnodesperel)
        jacobianf = function(params)
                        ans = 1./element_dxidx(self,e,params[1])
                        return ans
                    end
        for node_a = 1:self.nnodesperel
            f = function(params)
                    xi = params[1]
                    N = basis(self,xi)
                    B = delBasis(self,e,xi)
                    sumNjdj = 0.0
                    for node_j = 1:self.nnodesperel
                        sumNjdj += N[node_j]*self.d[1,self.IEN[node_j,e]]
                    end
                    sumNkdk = sumNjdj
                    sumBidi = 0.0
                    for node_i = 1:self.nnodesperel
                        sumBidi += B[node_i]*self.d[1,self.IEN[node_i,e]]
                    end

                    ans = B[node_a] * sumBidi * (sumNjdj * sumNkdk + 1)

                    #***********************
                    # If we set Kappa=1:
                    #ans = B[node_a] * sumBidi * (1) # (There are two of these!)
                    #***********************

                    return ans
                end
            Fint[node_a] = GaussQuadIntegrate(f,self.GQT1Dp,jacobianf)
        end
        retval = reshape(Fint,self.ndofpernode,self.nnodesperel)
        return retval
    end

dirichletConstraint =
            function(self::IGAGrid1D,e::Int64,a::Int64,dofi::Int64)
                #Setting bc that u(1)=0
                if(e==self.nelements && a==self.p+1)
                    return true,0.0
                end
                return false,0.0
            end

neumannBCForElement =
            function(self::IGAGrid1D,e::Int64)
                T = zeros(self.p+1)
                return T
            end

function KpqForElement(self::IGAGrid1D,e::Int64,node_p::Int64,node_q::Int64)
    jacobianf = function(params)
                    ans = 1./element_dxidx(self,e,params[1])
                    return ans
                end
    f = function(params)
            xi = params[1]
            B = delBasis(self,e,xi)
            N = basis(self,xi)
            sumNjdj = 0.0
            for node_j = 1:self.nnodesperel
                sumNjdj += N[node_j]*self.d[1,self.IEN[node_j,e]]
            end
            sumNkdk = sumNjdj
            sumBidi = 0.0
            for node_i = 1:self.nnodesperel
                sumBidi += B[node_i]*self.d[1,self.IEN[node_i,e]]
            end
            ans = B[node_p]*N[node_q]*sumBidi*(sumNjdj+sumNkdk) +
                                B[node_p]*B[node_q]*(sumNjdj*sumNkdk+1)

            #***********************
            # If we set Kappa=1:
            #ans = B[node_p]*B[node_q]  # (There are two of these!)
            #***********************

            return ans
        end
    Kpq = GaussQuadIntegrate(f,self.GQT1Dp,jacobianf)
    return Kpq
end

consistentTangentForElement =
            function(self::IGAGrid1D,e::Int64)
                Kel = Array(Array{Float64},self.nnodesperel,self.nnodesperel)
                for nd_p = 1:self.nnodesperel
                    for nd_q = 1:self.nnodesperel
                        entry = Array{Float64}([KpqForElement(self,e,nd_p,nd_q)])
                        Kel[nd_p,nd_q] = entry
                    end
                end
                return Kel
            end

function main()
    # Problem parameter (global)
    global bodyheat_i = 1
    bodyheat_titles = ["f=1.0","f=x","f=x^2"]

    #Creating the geometry
    dim_p = 1
    dim_s = 1

    #p = 1; n = 2; knot_v = [0., 0, 1, 1]
    #p = 2; n = 3; knot_v = [0., 0, 0, 1, 1, 1]
    p = 3; n = 4; knot_v = [0., 0, 0, 0, 1, 1, 1, 1];

    c_pts = reshape(collect(linspace(0.,1.,n)),1,n)
    weights = ones(size(c_pts)[2])
    geometry = Nurbs( dim_p, dim_s, [p], Array[knot_v], [n-1], c_pts, weights )
    nel = 10
    ndofpernode = 1

    # Creating the grid
    self = IGAGrid1D(geometry,nel,ndofpernode,
                    dirichletConstraint,
                    neumannBCForElement,
                    FextForElement,
                    NForElement,
                    consistentTangentForElement;
                    nloadsteps=5,
                    max_correction_loops=10)

    #solve_prep(self)
    println("Running Solve")
    Solve(self)

    tmin = self.mesh.knot_v[1][1]
    tmax = self.mesh.knot_v[1][end]
    t = collect(tmin:0.001:tmax)

    GR.figure()
    for i = 1:self.nnodes
        GR.plot(t,nurbsBasis(self,i,t))
        GR.title("nurbsBasis")
        GR.hold(true)
    end
    GR.hold(false)
    GR.savefig("./plots_ignore/fig_basis_look1.pdf")


    xi = collect(-1:0.001:1)
    GR.figure()
    for a = 1:self.p+1
        GR.plot(xi,basis(self,a,xi))
        GR.title("basis")
        GR.hold(true)
    end
    GR.hold(false)
    GR.savefig("./plots_ignore/fig_basisGQ_look1.pdf")


    xi = collect(-1:0.1:1)
    GR.figure()
    e = 1
    for a = 1:self.p+1
        result = delBasis(self,e,a,xi)
        GR.plot(xi,result)
        GR.title("delBasis")
        GR.hold(true)
    end
    GR.hold(false)
    GR.savefig("./plots_ignore/fig_delBasisGQ_look1.pdf")

    # Plotting the solution
    h = (1/100)
    if(nel>=10)
        h = (1/(nel*10))
    end
    t = minimum(self.uniqueknots):h:(maximum(self.uniqueknots)-h)
    x = domain_X(self.mesh,t)
    y = zeros(size(t))
    for i = 1:length(self.d)
        y += nurbsBasis(self,i,t) .* self.d[i]
    end
    GR.plot(x,y)
    GR.ylim((minimum(y)-0.1,maximum(y)+0.1))
    GR.title("Num elements="*string(self.nelements)*", p="*string(self.p))
    GR.xlabel("x")
    GR.ylabel("Temperature")
    GR.savefig("./plots_ignore/fig_sol_look1.pdf")


    return self, xi
end

self, xi = main()
