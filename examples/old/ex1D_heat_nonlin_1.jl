
include("../ssiga.jl")

using SSIGA
import SSIGA.IGAGrid1D
import SSIGA.nurbsBasis
import SSIGA.delBasis
import SSIGA.Solve
import SSIGA.solve_prep
import SSIGA.solution

using SSNurbsTools
import SSNurbsTools.Nurbs
import SSNurbsTools.domain_X
import SSNurbsTools.basis
import SSNurbsTools.t_for_xi
import SSNurbsTools.Jacobian

using SSGaussQuad
import SSGaussQuad.GaussQuadIntegrate
import SSGaussQuad.NumGaussPts

using GR


function body_heat_f(x::Float64)
    if(bodyheat_i == 1)
        return 1.0
    elseif(bodyheat_i == 2)
        return x
    elseif(bodyheat_i == 3)
        return x^2
    else
        return 0.0
    end
end

# Computing the folowing expression:
#  \int_{\Omega_{e}}N_{i}N_{a}f_{a}d\Omega_{e}
FextForElement =
    function(self::IGAGrid1D,e::Int64)
        Fext = zeros(self.nnodesperel)
        jacobianf = function(params)
                        ans = Jacobian(self.mesh,[e],params)[1,1]
                        return ans
                    end
        for node_a = 1:self.nnodesperel
            f = function(params)
                    xi = params[1]
                    N = basis(self.mesh,[e],[xi])
                    sup_cpmin = self.mesh.IEN[1][1,e]
                    sup_cpmax = self.mesh.IEN[1][self.p+1,e]
                    xval = domain_X(self.mesh,
                                    reshape(t_for_xi(self.mesh,[e],[xi]),1,1),
                                    Array[collect(sup_cpmin:sup_cpmax)])
                    fofx = body_heat_f(xval[1])
                    ans = N[node_a]*fofx
                    return ans
                end
            gqtable = self.GQT1D[NumGaussPts(self.p+bodyheat_i-1)]
            Fext[node_a] = GaussQuadIntegrate(f,gqtable,jacobianf)
        end
        return reshape(Fext,self.ndofpernode,self.nnodesperel)
    end

# Computing the folowing expression:
#  \int_{\Omega_{e}}B_{a}B_{i}d_{i}\left(N_{j}d_{j}N_{k}d_{k}+1\right)d\Omega_{e}
NForElement =
    function(self::IGAGrid1D,e::Int64)
        Fint = zeros(self.nnodesperel)
        jacobianf = function(params)
                        ans = Jacobian(self.mesh,[e],params)[1,1]
                        return ans
                    end
        for node_a = 1:self.nnodesperel
            f = function(params)
                    xi = params[1]
                    N = basis(self.mesh,[e],[xi])
                    B = Array{Float64}([delBasis(self.mesh,[e],[nd_a],[xi])[1,1] for nd_a=1:self.nnodesperel])
                    sumNjdj = 0.0
                    for node_j = 1:self.nnodesperel
                        sumNjdj += N[node_j]*self.d[1,self.mesh.IEN[1][node_j,e]]
                    end
                    sumNkdk = sumNjdj
                    sumBidi = 0.0
                    for node_i = 1:self.nnodesperel
                        sumBidi += B[node_i]*self.d[1,self.mesh.IEN[1][node_i,e]]
                    end

                    ans = B[node_a] * sumBidi * (sumNjdj * sumNkdk + 1)

                    #***********************
                    # If we set Kappa=1:
                    #ans = B[node_a] * sumBidi * (1) # (There are two of these!)
                    #***********************

                    return ans
                end
            gqtable = self.GQT1D[NumGaussPts(2*(self.p-1)+(2*self.p))]
            Fint[node_a] = GaussQuadIntegrate(f,gqtable,jacobianf)
        end
        retval = reshape(Fint,self.ndofpernode,self.nnodesperel)
        return retval
    end

dirichletConstraint =
            function(self::IGAGrid1D,e::Int64,a::Int64,dofi::Int64)
                #Setting bc that u(1)=0
                if(e==self.nelements && a==self.p+1)
                    return true,0.0
                end
                return false,0.0
            end

neumannBCForElement =
            function(self::IGAGrid1D,e::Int64)
                T = zeros(self.p+1)
                return T
            end

function KpqForElement(self::IGAGrid1D,e::Int64,node_p::Int64,node_q::Int64)
    jacobianf = function(params)
                    ans = Jacobian(self.mesh,[e],params)[1,1]
                    return ans
                end
    f = function(params)
            xi = params[1]
            B = Array{Float64}([delBasis(self.mesh,[e],[nd_a],[xi])[1,1] for nd_a=1:self.nnodesperel])
            N = basis(self.mesh,[e],[xi])
            sumNjdj = 0.0
            for node_j = 1:self.nnodesperel
                sumNjdj += N[node_j]*self.d[1,self.mesh.IEN[1][node_j,e]]
            end
            sumNkdk = sumNjdj
            sumBidi = 0.0
            for node_i = 1:self.nnodesperel
                sumBidi += B[node_i]*self.d[1,self.mesh.IEN[1][node_i,e]]
            end
            ans = B[node_p]*N[node_q]*sumBidi*(sumNjdj+sumNkdk) +
                                B[node_p]*B[node_q]*(sumNjdj*sumNkdk+1)

            #***********************
            # If we set Kappa=1:
            #ans = B[node_p]*B[node_q]  # (There are two of these!)
            #***********************

            return ans
        end
    gqtable = self.GQT1D[NumGaussPts(2*(self.p-1)+(2*self.p))]
    Kpq = GaussQuadIntegrate(f,gqtable,jacobianf)
    return Kpq
end

consistentTangentForElement =
            function(self::IGAGrid1D,e::Int64)
                Kel = Array(Array{Float64},self.nnodesperel,self.nnodesperel)
                for nd_p = 1:self.nnodesperel
                    for nd_q = 1:self.nnodesperel
                        entry = Array{Float64}([KpqForElement(self,e,nd_p,nd_q)])
                        Kel[nd_p,nd_q] = entry
                    end
                end
                return Kel
            end

function main()
    # Problem parameter (global)
    global bodyheat_i = 1
    bodyheat_titles = ["f=1.0","f=x","f=x^2"]

    #Creating the geometry
    dim_p = 1
    dim_s = 1

    #p = 1
    #n = 2
    #knot_v = [0., 0, 1, 1]
    p = 2
    n = 3
    knot_v = [0., 0, 0, 1, 1, 1]
    #p = 3
    #n = 4
    #knot_v = [0., 0, 0, 0, 1, 1, 1, 1]

    c_pts = reshape(collect(linspace(0.,1.,n)),1,n)
    weights = Array{Array{Float64}}(undef,1)
    weights[1] = ones(size(c_pts)[2])
    geometry = Nurbs( dim_p, dim_s, [p], Array[knot_v], [n-1], c_pts, weights )
    nel = 10
    ndofpernode = 1
    max_poly_degree = maximum((2*(p-1)+(2*p),p+bodyheat_i-1))

    # Creating the grid
    self = IGAGrid1D(geometry,nel,ndofpernode,
                    dirichletConstraint,
                    neumannBCForElement,
                    FextForElement,
                    NForElement,
                    consistentTangentForElement;
                    nloadsteps=5,
                    max_correction_loops=10,
                    max_poly_degree=max_poly_degree)

    xi = nothing

    #solve_prep(self)
    println("Running Solve")
    Solve(self)

    tmin = self.mesh.knot_v[1][1]
    tmax = self.mesh.knot_v[1][end]
    t = collect(tmin:0.001:tmax)


    GR.figure()
    for i = 1:self.nnodes
        GR.plot(t,nurbsBasis(self,i,t))
        GR.title("nurbsBasis")
        GR.hold(true)
    end
    GR.hold(false)
    GR.savefig("./plots_ignore/fig_basis_look1.pdf")


    xi = collect(-1:0.001:1)
    GR.figure()
    e = 1
    for a = 1:self.p+1
        basis_arr = Array{Float64}([basis(self.mesh,[e],[xi[k]])[a] for k=1:length(xi)])
        GR.plot(xi,basis_arr)
        GR.title("basis")
        GR.hold(true)
    end
    GR.hold(false)
    GR.savefig("./plots_ignore/fig_basisGQ_look1.pdf")


    h = 0.005
    xi = collect(-1:h:1-h)
    GR.figure()
    e = 1
    for a = 1:self.p+1
        result = Array{Float64}([delBasis(self.mesh,[e],[a],[xi[k]])[1,1] for k=1:length(xi)])
        GR.plot(xi,result)
        GR.title("delBasis")
        GR.hold(true)
    end
    GR.hold(false)
    GR.savefig("./plots_ignore/fig_delBasisGQ_look1.pdf")


    # Plotting the solution
    samplesperel = div(200,nel)+1
    x,y = solution(self,1,samplesperel)
    GR.plot(x,y)
    GR.ylim((minimum(y)-0.1,maximum(y)+0.1))
    GR.title("Num elements="*string(self.nelements)*", p="*string(self.p))
    GR.xlabel("x")
    GR.ylabel("Temperature")
    GR.savefig("./plots_ignore/fig_sol_look1.pdf")


    if(true)

        ndofpernode = 1

        #Iterate for various runs.
        for bodyheat_i = 1:3
            for p in [1,2]

                n = p+1
                knot_v = [zeros(n);ones(n)]
                min_y = 0.0
                max_y = 0.1
                max_poly_degree = maximum((2*(p-1)+(2*p),p+bodyheat_i-1))

                GR.figure()
                for nel in [1,10,100,1000] #,100,1000]

                    println("Running Problem: bodyheat_i=",bodyheat_i," p=",p," nel=",nel)
                    print("Deallocating old memory...")
                    # Clear out the memory from before.
                    self = nothing
                    gc()
                    print("done.\n")

                    print("Generating nurbs...")
                    c_pts = reshape(collect(linspace(0.,1.,n)),1,n)
                    weights = Array{Array{Float64}}(undef,1)
                    weights[1] = ones(size(c_pts)[2])
                    geometry = Nurbs( dim_p, dim_s, [p], Array[knot_v], [n-1], c_pts, weights )
                    print("done.\n")

                    print("Creating grid...")
                    # Creating the grid
                    self = IGAGrid1D(geometry,nel,ndofpernode,
                                    dirichletConstraint,
                                    neumannBCForElement,
                                    FextForElement,
                                    NForElement,
                                    consistentTangentForElement;
                                    nloadsteps=5,
                                    max_correction_loops=10,
                                    max_poly_degree=max_poly_degree)
                    print("done.\n")

                    #solve_prep(self)
                    println("Running Solve...")
                    Solve(self)
                    print("done.\n")

                    # plot solution
                    samplesperel = div(200,nel)+1
                    print("Computing solution...")
                    x,y = solution(self,1,samplesperel)
                    print("done.\n")
                    print("Computing plot...")
                    GR.plot(x,y)
                    if(minimum(y) < min_y)
                        min_y = minimum(y)
                    end
                    if(maximum(y) > max_y)
                        max_y = maximum(y)
                    end
                    GR.hold(true)
                    print("done.\n")
                    gc()
                end
                GR.hold(false)
                GR.ylim((min_y-0.02,max_y+0.02))
                GR.title("[IGA] p="*string(self.p)*", body heat "*bodyheat_titles[bodyheat_i])
                GR.legend("1 elem","10 elem","100 elem","1000 elem") #,"100 elem","1000 elem")
                GR.xlabel("x")
                GR.ylabel("Temperature")
                GR.savefig("./plots_ignore/plot_p"*string(p)*"_bh"*string(bodyheat_i)*".pdf")
            end
        end

    end




    return self, xi
end

self, xi = main()








#=
dirichletConstraint =
    function()
        return nothing
    end

neumannBCForElement =
    function()
        return nothing
    end

FextForElement =
    function()
        return nothing
    end

NForElement =
    function()
        return nothing
    end

consistentTangentForElement =
    function()
        return nothing
    end
=#
