

include("../ssiga2d.jl")
using SSIGA2D
import SSIGA2D.IGAGrid2D
import SSIGA2D.basis
import SSIGA2D.delBasis
import SSIGA2D.Solve
import SSIGA2D.GetStrainDispMatB
import SSIGA2D.Jacobian
import SSIGA2D.solution

import SSBezierTools.Nurbs
import SSBezierTools.draw
import SSBezierTools.bezier_basis_helper
import SSBezierTools.BernsteinBasis

import SSGaussQuad.GaussQuadIntegrate
import SSGaussQuad.NumGaussPts

a = convert(Int64,1)
p = convert(Int64,3)
h = convert(Float64,1e-40)
t = convert(ComplexF64,-0.45+h*im)
dmin = convert(Float64,-1.0)
dmax = convert(Float64,1.0)
N = 1000000

println("BernsteinBasis, ",N," loops")
@time for loop=1:N
    ans = BernsteinBasis(a,p,t,dmin=dmin,dmax=dmax)
end



i = convert(Int64,1)
degree = convert(Int64,3)
knot_v = Array{Float64,1}([0.,0.,0.,0.,1.,1.,1.,1.])
h = convert(Float64,1e-40)
t = convert(ComplexF64,0.34+h*im)

println("bezier_basis_helper, ",N," loops")
@time for loop=1:N
    ans = bezier_basis_helper(i,degree,knot_v,t)
end
