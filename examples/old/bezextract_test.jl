
include("../ssiga.jl")

using SSIGA
importall SSIGA
using SSBezierTools
importall SSBezierTools
using SSGaussQuad
importall SSGaussQuad
using GR

# Goal: Implement the Bezier Extraction algorithm and do tests on it.
#   In other words: Create a bezier extraction operator for each element

#=
"""
function bezExtOperators(knot_v, p)
=====
From page 26-27 in Bezier extraction of NURBS paper
 (Borden, Scott, Evans, Hughes; 2010)
"""
function bezExtOperators(knot_v::Array{Float64}, p::Int64)

    a = p+1
    b = a+1
    nel = 1
    C = Array{Array{Float64,2},1}(1)


    return nel,C
end
=#


# Init
#=
p = 3
knot_v = [0., 0, 0, 0, 1, 1, 1, 1]
n = length(knot_v)-p-1
control_pts = reshape(collect(linspace(0.,1.,n)),1,n)
=#

p = 3
knot_v = [0.,0,0,0,1,2,3,4,4,4,4]
n = length(knot_v)-p-1
control_pts = reshape(collect(linspace(0.,1.,n)),1,n)

println("")
println("")
println("START")
println("p: ",p)
println("knot_v: ",knot_v)
println("control_pts",control_pts)


# simple test
println("")
println("")
println("Simple test: Inserting one knot at 0.1")
knot_v_ik1 = copy(knot_v)
control_pts_ik1 = copy(control_pts)
sol = SSBezierTools.InsertKnot(0.1,p,knot_v_ik1,control_pts_ik1)
println("sol: ",sol)
Ctest,knot_v_test = SSBezierTools.BezExtOpMatrix(0.1,p,knot_v_ik1)
println("bezExtOp C:\n",Ctest)
test_new_contpts = Ctest'*control_pts_ik1'
println("test_new_contpts: ",test_new_contpts')

# Looking at regular Knot Insertion, seeing what control points we get.
println("")
println("")
println("Looking at regular Knot Insertion, seeing what control points we get.")
knot_v_ik = copy(knot_v)
control_pts_ik = copy(control_pts)

#=
knot_v_ik,control_pts_ik = SSBezierTools.InsertKnot(0.1,p,knot_v_ik,control_pts_ik)
knot_v_ik,control_pts_ik = SSBezierTools.InsertKnot(0.2,p,knot_v_ik,control_pts_ik)
knot_v_ik,control_pts_ik = SSBezierTools.InsertKnot(0.3,p,knot_v_ik,control_pts_ik)
knot_v_ik,control_pts_ik = SSBezierTools.InsertKnot(0.4,p,knot_v_ik,control_pts_ik)
=#

knot_v_ik,control_pts_ik = SSBezierTools.InsertKnot(1.,p,knot_v_ik,control_pts_ik)
knot_v_ik,control_pts_ik = SSBezierTools.InsertKnot(1.,p,knot_v_ik,control_pts_ik)
knot_v_ik,control_pts_ik = SSBezierTools.InsertKnot(2.,p,knot_v_ik,control_pts_ik)
knot_v_ik,control_pts_ik = SSBezierTools.InsertKnot(2.,p,knot_v_ik,control_pts_ik)
knot_v_ik,control_pts_ik = SSBezierTools.InsertKnot(3.,p,knot_v_ik,control_pts_ik)
knot_v_ik,control_pts_ik = SSBezierTools.InsertKnot(3.,p,knot_v_ik,control_pts_ik)


println("control_pts_ik: ",control_pts_ik)


# Computing the bezier extraction operator, and then seeing what control points we get.
println("")
println("")
println("Computing the bezier extraction operator, and then seeing what control points we get.")
knot_v_be = copy(knot_v)

#=
C1,knot_v_be = SSBezierTools.BezExtOpMatrix(0.1,p,knot_v_be)
C2,knot_v_be = SSBezierTools.BezExtOpMatrix(0.2,p,knot_v_be)
C3,knot_v_be = SSBezierTools.BezExtOpMatrix(0.3,p,knot_v_be)
C4,knot_v_be = SSBezierTools.BezExtOpMatrix(0.4,p,knot_v_be)
=#

C1,knot_v_be = SSBezierTools.BezExtOpMatrix(1.,p,knot_v_be)
C2,knot_v_be = SSBezierTools.BezExtOpMatrix(1.,p,knot_v_be)
C3,knot_v_be = SSBezierTools.BezExtOpMatrix(2.,p,knot_v_be)
C4,knot_v_be = SSBezierTools.BezExtOpMatrix(2.,p,knot_v_be)
C5,knot_v_be = SSBezierTools.BezExtOpMatrix(3.,p,knot_v_be)
C6,knot_v_be = SSBezierTools.BezExtOpMatrix(3.,p,knot_v_be)


#C = (C4'*C3'*C2'*C1')'
C = (C6'*C5'*C4'*C3'*C2'*C1')'
control_pts_be = C'*control_pts'
println("control_pts_be: ",control_pts_be')
println("C:\n",C)

println("")
println("")
#println("C4:\n",C4)
