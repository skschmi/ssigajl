include("../ssiga2d.jl")
using SSIGA2D
import SSIGA2D.IGAGrid2D
import SSIGA2D.basis
import SSIGA2D.delBasis
import SSIGA2D.Solve
import SSIGA2D.GetStrainDispMatB
import SSIGA2D.Jacobian
import SSIGA2D.solution

import SSBezierTools.Nurbs
import SSBezierTools.draw
import SSBezierTools.BernsteinBasis
import SSBezierTools.DelBernsteinBasis
import SSBezierTools.bezier_basis_helper
import SSBezierTools.del_bezier_basis_helper

import SSGaussQuad.GaussQuadIntegrate
import SSGaussQuad.NumGaussPts

h = 1e-40
N = 2e6
@show h
@show N


xi = -0.45


println("")
println("")
println("")
println("BernsteinBasis")
println("")
println("")
println("")
println("Init run...")
for i = 1:N
    DelBernsteinBasis(1,3,xi,dmin=-1.,dmax=1.)
    imag(BernsteinBasis(1,3,xi+h*im,dmin=-1.,dmax=1.))/h
end




println("Del run... (only del)")
orig_del_ans = 0.0
@time for i = 1:N
    orig_del_ans = DelBernsteinBasis(1,3,xi,dmin=-1.,dmax=1.)
end
println("Imag Del run... (only del)")
del_ans = 0.0
@time for i = 1:N
    del_ans = imag(BernsteinBasis(1,3,xi+h*im,dmin=-1.,dmax=1.))/h
end
@show orig_del_ans
@show del_ans



println("Del run... (both)")
orig_ans = 0.0
orig_del_ans = 0.0
@time for i = 1:N
    orig_ans = BernsteinBasis(1,3,xi,dmin=-1.,dmax=1.)
    orig_del_ans = DelBernsteinBasis(1,3,xi,dmin=-1.,dmax=1.)
end
println("Imag Del run... (both)")
ans = 0.0
del_ans = 0.0
@time for i = 1:N
    ans = BernsteinBasis(1,3,xi+h*im,dmin=-1.,dmax=1.)
    del_ans = imag(ans)/h
end
@show orig_ans
@show orig_del_ans
@show ans
@show del_ans












println("")
println("")
println("")
println("bezier_basis_helper")
println("")
println("")
println("")
i = 1
knot_v = [0., 0, 0, 0, 1, 1, 1, 1]
degree = 3
t = 0.34
println("Init run...")
for j = 1:N
    bezier_basis_helper(i,degree,knot_v,t)
    del_bezier_basis_helper(i,degree,knot_v,t)
end


println("Del run... (only del)")
orig_del_ans = 0.0
@time for j = 1:N
    orig_del_ans = del_bezier_basis_helper(i,degree,knot_v,t)
end
println("Imag Del run... (only del)")
del_ans = 0.0
@time for j = 1:N
    del_ans = imag(bezier_basis_helper(i,degree,knot_v,t+h*im))/h
end
@show orig_del_ans
@show del_ans



println("Del run... (both)")
orig_ans = 0.0
orig_del_ans = 0.0
@time for j = 1:N
    orig_ans = bezier_basis_helper(i,degree,knot_v,t)
    orig_del_ans = del_bezier_basis_helper(i,degree,knot_v,t)
end
println("Imag Del run... (both)")
ans = 0.0
del_ans = 0.0
@time for j = 1:N
    ans = bezier_basis_helper(i,degree,knot_v,t+h*im)
    del_ans = imag(ans)/h
end
@show orig_ans
@show orig_del_ans
@show ans
@show del_ans
