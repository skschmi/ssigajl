
include("../ssiga.jl")

using SSIGA
import SSIGA.IGAGrid1D
import SSIGA.nurbsBasis
import SSIGA.basis
import SSIGA.delBasis
import SSIGA.t_for_xi
import SSIGA.Solve
import SSIGA.solve_prep
import SSIGA.element_dxidx
import SSIGA.solution

using SSBezierTools
import SSBezierTools.Nurbs
import SSBezierTools.domain_X
import SSBezierTools.DelBernsteinBasis

using SSGaussQuad
import SSGaussQuad.GaussQuadIntegrate
import SSGaussQuad.NumGaussPts

using PyPlot; plt = PyPlot;


# Computing the folowing expression:
#  \int_{\Omega_{e}}N_{i}N_{a}f_{a}d\Omega_{e}
FextForElement =
    function(self::IGAGrid1D,e::Int64)
        return nothing
    end

# Computing the folowing expression:
#  \int_{\Omega_{e}}B_{a}B_{i}d_{i}\left(N_{j}d_{j}N_{k}d_{k}+1\right)d\Omega_{e}
NForElement =
    function(self::IGAGrid1D,e::Int64)
        return nothing
    end

dirichletConstraint =
            function(self::IGAGrid1D,e::Int64,a::Int64,dofi::Int64)
                return nothing
            end

neumannBCForElement =
            function(self::IGAGrid1D,e::Int64)
                return nothing
            end

consistentTangentForElement =
            function(self::IGAGrid1D,e::Int64)
                return nothing
            end


# Problem parameter (global)
global bodyheat_i = 1
bodyheat_titles = ["f=1.0","f=x","f=x^2"]

#Creating the geometry
dim_p = 1
dim_s = 1

#p = 1
#n = 2
#knot_v = [0., 0, 1, 1]
#p = 2
#n = 3
#knot_v = [0., 0, 0, 1, 1, 1]
p = 3
n = 4
knot_v = [0., 0, 0, 0, 1, 1, 1, 1]

c_pts = reshape(collect(linspace(0.,1.,n)),1,n)
#weights = ones(size(c_pts)[2])
weights = [1.,2.,5.3,1.]
geometry = Nurbs( dim_p, dim_s, [p], Array[knot_v], [n-1], c_pts, weights )
nel = 10
ndofpernode = 1
max_poly_degree = maximum((2*(p-1)+(2*p),p+bodyheat_i-1))

# Creating the grid
self = IGAGrid1D(geometry,nel,ndofpernode,
                dirichletConstraint,
                neumannBCForElement,
                FextForElement,
                NForElement,
                consistentTangentForElement;
                nloadsteps=5,
                max_correction_loops=10,
                max_poly_degree=max_poly_degree)

#Plot the basis functions for geom, mesh, and bmesh
titles = ["geom","mesh","bmesh"]
titles_i = 0
for nurbsobj in [self.geom,self.mesh,self.bmesh]
    titles_i += 1
    plt.figure()
    t = collect(nurbsobj.knot_v[1][1]:0.001:nurbsobj.knot_v[1][end])
    for i = 1:size(nurbsobj.control_pts)[2]
        plt.plot(t,SSBezierTools.basis(nurbsobj,i,t))
        plt.hold(true)
    end
    plt.hold(false)
    plt.title(titles[titles_i])
end


plt.figure()
xi = collect(-1:0.01:1)
for a = 1:self.p+1
    Nf = [SSIGA.basis(self,1,a,xi[i]) for i=1:length(xi)]
    plt.plot(xi,Nf)
    plt.hold(true)
end
plt.hold(false)
plt.title("basis - IGA")

plt.figure()
xi = collect(-1:0.01:1)
for a = 1:self.p+1
    delNf = [SSIGA.delBasis(self,1,a,xi[i]) for i=1:length(xi)]
    plt.plot(xi,delNf)
    plt.hold(true)
end
plt.hold(false)
#plt.ylim([-2,2])
plt.xlim([-1,1])
plt.title("delBasis - IGA")
plt.savefig("plots_ignore/deltest1.pdf")

plt.figure()
hh = 0.0001
xi1 = collect(-1:hh:1-hh)
xi2 = collect(-1+hh:hh:1)
xi3 = collect(-1+hh/2:hh:1-hh/2)
for a = 1:self.p+1
    Nf1 = [SSIGA.basis(self,1,a,xi1[i]) for i=1:length(xi1)]
    Nf2 = [SSIGA.basis(self,1,a,xi2[i]) for i=1:length(xi2)]
    delNftest = (Nf2-Nf1)./hh
    plt.plot(xi3,delNftest)
    plt.hold(true)
end
plt.hold(false)
#plt.ylim([-2,2])
plt.xlim([-1,1])
plt.title("delBasis discrete test")
plt.savefig("plots_ignore/deltest2.pdf")
