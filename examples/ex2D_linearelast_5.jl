# Example - 2D IGA
# Basic 2D linear elastic problem in IGA

# Plate with a hole in the center (pressure on the inside surface).

include("../sslinearelastic.jl")

using .SSLinearElastic

using .SSLinearElastic.SSIGA2D
import .SSLinearElastic.SSIGA2D.IGAGrid2D
import .SSLinearElastic.SSIGA2D.delBasis
import .SSLinearElastic.SSIGA2D.Solve
import .SSLinearElastic.SSIGA2D.GetStrainDispMatB
import .SSLinearElastic.SSIGA2D.solution

import .SSLinearElastic.SSIGA2D.SSNurbsTools.Nurbs
import .SSLinearElastic.SSIGA2D.SSNurbsTools.draw
import .SSLinearElastic.SSIGA2D.SSNurbsTools.basis
import .SSLinearElastic.SSIGA2D.SSNurbsTools.Jacobian
import .SSLinearElastic.SSIGA2D.SSNurbsTools.domain_X
import .SSLinearElastic.SSIGA2D.SSNurbsTools.domain_dXdxi
import .SSLinearElastic.SSIGA2D.SSNurbsTools.domain_dXdsdxi

import .SSLinearElastic.SSIGA2D.SSGaussQuad.GaussQuadIntegrate
import .SSLinearElastic.SSIGA2D.SSGaussQuad.NumGaussPts

using LinearAlgebra
using Printf

dirichletConstraint =
    function(self::IGAGrid2D,e::Array{Int64},a::Array{Int64},dofi::Int64)
        # bottom side (left side of xi) restricted in the y-dir
        if(e[1] == 1 && a[1] == 1 && dofi == 2)
            return true,0.0
        end
        # right side (right side of xi) restricted in the x-dir
        if(e[1] == self.nelements[1] && a[1] == self.nnodesperel[1] && dofi == 1)
            return true,0.0
        end
        return false,0.0
    end


neumannBCForElementFace =
    function(self::IGAGrid2D,e::Array{Int64},face::Tuple{Int64,Int64},xi::Array{Float64})
        # This returns the tractions for the specified face of the element, at position xi
        # Paramters:
        # e: Element index
        # face[1]: Parameter direction: 1={xi=-1,xi=1} faces.  2={eta=-1,eta=1} faces
        # face[2]: bottom or top face:  1={xi=-1 face} 2={xi=1 face}
        # xi: The xi,eta coordinates along the face we are looking at
        # Returns:
        # x_dir_traction, y_dir_traction
        if(e[2] == 1 && face[1] == 2 && face[2] == 1)

            # The traction goes against the surface, normal to the surface
            # "try03"
            dXdxi_val = domain_dXdxi(self.mesh,e,reshape(xi,2,1))
            xvec = dXdxi_val[1,2]
            yvec = dXdxi_val[2,2]
            norm_vec = norm([xvec,yvec])
            xvec_unit = xvec/norm_vec
            yvec_unit = yvec/norm_vec
            return xvec_unit,yvec_unit

        end
        return 0.0,0.0
    end


neumannBCDsForElementFace =
    function(self::IGAGrid2D,e::Array{Int64},face::Tuple{Int64,Int64},xi::Array{Float64},s_dir::Int64)
        # This returns the tractions for the specified face of the element, at position xi
        # Paramters:
        # e: Element index
        # face[1]: Parameter direction: 1={xi=-1,xi=1} faces.  2={eta=-1,eta=1} faces
        # face[2]: bottom or top face:  1={xi=-1 face} 2={xi=1 face}
        # xi: The xi,eta coordinates along the face we are looking at
        # Returns:
        # x_dir_traction, y_dir_traction
        if(e[2] == 1 && face[1] == 2 && face[2] == 1)

            # The traction goes against the surface, normal to the surface
            # "try03"
            dXdxi_val = domain_dXdxi(self.mesh,e,reshape(xi,2,1))
            dXdxi_val_ds = domain_dXdsdxi(self.mesh,e,reshape(xi,2,1),s_dir)
            xvec = dXdxi_val[1,2]
            xvec_ds = dXdxi_val_ds[1,2]
            yvec = dXdxi_val[2,2]
            yvec_ds = dXdxi_val_ds[2,2]
            norm_vec = norm([xvec,yvec])
            norm_vec_ds = dot([xvec,yvec],[xvec_ds,yvec_ds])/norm_vec
            xvec_unit_ds = (xvec_ds * norm_vec - xvec * norm_vec_ds) / (norm_vec^2)
            yvec_unit_ds = (yvec_ds * norm_vec - yvec * norm_vec_ds) / (norm_vec^2)
            return xvec_unit_ds,yvec_unit_ds

        end
        return 0.0,0.0
    end


dim_p = 2
dim_s = 2
p = [2,2]
n = [4,3]
span = n .- 1

knot_v = Array[[0., 0, 0, 0.5, 1, 1, 1],[0., 0, 0, 1, 1, 1]]

#c_pts = zeros(dim_s,n[1],n[2])
c_pts = Array{Function}(undef,dim_s,n[1],n[2])

#### c_pts[:,1,1] = [3.0, 0.0]
c_pts[1,1,1] = function(s) 3.0 end
c_pts[2,1,1] = function(s) 0.0 end
#### c_pts[:,2,1] = [3.0, 0.3]
c_pts[1,2,1] = function(s)
    return 3.5-0.5*s[1]
end
c_pts[2,2,1] = function(s)
    return 0.0 + 0.5*s[1]
end
#### c_pts[:,3,1] = [3.7, 1.0]
c_pts[1,3,1] = function(s)
    return 4.0-0.5*s[1]
end
c_pts[2,3,1] = function(s)
    return 0.5 + 0.5*s[1]
end
#### c_pts[:,4,1] = [4.0, 1.0]
c_pts[1,4,1] = function(s) 4.0 end
c_pts[2,4,1] = function(s) 1.0 end


#### c_pts[:,1,2] = [1.5, 0.0]
c_pts[1,1,2] = function(s) 1.5 end
c_pts[2,1,2] = function(s) 0.0 end
#### c_pts[:,2,2] = [1.5, 0.7]
c_pts[1,2,2] = function(s) 1.5 end
c_pts[2,2,2] = function(s) 0.7 end
#### c_pts[:,3,2] = [3.3, 2.5]
c_pts[1,3,2] = function(s) 3.3 end
c_pts[2,3,2] = function(s) 2.5 end
#### c_pts[:,4,2] = [4.0, 2.5]
c_pts[1,4,2] = function(s) 4.0 end
c_pts[2,4,2] = function(s) 2.5 end


#### c_pts[:,1,3] = [0.0, 0.0]
c_pts[1,1,3] = function(s) 0.0 end
c_pts[2,1,3] = function(s) 0.0 end
#### c_pts[:,2,3] = [0.0, 4.0]
c_pts[1,2,3] = function(s) 0.0 end
c_pts[2,2,3] = function(s) 4.0 end
#### c_pts[:,3,3] = [0.0, 4.0]
c_pts[1,3,3] = function(s) 0.0 end
c_pts[2,3,3] = function(s) 4.0 end
#### c_pts[:,4,3] = [4.0, 4.0]
c_pts[1,4,3] = function(s) 4.0 end
c_pts[2,4,3] = function(s) 4.0 end


weights = Array{Array{Float64}}(undef,2)
weights[1] = Array{Float64}([1., 1., 1., 1.])
weights[2] = Array{Float64}([1., 1., 1.])

shape_p = 0.8 #0.37
geometry = Nurbs(dim_p, dim_s, p, knot_v, span, c_pts, weights; s=Array{Float64}([shape_p]))
YoungsModE = 5.0e3
PoissonsRatNu = 0.3


nel = [2,1]
ndofpernode = 2
self = IGAGrid2D(geometry,
                nel,
                ndofpernode,
                YoungsModE,
                PoissonsRatNu,
                dirichletConstraint,
                neumannBCForElementFace,
                neumannBCDsForElementFace,
                SSLinearElastic.bodyForceForElement_Zeros,
                SSLinearElastic.FextForElement,
                SSLinearElastic.FextDsForElement,
                SSLinearElastic.NForElement,
                SSLinearElastic.NDsForElement,
                SSLinearElastic.consistentTangentForElement,
                SSLinearElastic.consistentTangentDsForElement;
                nloadsteps=0)

Solve(self)

using PyPlot; plt = PyPlot;
closeplots = false


res = [80,80]
X,t,h = draw(self.geom,res)
plt.figure()
plt.scatter(X[1,:],X[2,:],color="b")
plt.scatter(self.geom.control_pts[1,:,:],self.geom.control_pts[2,:,:],color="r")
plt.xlim(-1.75,5.75)
plt.ylim(-1.0,5.0)
plt.title("Original Geometry - Before Refinement")
plt.savefig("plots_ignore/plot_2Dprob_geom01.pdf")
if(closeplots)
    plt.close()
end

res = [80,80]
X,t,h = draw(self.mesh,res)
plt.figure()
plt.scatter(X[1,:],X[2,:],color="b")
plt.scatter(self.mesh.control_pts[1,:,:],self.mesh.control_pts[2,:,:],color="r")
plt.xlim(-1.75,5.75)
plt.ylim(-1.0,5.0)
plt.title("Geometry")
plt.savefig("plots_ignore/plot_2Dprob_geom02.pdf")
if(closeplots)
    plt.close()
end

newmesh = Nurbs(self.mesh)
newmesh.control_pts = newmesh.control_pts + self.d
res = [80,80]
X,t,h = draw(newmesh,res)
plt.figure()
plt.scatter(X[1,:],X[2,:],color="b")
plt.scatter(newmesh.control_pts[1,:,:],newmesh.control_pts[2,:,:],color="r")
plt.xlim(-1.75,5.75)
plt.ylim(-1.0,5.0)
plt.title("With Deflection")
plt.savefig("plots_ignore/plot_2Dprob_geom02b.pdf")
if(closeplots)
    plt.close()
end

X,Y,F = solution(self,1,[25,25])
plt.figure()
plt.pcolor(X,Y,F)
plt.colorbar()
plt.title("disp_x")
plt.savefig("plots_ignore/plot_2Dprob_disp01.pdf")
if(closeplots)
    plt.close()
end

X,Y,F = solution(self,2,[25,25])
plt.figure()
plt.pcolor(X,Y,F)
plt.colorbar()
plt.title("disp_y")
plt.savefig("plots_ignore/plot_2Dprob_disp02.pdf")
if(closeplots)
    plt.close()
end

X,Y,Fx = solution(self,1,[25,25])
X,Y,Fy = solution(self,2,[25,25])
normF = zeros(size(Fx))
for i = 1:length(Fx[:])
    normF[i] = sqrt(Fx[i]^2 + Fy[i]^2)
end
plt.figure()
plt.pcolor(X,Y,normF)
plt.colorbar()
plt.title("disp_norm")
plt.savefig("plots_ignore/plot_2Dprob_disp03.pdf")
if(closeplots)
    plt.close()
end



##################################
#### Test the d/ds derivative ####
##################################

values = collect(0.0:0.01:1.0)

# Check the displacement_ds
disp_arr = zeros(length(values))
disp_ds_arr = zeros(length(values))
disp_ds_fd_arr = zeros(length(values))

# Check the strain energy d/ds
strEnergy_arr = zeros(length(values))
strEnergy_ds_arr = zeros(length(values))
strEnergy_ds_fd_arr = zeros(length(values))

closeplots = false

node_xi = nothing
node_eta = nothing
println("shape_p   disp   disp_ds   disp_ds_fd_arr[i]   (disp_ds-disp_ds_fd_arr[i])")
for frame_i = 1:length(values)
    global shape_p
    global geometry
    global YoungsModE
    global PoissonsRatNu
    global nel
    global ndofpernode
    global self
    global node_xi
    global node_eta

    shape_p = values[frame_i]
    i = frame_i

    geometry = Nurbs(dim_p, dim_s, p, knot_v, span, c_pts, weights; s=Array{Float64}([shape_p]))

    YoungsModE = 5.0e3
    PoissonsRatNu = 0.3
    nel = [2,1]

    ndofpernode = 2
    self = IGAGrid2D(geometry,
                    nel,
                    ndofpernode,
                    YoungsModE,
                    PoissonsRatNu,
                    dirichletConstraint,
                    neumannBCForElementFace,
                    neumannBCDsForElementFace,
                    SSLinearElastic.bodyForceForElement_Zeros,
                    SSLinearElastic.FextForElement,
                    SSLinearElastic.FextDsForElement,
                    SSLinearElastic.NForElement,
                    SSLinearElastic.NDsForElement,
                    SSLinearElastic.consistentTangentForElement,
                    SSLinearElastic.consistentTangentDsForElement;
                    nloadsteps=0,
                    runtimeoutput=false)

    Solve(self)

    # Check the displacement_ds
    node_xi = 2
    node_eta = 1
    disp = self.d[2,node_xi,node_eta]
    disp_ds = self.d_ds[1][2,node_xi,node_eta]

    str_energy = (self.Fext_vec'*self.d_vec)[1]
    str_energy_ds = (self.Fext_vec'*self.d_vec_ds[1]+self.Fext_vec_ds[1]'*self.d_vec)[1]
    strEnergy_arr[i] = str_energy
    strEnergy_ds_arr[i] = str_energy_ds
    disp_arr[i] = disp
    disp_ds_arr[i] = disp_ds
    if(i > 1)
        disp_ds_fd_arr[i] = (disp_arr[i]-disp_arr[i-1])/(values[i]-values[i-1])
        strEnergy_ds_fd_arr[i] = (strEnergy_arr[i]-strEnergy_arr[i-1])/(values[i]-values[i-1])
    end
    @printf "%0.16f    %0.8f     %0.8f     %0.8f     %0.8f\n" shape_p disp disp_ds disp_ds_fd_arr[i] (disp_ds-disp_ds_fd_arr[i])
    println("strain_energy: ",str_energy, "  strain_energy_ds:  ",str_energy_ds)

end

plt.figure()
plt.plot(collect(values),disp_arr)
plt.legend(["Displacement"])
plt.xlabel("Design parameter (s)")
plt.ylabel("y-displacement at node ("*string(node_xi)*","*string(node_eta)*")")
plt.title("Maximum y-displacement vs. design parameter (s)")
plt.savefig("plots_ignore/plot_2Dprob_disp-vs-shapep.pdf")
if(closeplots)
    plt.close()
end

plt.figure()
plt.plot(collect(values),disp_ds_arr,"r")
plt.plot(collect(values)[2:end],disp_ds_fd_arr[2:end],"g")
plt.legend(["Analytical","Finite Diff"])
plt.xlabel("Design parameter (s)")
plt.ylabel("dd/ds at node ("*string(node_xi)*","*string(node_eta)*")")
plt.title("Derivative of y-displacement w.r.t. design parameter (s)")
plt.savefig("plots_ignore/plot_2Dprob_dd_ds_finitediff_vs_analytical.pdf")
if(closeplots)
    plt.close()
end

plt.figure()
plt.plot(collect(values),strEnergy_arr)
plt.legend(["Strain Energy"])
plt.xlabel("Design parameter (s)")
plt.ylabel("Strain Energy")
plt.title("Strain Energy vs. Design Parameter (s)")
plt.savefig("plots_ignore/plot_2Dprob_strEnergy-vs-shapep.pdf")
if(closeplots)
    plt.close()
end

plt.figure()
plt.plot(collect(values),strEnergy_ds_arr,"r")
plt.plot(collect(values)[2:end],strEnergy_ds_fd_arr[2:end],"g")
plt.legend(["Analytical","Finite Diff"])
plt.xlabel("Design parameter (s)")
plt.ylabel("d_strainEnergy/ds")
plt.title("Derivative of Strain Energy w.r.t. design parameter (s)")
plt.savefig("plots_ignore/plot_2Dprob_strEnergy_ds_finitediff_vs_analytical.pdf")
if(closeplots)
    plt.close()
end





#=

#### Make a movie

closeplots = true
shape_p = collect(0.0:0.05:1.2)
for frame_i = 1:length(shape_p)

    println("frame",frame_i,"  s = ",shape_p[frame_i])

    geometry = Nurbs(dim_p, dim_s, p, knot_v, span, c_pts, weights, Array{Float64}([shape_p[frame_i]]))
    YoungsModE = 5.0e3
    PoissonsRatNu = 0.3

    nel = [4,4]
    ndofpernode = 2
    self = IGAGrid2D(geometry,
                    nel,
                    ndofpernode,
                    YoungsModE,
                    PoissonsRatNu,
                    dirichletConstraint,
                    neumannBCForElementFace,
                    neumannBCDsForElementFace,
                    SSLinearElastic.bodyForceForElement_Zeros,
                    SSLinearElastic.FextForElement,
                    SSLinearElastic.FextDsForElement,
                    SSLinearElastic.NForElement,
                    SSLinearElastic.NDsForElement,
                    SSLinearElastic.consistentTangentForElement,
                    SSLinearElastic.consistentTangentDsForElement;
                    nloadsteps=0,
                    runtimeoutput=false)

    Solve(self)

    X,Y,Fx = solution(self,1,[25,25])
    X,Y,Fy = solution(self,2,[25,25])
    normF = zeros(size(Fx))
    for i = 1:length(Fx[:])
        normF[i] = sqrt(Fx[i]^2 + Fy[i]^2)
    end
    plt.figure()
    plt.pcolor(X,Y,normF)
    plt.colorbar()
    plt.title("disp_norm")
    if(frame_i < 10)
        filename_begginning = "plots_ignore/frame_0"
    else
        filename_begginning = "plots_ignore/frame_"
    end
    plt.savefig(filename_begginning*string(frame_i)*".pdf")
    if(closeplots)
        plt.close()
    end
end
=#






#### SCRATCH ####

#=
# The traction is unit-valued going radially outward
# from the bottom-right side of the geometry
# "try02"
X = domain_X(self.mesh,e,reshape(xi,2,1))
bot_right_x = 4.0
bot_right_y = 0.0
xvec = X[1] - bot_right_x
yvec = X[2] - bot_right_y
norm_vec = norm([xvec,yvec])
xvec_unit = xvec/norm_vec
yvec_unit = yvec/norm_vec
return xvec_unit,yvec_unit
=#

# Traction in generally the left-up direction
# "try01"
#return -1.0,1.0
