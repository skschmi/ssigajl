# Example - 2D IGA
# Basic 2D linear elastic problem in IGA

# Beam pinned on the left side, traction force on the right face.

include("../sslinearelastic.jl")

using .SSLinearElastic

using .SSLinearElastic.SSIGA2D
import .SSLinearElastic.SSIGA2D.IGAGrid2D
import .SSLinearElastic.SSIGA2D.delBasis
import .SSLinearElastic.SSIGA2D.Solve
import .SSLinearElastic.SSIGA2D.GetStrainDispMatB
import .SSLinearElastic.SSIGA2D.solution

import .SSLinearElastic.SSIGA2D.SSNurbsTools.Nurbs
import .SSLinearElastic.SSIGA2D.SSNurbsTools.draw
import .SSLinearElastic.SSIGA2D.SSNurbsTools.basis
import .SSLinearElastic.SSIGA2D.SSNurbsTools.Jacobian

import .SSLinearElastic.SSIGA2D.SSGaussQuad.GaussQuadIntegrate
import .SSLinearElastic.SSIGA2D.SSGaussQuad.NumGaussPts

dirichletConstraint =
    function(self::IGAGrid2D,e::Array{Int64},a::Array{Int64},dofi::Int64)
        if(e[1] == 1 && a[1] == 1)
            return true,0.0
        end
        return false,0.0
    end

neumannBCForElementFace =
    function(self::IGAGrid2D,e::Array{Int64},face::Tuple{Int64,Int64},xi::Array{Float64})
        # This returns the tractions for the specified face of the element, at position xi
        # Paramters:
        # e: Element index
        # face[1]: Parameter direction: 1={xi=-1,xi=1} faces.  2={eta=-1,eta=1} faces
        # face[2]: bottom or top face:  1={xi=-1 face} 2={xi=1 face}
        # xi: The xi,eta coordinates along the face we are looking at
        # Returns:
        # x_dir_traction, y_dir_traction
        if(e[1] == 4 && face[1] == 1 && face[2] == 2)
            return 25.0,0.0
        end
        return 0.0,0.0
    end



dim_p = 2
dim_s = 2
p = [2,2]
n = [3,3]
span = n .- 1
knot_v = Array[[0., 0, 0, 1, 1, 1],[0., 0, 0, 1, 1, 1]]
c_pts = zeros(dim_s,n[1],n[2])
c_pts[:,1,1] = [0.,0.0]
c_pts[:,2,1] = [1.,0.0]
c_pts[:,3,1] = [2.,0.0]
c_pts[:,1,2] = [0.,0.5]
c_pts[:,2,2] = [1.,0.5]
c_pts[:,3,2] = [2.,0.5]
c_pts[:,1,3] = [0.,1.0]
c_pts[:,2,3] = [1.,1.0]
c_pts[:,3,3] = [2.,1.0]
weights = Array{Array{Float64}}(undef,2)
weights[1] = Array{Float64}([1., 1., 1.])
weights[2] = Array{Float64}([1., 1., 1.])
geometry = Nurbs(dim_p, dim_s, p, knot_v, span, c_pts, weights)
YoungsModE = 5.0e2
PoissonsRatNu = 0.3


nel = [4,2]
ndofpernode = 2
self = IGAGrid2D(geometry,
                nel,
                ndofpernode,
                YoungsModE,
                PoissonsRatNu,
                dirichletConstraint,
                neumannBCForElementFace,
                SSLinearElastic.neumannBCDsForElementFace_Zeros,
                SSLinearElastic.bodyForceForElement_Zeros,
                SSLinearElastic.FextForElement,
                SSLinearElastic.FextDsForElement,
                SSLinearElastic.NForElement,
                SSLinearElastic.NDsForElement,
                SSLinearElastic.consistentTangentForElement,
                SSLinearElastic.consistentTangentDsForElement;
                nloadsteps=0)

Solve(self)

using PyPlot; plt = PyPlot;
closeplots = false

res = [50,25]
X,t,h = draw(self.mesh,res)
plt.figure()
plt.scatter(X[1,:],X[2,:],color="b")
plt.scatter(self.mesh.control_pts[1,:,:],self.mesh.control_pts[2,:,:],color="r")
plt.xlim(-0.1,2.1)
plt.ylim(-0.5,1.5)
plt.title("Geometry")
plt.savefig("plots_ignore/plot_2Dprob_geom02.pdf")
if(closeplots)
    plt.close()
end

newmesh = Nurbs(self.mesh)
newmesh.control_pts = newmesh.control_pts + self.d
res = [50,25]
X,t,h = draw(newmesh,res)
plt.figure()
plt.scatter(X[1,:],X[2,:],color="b")
plt.scatter(newmesh.control_pts[1,:,:],newmesh.control_pts[2,:,:],color="r")
plt.xlim(-0.1,2.1)
plt.ylim(-0.5,1.5)
plt.title("With Deflection")
plt.savefig("plots_ignore/plot_2Dprob_geom02b.pdf")
if(closeplots)
    plt.close()
end

q = 1.0
L = 2.0
Im = 1/12
dispEnd = q*L^2 / (8*self.E*Im)
println("dispEnd: ",dispEnd)

X,Y,F = solution(self,1,[10,10])
plt.figure()
plt.pcolor(X,Y,F)
plt.colorbar()
plt.title("disp_x")
plt.savefig("plots_ignore/plot_2Dprob_disp01.pdf")

X,Y,F = solution(self,2,[10,10])
plt.figure()
plt.pcolor(X,Y,F)
plt.colorbar()
plt.title("disp_y")
plt.savefig("plots_ignore/plot_2Dprob_disp02.pdf")
