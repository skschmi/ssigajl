# Example - 2D IGA
# Basic 2D linear elastic problem in IGA

# Testing basis functions for 2D

include("../ssiga2d.jl")
using .SSIGA2D
import .SSIGA2D.IGAGrid2D

import .SSIGA2D.SSNurbsTools.Nurbs
import .SSIGA2D.SSNurbsTools.draw
import .SSIGA2D.SSNurbsTools.basis
import .SSIGA2D.SSNurbsTools.delBasis

func = function(self::IGAGrid2D,e::Int64,a::Int64,dofi::Int64)
       return 0.0
    end

dim_p = 2
dim_s = 2
p = [2,2]
n = [3,3]
span = n .- 1
knot_v = Array[[0., 0, 0, 1, 1, 1],[0., 0, 0, 1, 1, 1]]
c_pts = zeros(dim_s,n[1],n[2])
c_pts[:,1,1] = [0., 0.0]
c_pts[:,2,1] = [1., 0.0]
c_pts[:,3,1] = [2., 0.0]
c_pts[:,1,2] = [0., 0.5]
c_pts[:,2,2] = [1., 0.5]
c_pts[:,3,2] = [2., 0.5]
c_pts[:,1,3] = [0., 1.0]
c_pts[:,2,3] = [1., 1.0]
c_pts[:,3,3] = [2., 1.0]
weights = Array{Array{Float64}}(undef,2)
weights[1] = Array{Float64}([1., 1., 1.])
weights[2] = Array{Float64}([1., 1., 1.])
geometry = Nurbs(dim_p, dim_s, p, knot_v, span, c_pts, weights)

nel = [4,2]
ndofpernode = 2
self = IGAGrid2D(geometry,
                nel,
                ndofpernode,
                1.,
                1.,
                func,
                func,
                func,
                func,
                func,
                func,
                func,
                func,
                func,
                func)

######
@show self.bmesh.knot_v

#res = [50,25]
#X,t,h = draw(self.bmesh,res)


using PyPlot; plt = PyPlot;
closeplots = true


res = [50,25]
X,t,h = draw(self.geom,res)
plt.figure()
plt.scatter(X[1,:],X[2,:],color="b")
plt.scatter(self.geom.control_pts[1,:,:],self.geom.control_pts[2,:,:],color="r")
plt.xlim(-0.1,2.1)
plt.ylim(-0.5,1.5)
plt.title("Original Geometry")
plt.savefig("plots_ignore/plot_2Dprob_geom01.pdf")
if(closeplots)
    plt.close()
end

res = [50,25]
X,t,h = draw(self.mesh,res)
plt.figure()
plt.scatter(X[1,:],X[2,:],color="b")
plt.scatter(self.mesh.control_pts[1,:,:],self.mesh.control_pts[2,:,:],color="r")
plt.xlim(-0.1,2.1)
plt.ylim(-0.5,1.5)
plt.title("After Refinement")
plt.savefig("plots_ignore/plot_2Dprob_geom02.pdf")
if(closeplots)
    plt.close()
end


res = [50,25]
X,t,h = draw(self.bmesh,res)
plt.figure()
plt.scatter(X[1,:],X[2,:],color="b")
plt.scatter(self.bmesh.control_pts[1,:,:],self.bmesh.control_pts[2,:,:],color="r")
plt.xlim(-0.1,2.1)
plt.ylim(-0.5,1.5)
plt.title("After Bezier Decomposition")
plt.savefig("plots_ignore/plot_2Dprob_geom03.pdf")
if(closeplots)
    plt.close()
end




function plotBasisAtIndex(self::IGAGrid2D,e::Array{Int64},bi::Int64,bj::Int64)
    h = 0.01
    xi = -1.0+h:h:1.0-h
    eta = -1.0+h:h:1.0-h
    XI = zeros(length(xi),length(eta))
    ETA = zeros(length(xi),length(eta))
    BASIS = zeros(length(xi),length(eta))
    for i = 1:length(xi)
        for j = 1:length(eta)
            XI[i,j] = xi[i]
            ETA[i,j] = eta[j]
            BASIS[i,j] = basis(self.mesh,e,[xi[i],eta[j]])[bi,bj]
        end
    end
    plt.figure()
    plt.surf(XI,ETA,BASIS)
    title = "bibj_"*string(bi)*"_"*string(bj)
    plt.title(title)
    plt.zlim([0.,1])
    plt.savefig("plots_ignore/plot_2Dprob_"*title*".pdf")
    if(closeplots)
        plt.close()
    end

    return BASIS
end

function plotDelBasisAtIndex(self::IGAGrid2D,e::Array{Int64},bi::Int64,bj::Int64)
    h = 0.01
    xi = -1.0+h:h:1.0-h
    eta = -1.0+h:h:1.0-h
    XI = zeros(length(xi),length(eta))
    ETA = zeros(length(xi),length(eta))
    DELBASIS_x = zeros(length(xi),length(eta))
    DELBASIS_y = zeros(length(xi),length(eta))
    for i = 1:length(xi)
        for j = 1:length(eta)
            XI[i,j] = xi[i]
            ETA[i,j] = eta[j]
            dbresult = delBasis(self.mesh,e,[bi,bj],[xi[i],eta[j]])
            DELBASIS_x[i,j] = dbresult[1]
            DELBASIS_y[i,j] = dbresult[2]
        end
    end
    plt.figure()
    plt.surf(XI,ETA,DELBASIS_x)
    title = "delx_bibj_"*string(bi)*"_"*string(bj)
    plt.title(title)
    #plt.zlim([0.,1])
    plt.savefig("plots_ignore/plot_2Dprob_"*title*".pdf")
    if(closeplots)
        plt.close()
    end

    plt.figure()
    plt.surf(XI,ETA,DELBASIS_y)
    title = "dely_bibj_"*string(bi)*"_"*string(bj)
    plt.title(title)
    #plt.zlim([0.,1])
    plt.savefig("plots_ignore/plot_2Dprob_"*title*".pdf")
    if(closeplots)
        plt.close()
    end

    return DELBASIS_x,DELBASIS_y
end


element = [1,2]

BASIS = nothing
for bi = 1:3
    for bj = 1:3
        global BASIS = plotBasisAtIndex(self,element,bi,bj)
    end
end

DELBASIS_x = nothing
DELBASIS_y = nothing
for bi = 1:3
    for bj = 1:3
        global DELBASIS_x
        global DELBASIS_y
        DELBASIS_x, DELBASIS_y = plotDelBasisAtIndex(self,element,bi,bj)
    end
end
