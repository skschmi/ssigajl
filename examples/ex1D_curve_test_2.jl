
include("../ssnurbstoolsjl/nurbstools.jl")

using .SSNurbsTools
import .SSNurbsTools.Nurbs
import .SSNurbsTools.draw
import .SSNurbsTools.basis
import .SSNurbsTools.Jacobian
import .SSNurbsTools.e_xi_for_t
import .SSNurbsTools.domain_X

dim_p = 1
dim_s = 2
p = [3]
n = [6]
span = n .- 1
knot_v = Array[[0.,  0., 0, 0, 3, 3, 6, 6, 6,  6]]
c_pts = zeros(dim_s,n[1])
c_pts[:,1] = [1.,0.]
c_pts[:,2] = [0.,1.]
c_pts[:,3] = [1.,2.]
c_pts[:,4] = [2.,2.]
c_pts[:,5] = [3.,1.]
c_pts[:,6] = [2.,0.]
weights = Array{Array{Float64}}(undef,1)
weights[1] = Array{Float64}([1.,1.,1.,1.,1.,1.])
self = Nurbs(dim_p, dim_s, p, knot_v, span, c_pts, weights)



res = [50]
X,t,h = draw(self,res)

using PyPlot; plt = PyPlot;
closeplots = false

plt.figure()
plt.plot(X[1,:],X[2,:],color="b")
plt.scatter(self.control_pts[1,:],self.control_pts[2,:],color="r")
#plt.xlim(-0.1,2.1)
#plt.ylim(-0.5,1.5)
plt.title("self")
#plt.savefig("plots_ignore/plot_2Dprob_geom02.pdf")

e,xi = e_xi_for_t(self,[1/3])
xval = domain_X(self,e,reshape(xi,1,1))
@show xval

@show self.knot_v
bmesh = Nurbs(self)
SSNurbsTools.ConvertToBernsteinMesh(bmesh)
@show bmesh.knot_v
