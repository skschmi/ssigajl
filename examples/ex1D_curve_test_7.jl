
include("../ssnurbstoolsjl/nurbstools.jl")

using .SSNurbsTools
import .SSNurbsTools.Nurbs
import .SSNurbsTools.draw
import .SSNurbsTools.basis
import .SSNurbsTools.Jacobian
import .SSNurbsTools.e_xi_for_t
import .SSNurbsTools.domain_X
import .SSNurbsTools.domain_dXdt
import .SSNurbsTools.domain_dXdxi

dim_p = 1
dim_s = 2
p = [2]
n = [5]
span = n .- 1

knot_v = Array[[0.,   0., 0., 1, 3, 4, 4,   4]]

c_pts = zeros(dim_s,n[1])
c_pts[:,1] = [ 0., 10.]
c_pts[:,2] = [ 5., 15.]
c_pts[:,3] = [10., 15.]
c_pts[:,4] = [15.,  5.]
c_pts[:,5] = [10.,  0.]
weights = Array{Array{Float64}}(undef,1)
weights[1] = Array{Float64}([1.,1.,1.,1.,1.])
self = Nurbs(dim_p, dim_s, p, knot_v, span, c_pts, weights)


# Print the first and second derivatives of the curve eval
# at a particular global curve parameter value t.
xi = Array{Float64,2}(undef,1,1)

xi[1,1] = 0.0
@show (xi .+ 1.)/2. ## Converting for parameterization 0...1

for e=1:3
    @show e
    deriv0_result = domain_X(self,[e],xi)
    deriv1_result = domain_dXdxi(self,[e],xi)
    @show deriv0_result
    @show deriv1_result*2  ## Converting for parameterization 0...1
end

#=
res = [50]
X,t,h = draw(self,res)

using PyPlot; plt = PyPlot;
closeplots = false

plt.figure()
plt.scatter(X[1,:],X[2,:],color="b")
plt.scatter(self.control_pts[1,:],self.control_pts[2,:],color="r")
plt.xlim(-5,20)
plt.ylim(-5,20)
plt.title("1d curve test 5")
#plt.savefig("plots_ignore/plot_2Dprob_geom02.pdf")

@show self.knot_v
bmesh = Nurbs(self)
#SSNurbsTools.ConvertToBernsteinMesh(bmesh)
#@show bmesh.knot_v
=#
