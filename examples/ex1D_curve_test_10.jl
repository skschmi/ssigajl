
include("../ssnurbstoolsjl/nurbstools.jl")

using .SSNurbsTools
import .SSNurbsTools.Nurbs
import .SSNurbsTools.draw
import .SSNurbsTools.basis
import .SSNurbsTools.Jacobian
import .SSNurbsTools.e_xi_for_t
import .SSNurbsTools.domain_X
import .SSNurbsTools.ConvertToBernsteinMesh
import .SSNurbsTools.e_a_xi_for_gb_t

# Spiral curve (spring) in spatial-3D

dim_p = 1
dim_s = 3
p = [2]
n = [17]
spans = n .- 1
knot_v = Array[[ 0., 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 8 ]]

c_pts = zeros(dim_s,n[1])
c_pts[:,1] = [ 0.,  1., 0.0 ]
c_pts[:,2] = [ 1.,  1., 0.1 ]
c_pts[:,3] = [ 1.,  0., 0.2 ]
c_pts[:,4] = [ 1., -1., 0.3 ]
c_pts[:,5] = [ 0., -1., 0.4 ]
c_pts[:,6] = [-1., -1., 0.5 ]
c_pts[:,7] = [-1.,  0., 0.6 ]
c_pts[:,8] = [-1.,  1., 0.7 ]
c_pts[:,9] = [ 0.,  1., 0.8 ]
c_pts[:,10] = [ 1.,  1., 0.9 ]
c_pts[:,11] = [ 1.,  0., 1.0 ]
c_pts[:,12] = [ 1., -1., 1.1 ]
c_pts[:,13] = [ 0., -1., 1.2 ]
c_pts[:,14] = [-1., -1., 1.3 ]
c_pts[:,15] = [-1.,  0., 1.4 ]
c_pts[:,16] = [-1.,  1., 1.5 ]
c_pts[:,17] = [ 0.,  1., 1.6 ]
weights = Array{Array{Float64}}(undef,1)
weights[1] = Array{Float64}(
	[ 1., 1/sqrt(2), 1., 1/sqrt(2), 1.,  1/sqrt(2),
	  1.,  1/sqrt(2), 1., 1/sqrt(2), 1., 1/sqrt(2),
	  1., 1/sqrt(2), 1., 1/sqrt(2), 1. ])
self = Nurbs(dim_p, dim_s, p, knot_v, spans, c_pts, weights)

using PyPlot; plt = PyPlot;

# Drawing the curve
res = [200]
X,t,h = draw(self,res)
plt.figure()
plt.scatter3D(X[1,:,:],X[2,:,:],X[3,:,:])
plt.scatter3D(self.control_pts[1,:,:],self.control_pts[2,:,:],self.control_pts[3,:,:],color="r")
plt.xlabel("x")
plt.ylabel("y")
plt.zlabel("z");
plt.title("3D Line")
