



module SSLinearElastic

include("./ssiga2d.jl")
using .SSIGA2D
import .SSIGA2D.IGAGrid2D
import .SSIGA2D.delBasis
import .SSIGA2D.Solve
import .SSIGA2D.GetStrainDispMatB
import .SSIGA2D.GetStrainDispMatB_ds
import .SSIGA2D.ElementFaceIntegral
import .SSIGA2D.ElementFaceIntegral_ds
import .SSIGA2D.retrieveQuantityForElement
import .SSIGA2D.solution

import .SSIGA2D.SSNurbsTools.Nurbs
import .SSIGA2D.SSNurbsTools.draw
import .SSIGA2D.SSNurbsTools.basis
import .SSIGA2D.SSNurbsTools.basis_ds
import .SSIGA2D.SSNurbsTools.Jacobian
import .SSIGA2D.SSNurbsTools.Jacobian_ds
import .SSIGA2D.SSNurbsTools.domain_dXdsdxi

import .SSIGA2D.SSGaussQuad.GaussQuadIntegrate
import .SSIGA2D.SSGaussQuad.NumGaussPts

# Linear algebra library
using LinearAlgebra



function dirichletConstraint_Example(self::IGAGrid2D,e::Array{Int64},a::Array{Int64},dofi::Int64)
    if(e[1] == 1 && a[1] == 1)
        return true,0.0
    end
    return false,0.0
end


function neumannBCForElementFace_Zeros(self::IGAGrid2D,e::Array{Int64},face::Tuple{Int64,Int64},xi::Array{Float64})
    # This returns the tractions for the specified face of the element
    # Paramters:
    # e: Element index
    # face[1]: Parameter direction: 1={xi=-1,xi=1} faces.  2={eta=-1,eta=1} faces
    # face[2]: bottom or top face:  1={xi=-1 face} 2={xi=1 face}
    # Returns:
    # x_dir_traction, y_dir_traction
    return 0.0,0.0
end


function neumannBCDsForElementFace_Zeros(self::IGAGrid2D,e::Array{Int64},face::Tuple{Int64,Int64},xi::Array{Float64},s_dir::Int64)
    # This returns the tractions for the specified face of the element, at position xi
    # Paramters:
    # e: Element index
    # face[1]: Parameter direction: 1={xi=-1,xi=1} faces.  2={eta=-1,eta=1} faces
    # face[2]: bottom or top face:  1={xi=-1 face} 2={xi=1 face}
    # xi: The xi,eta coordinates along the face we are looking at
    # Returns:
    # x_dir_traction, y_dir_traction
    return 0.0,0.0
end


function bodyForceForElement_Zeros(self::IGAGrid2D,e::Array{Int64})
    # Returns g_x, g_y
    return 0.0, 0.0
end


function FextForElement(self::IGAGrid2D,e::Array{Int64})
    # Init the resulting array
    Fext = zeros(self.ndofpernode,self.nnodesperel[1],self.nnodesperel[2])

    # Body Forces
    jacobianf = function(params)
                    jac = Jacobian(self.mesh,e,params)
                    return det(jac)
                end
    ngp = NumGaussPts(self.max_poly_degree^2)
    f = function(params)
            ans = zeros(self.ndofpernode,self.nnodesperel[1],self.nnodesperel[2])
            R = basis(self.mesh,e,params)
            g_x, g_y = self.bodyForceForElement(self,e)
            ans[1,:,:] = R*g_x
            ans[2,:,:] = R*g_y
            return ans
        end
    Fext[:,:,:] += GaussQuadIntegrate(f,self.GQT2D[ngp],jacobianf)

    # Tractions
    for face = 1:2
        for p_dir = 1:2
            for dof_dir = 1:2
                T_function =
                    function(self_,e_,face_,xi_)
                        return self.neumannBCForElementFace(self_,e_,face_,xi_)[dof_dir]
                    end
                FI = ElementFaceIntegral(self,e,(p_dir,face),T_function)
                for a2 = 1:self.nnodesperel[2]
                    for a1 = 1:self.nnodesperel[1]
                        sub_fext = FI[a1,a2]
                        Fext[dof_dir,a1,a2] += sub_fext
                    end
                end
            end
        end
    end

    return Fext
    # Returns array of dimension ndof,nnodes_xi1,nnodes_xi2
    # containing the external force value on each node in the element
    #return zeros(self.ndofpernode,self.nnodesperel[1],self.nnodesperel[2])
end


function FextDsForElement(self::IGAGrid2D,e::Array{Int64},s_dir::Int64)

    # Init the resulting array
    Fext_ds = zeros(self.ndofpernode,self.nnodesperel[1],self.nnodesperel[2])

    # Number of gauss points
    ngp = NumGaussPts(self.max_poly_degree^2)

    # Jacobian functions
    jacobianf = function(params)
                    jac = Jacobian(self.mesh,e,params)
                    return det(jac)
                end
    jacobian_ds_f = function(params)
                    # det(J)_ds = det(J)*tr(J_inv * J_ds)
                    J = Jacobian(self.mesh,e,params)
                    J_ds = Jacobian_ds(self.mesh,e,params,s_dir)
                    J_inv = inv(J)
                    return det(J)*tr(J_inv*J_ds)
                end

    # Body Forces
    f1 = function(params)
            ans = zeros(self.ndofpernode,self.nnodesperel[1],self.nnodesperel[2])
            R_ds = basis_ds(self.mesh,e,params,s_dir)
            g_x, g_y = self.bodyForceForElement(self,e)
            ans[1,:,:] = R_ds*g_x
            ans[2,:,:] = R_ds*g_y
            return ans
        end
    Fext_ds[:,:,:] += GaussQuadIntegrate(f1,self.GQT2D[ngp],jacobianf)
    f2 = function(params)
            ans = zeros(self.ndofpernode,self.nnodesperel[1],self.nnodesperel[2])
            R = basis(self.mesh,e,params)
            g_x, g_y = self.bodyForceForElement(self,e)
            ans[1,:,:] = R*g_x
            ans[2,:,:] = R*g_y
            return ans
        end
    Fext_ds[:,:,:] += GaussQuadIntegrate(f2,self.GQT2D[ngp],jacobian_ds_f)

    # Tractions
    for face = 1:2
        for p_dir = 1:2
            for dof_dir = 1:2
                T_function =
                    function(self_,e_,face_,xi_)
                        return self.neumannBCForElementFace(self_,e_,face_,xi_)[dof_dir]
                    end
                T_function_ds =
                    function(self_,e_,face_,xi_,s_dir_)
                        return self.neumannBCDsForElementFace(self_,e_,face_,xi_,s_dir_)[dof_dir]
                    end
                FI_ds = ElementFaceIntegral_ds(self,e,(p_dir,face),T_function,T_function_ds,s_dir)
                for a2 = 1:self.nnodesperel[2]
                    for a1 = 1:self.nnodesperel[1]
                        sub_fest_ds = FI_ds[a1,a2]
                        Fext_ds[dof_dir,a1,a2] += sub_fest_ds
                    end
                end
            end
        end
    end

    return Fext_ds
    # Returns array of dimension ndof,nnodes_xi1,nnodes_xi2
    # containing the derivative w.r.t. the design params of the external
    # force value on each node in the element.
    #return zeros(self.ndofpernode,self.nnodesperel[1],self.nnodesperel[2])
end


function NForElement(self::IGAGrid2D,e::Array{Int64})
    Nvec = zeros(self.ndofpernode,self.nnodesperel[1],self.nnodesperel[2])
    K_el = self.K_el[e[1],e[2]]
    # Multiply the K*d = N for the element
    for a2i = 1:self.nnodesperel[2]
        for a1i = 1:self.nnodesperel[1]
            for a2j = 1:self.nnodesperel[2]
                for a1j = 1:self.nnodesperel[1]
                    for dofj = 1:self.ndofpernode
                        for dofi = 1:self.ndofpernode
                            # I'm sorry this is so opaque
                            Kel_val = K_el[a1i,a2i,a1j,a2j][dofi,dofj]
                            dval = self.d[ dofj, self.mesh.IEN[1][a1j,e[1]], self.mesh.IEN[2][a2j,e[2]] ]
                            Nvec[dofi,a1i,a2i] += Kel_val * dval
                        end
                    end
                end
            end
        end
    end
    return Nvec
    # Returns array of dimension ndof,nnodes_xi1,nnodes_xi2
    # containing the N values for each node in the element
    #return zeros(self.ndofpernode,self.nnodesperel[1],self.nnodesperel[2])
end


function NDsForElement(self::IGAGrid2D,e::Array{Int64},s_dir::Int64)
    # Init the resulting array
    Nvec_ds = zeros(self.ndofpernode,self.nnodesperel[1],self.nnodesperel[2])
    K_ds_el = self.K_ds_el[s_dir][e[1],e[2]]
    # Multiply the K_ds*d = N_ds for the element
    for a2i = 1:self.nnodesperel[2]
        for a1i = 1:self.nnodesperel[1]
            for a2j = 1:self.nnodesperel[2]
                for a1j = 1:self.nnodesperel[1]
                    for dofj = 1:self.ndofpernode
                        for dofi = 1:self.ndofpernode
                            # I'm sorry this is so opaque
                            Kdsel_val = K_ds_el[a1i,a2i,a1j,a2j][dofi,dofj]
                            dval = self.d[ dofj, self.mesh.IEN[1][a1j,e[1]], self.mesh.IEN[2][a2j,e[2]] ]
                            Nvec_ds[dofi,a1i,a2i] += Kdsel_val * dval
                        end
                    end
                end
            end
        end
    end
    return Nvec_ds
    # Returns array of dimension ndof,nnodes_xi1,nnodes_xi2
    # containing the N_ds values for each node in the element
    #return zeros(self.ndofpernode,self.nnodesperel[1],self.nnodesperel[2])
end


function consistentTangentForElement(self::IGAGrid2D,e::Array{Int64})
    jacobianf = function(params)
                    jac = Jacobian(self.mesh,e,params)
                    return det(jac)
                end

    ngp = NumGaussPts(self.max_poly_degree^2)

    # Returns a 4-dimensional array, each value of which contains a matrix.
    # Array[node_xi1_i,node_xi2_i,node_xi1_j,node_xi2_j] = K matrix mini-matrix for (node1,node2)
    Result = Array{Array{}}(undef,self.nnodesperel[1],self.nnodesperel[2],self.nnodesperel[1],self.nnodesperel[2])
    for n2j = 1:self.nnodesperel[2]
        for n1j = 1:self.nnodesperel[1]
            for n2i = 1:self.nnodesperel[2]
                for n1i = 1:self.nnodesperel[1]
                    node_a = [n1i,n2i]
                    node_b = [n1j,n2j]
                    # The function evaluated in the integral
                    f = function(params)
                            Ba = nothing
                            Bb = nothing
                            begin
                                Ba = GetStrainDispMatB(self,e,node_a,params)
                                Bb = GetStrainDispMatB(self,e,node_b,params)
                            end
                            BtDB = Ba[:,:]'*self.D*Bb[:,:]
                            return BtDB
                        end
                    Result[n1i,n2i,n1j,n2j] = GaussQuadIntegrate(f,self.GQT2D[ngp],jacobianf)
                    #Result[n1i,n2i,n1j,n2j] = zeros(self.ndofpernode,self.ndofpernode)
                end
            end
        end
    end
    return Result
end

function consistentTangentDsForElement(self::IGAGrid2D,e::Array{Int64},s_dir::Int64)

    jacobianf = function(params)
                    jac = Jacobian(self.mesh,e,params)
                    return det(jac)
                end

    jacobian_ds_f = function(params)
                    # det(J)_ds = det(J)*tr(J_inv * J_ds)
                    J = Jacobian(self.mesh,e,params)
                    J_ds = Jacobian_ds(self.mesh,e,params,s_dir)
                    J_inv = inv(J)
                    return det(J)*tr(J_inv*J_ds)
                end

    ngp = NumGaussPts(self.max_poly_degree^2)

    # Returns a 4-dimensional array, each value of which contains a matrix.
    # Array[node_xi1_i,node_xi2_i,node_xi1_j,node_xi2_j] = K matrix mini-matrix for (node1,node2)

    # First, fill each value up with zeros
    Result = Array{Array{}}(undef,self.nnodesperel[1],self.nnodesperel[2],self.nnodesperel[1],self.nnodesperel[2])
    for n2j = 1:self.nnodesperel[2]
        for n1j = 1:self.nnodesperel[1]
            for n2i = 1:self.nnodesperel[2]
                for n1i = 1:self.nnodesperel[1]
                    Result[n1i,n2i,n1j,n2j] = zeros(self.ndofpernode,self.ndofpernode)
                end
            end
        end
    end

    # Term 1
    for n2j = 1:self.nnodesperel[2]
        for n1j = 1:self.nnodesperel[1]
            for n2i = 1:self.nnodesperel[2]
                for n1i = 1:self.nnodesperel[1]
                    node_a = [n1i,n2i]
                    node_b = [n1j,n2j]
                    # The function evaluated in the integral
                    f = function(params)
                            Ba = nothing
                            Bb = nothing
                            begin
                                Ba = GetStrainDispMatB_ds(self,e,node_a,params,s_dir)
                                Bb = GetStrainDispMatB(self,e,node_b,params)
                            end
                            BtDB = Ba[:,:]'*self.D*Bb[:,:]
                            return BtDB
                        end
                    Result[n1i,n2i,n1j,n2j] += GaussQuadIntegrate(f,self.GQT2D[ngp],jacobianf)
                end
            end
        end
    end

    # Term 2
    for n2j = 1:self.nnodesperel[2]
        for n1j = 1:self.nnodesperel[1]
            for n2i = 1:self.nnodesperel[2]
                for n1i = 1:self.nnodesperel[1]
                    node_a = [n1i,n2i]
                    node_b = [n1j,n2j]
                    # The function evaluated in the integral
                    f = function(params)
                            Ba = nothing
                            Bb = nothing
                            begin
                                Ba = GetStrainDispMatB(self,e,node_a,params)
                                Bb = GetStrainDispMatB_ds(self,e,node_b,params,s_dir)
                            end
                            BtDB = Ba[:,:]'*self.D*Bb[:,:]
                            return BtDB
                        end
                    Result[n1i,n2i,n1j,n2j] += GaussQuadIntegrate(f,self.GQT2D[ngp],jacobianf)
                end
            end
        end
    end

    # Term 3
    for n2j = 1:self.nnodesperel[2]
        for n1j = 1:self.nnodesperel[1]
            for n2i = 1:self.nnodesperel[2]
                for n1i = 1:self.nnodesperel[1]
                    node_a = [n1i,n2i]
                    node_b = [n1j,n2j]
                    # The function evaluated in the integral
                    f = function(params)
                            Ba = nothing
                            Bb = nothing
                            begin
                                Ba = GetStrainDispMatB(self,e,node_a,params)
                                Bb = GetStrainDispMatB(self,e,node_b,params)
                            end
                            BtDB = Ba[:,:]'*self.D*Bb[:,:]
                            return BtDB
                        end
                    Result[n1i,n2i,n1j,n2j] += GaussQuadIntegrate(f,self.GQT2D[ngp],jacobian_ds_f)
                end
            end
        end
    end

    return Result
end














"""
function compute_integral_strain_energy(self::IGAGrid2D)
=====

Computes the integral _int dot(f,d) d_sigma
"""
function compute_integral_strain_energy(self::IGAGrid2D)

    # Number of gauss points
    ngp = NumGaussPts(self.max_poly_degree^2)

    str_energy = 0.0

    # Loop over elements
    for e1 = 1:self.nelements[1]
        for e2 = 1:self.nelements[2]

        jacobianf = function(params)
            jac = Jacobian(self.mesh,[e1,e2],params)
            return det(jac)
        end

        f = function(params)
            Fext_x_el = retrieveQuantityForElement(self,[e1,e2],1,self.Fext)
            Fext_y_el = retrieveQuantityForElement(self,[e1,e2],2,self.Fext)
            dx_el = retrieveQuantityForElement(self,[e1,e2],1,self.d)
            dy_el = retrieveQuantityForElement(self,[e1,e2],2,self.d)
            R = basis(self.mesh,[e1,e2],params)
            val = 0.0
            for i = 1:length(R)
                val += R[i]*(Fext_x_el[i]*dx_el[i] + Fext_y_el[i]*dy_el[i])
            end
            return val
        end

        # Integrate over the element
        str_energy += GaussQuadIntegrate(f,self.GQT2D[ngp],jacobianf)

        end
    end

    return str_energy
end



"""
function compute_integral_strain_energy_ds(self::IGAGrid2D)
=====

Computes the integral _int dot(f,d) d_sigma
"""
function compute_integral_strain_energy_ds(self::IGAGrid2D)

    # Number of gauss points
    ngp = NumGaussPts(self.max_poly_degree^2)

    str_energy_ds = zeros(self.mesh.ndesignvars)

    # Loop over elements
    for e1 = 1:self.nelements[1]
        for e2 = 1:self.nelements[2]

            jacobianf = function(params)
                jac = Jacobian(self.mesh,[e1,e2],params)
                return det(jac)
            end

            f = function(params)
                Fext_x_el = retrieveQuantityForElement(self,[e1,e2],1,self.Fext)
                Fext_y_el = retrieveQuantityForElement(self,[e1,e2],2,self.Fext)
                dx_el = retrieveQuantityForElement(self,[e1,e2],1,self.d)
                dy_el = retrieveQuantityForElement(self,[e1,e2],2,self.d)
                R = basis(self.mesh,[e1,e2],params)
                val = 0.0
                for i = 1:length(R)
                    val += R[i]*(Fext_x_el[i]*dx_el[i] + Fext_y_el[i]*dy_el[i])
                end
                return val
            end

            for s_dir = 1:self.mesh.ndesignvars

                jacobian_ds_f = function(params)
                    # det(J)_ds = det(J)*tr(J_inv * J_ds)
                    J = Jacobian(self.mesh,[e1,e2],params)
                    J_ds = Jacobian_ds(self.mesh,[e1,e2],params,s_dir)
                    J_inv = inv(J)
                    return det(J)*tr(J_inv*J_ds)
                end
                f_ds = function(params)
                    Fext_x_el = retrieveQuantityForElement(self,[e1,e2],1,self.Fext)
                    Fext_y_el = retrieveQuantityForElement(self,[e1,e2],2,self.Fext)
                    Fext_x_el_ds = retrieveQuantityForElement(self,[e1,e2],1,self.Fext_ds[s_dir])
                    Fext_y_el_ds = retrieveQuantityForElement(self,[e1,e2],2,self.Fext_ds[s_dir])

                    dx_el = retrieveQuantityForElement(self,[e1,e2],1,self.d)
                    dy_el = retrieveQuantityForElement(self,[e1,e2],2,self.d)
                    dx_el_ds = retrieveQuantityForElement(self,[e1,e2],1,self.d_ds[s_dir])
                    dy_el_ds = retrieveQuantityForElement(self,[e1,e2],2,self.d_ds[s_dir])

                    R = basis(self.mesh,[e1,e2],params)
                    R_ds = basis_ds(self.mesh,[e1,e2],params,s_dir)

                    val = 0.0
                    for i = 1:length(R)
                        val += R[i]*(Fext_x_el_ds[i] * dx_el[i] +
                                     Fext_x_el[i]    * dx_el_ds[i] +
                                     Fext_y_el_ds[i] * dy_el[i] +
                                     Fext_y_el[i]*dy_el_ds[i])
                        val += R_ds[i]*(Fext_x_el[i]*dx_el[i] + Fext_y_el[i]*dy_el[i])
                    end
                    return val
                end

                # Integrate over the element
                str_energy_ds[s_dir] += GaussQuadIntegrate(f,self.GQT2D[ngp],jacobian_ds_f)
                str_energy_ds[s_dir] += GaussQuadIntegrate(f_ds,self.GQT2D[ngp],jacobianf)
            end

        end
    end

    return str_energy_ds
end






end #module SSLinearElastic
