# Example - 1D IGA
# Basic 1D linear elastic problem in IGA

# The same problem as 'Coding 3' in Finite Elements 507 class.

include("../ssiga.jl")
include("../sstimoshenkobeam.jl")

using SSIGA
import SSIGA.IGAGrid1D
import SSIGA.nurbsBasis
import SSIGA.Solve
import SSIGA.solve_prep
import SSIGA.solution

using SSNurbsTools
import SSNurbsTools.Nurbs
import SSNurbsTools.domain_X
import SSNurbsTools.del_bernstein_basis
import SSNurbsTools.basis
import SSNurbsTools.delBasis
import SSNurbsTools.Jacobian
import SSNurbsTools.draw

using SSGaussQuad
import SSGaussQuad.GaussQuadIntegrate
import SSGaussQuad.NumGaussPts

using SSTimoshenkoBeam
import SSTimoshenkoBeam.timoshenko_D_mat
import SSTimoshenkoBeam.timoshenko_B_mat

"""
Printing a matrix in row,col view.
"""
function matprint( A )
	return replace(string(A),";","\n")
end


#Creating the geometry
dim_p = 1
dim_s = 1
minz = 0.0
L = 1.0
maxz = minz+L

p = 1; n = 2; knot_v = [minz, minz, maxz, maxz];
#p = 2; n = 3; knot_v = [minz, minz, minz, maxz, maxz, maxz];
#p = 3; n = 4; knot_v = [minz, minz, minz, minz, maxz, maxz, maxz, maxz];

c_pts = reshape(collect(linspace(minz,maxz,n)),1,n) ### TODO: the control points should be at the grevvile abscisca.
weights = Array{Array{Float64}}(undef,1)
weights[1] = ones(size(c_pts)[2])
geometry = Nurbs( dim_p, dim_s, [p], Array[knot_v], [n-1], c_pts, weights )
nel = 50

YoungsModE = 200.0e9
PoissonsRatNu = 0.3
geom_r = 0.1
AreaCS = pi * geom_r^2
sheerCorr1 = 5./6.
sheerCorr2 = 5./6.
I1 = ( pi / 4 ) * geom_r^4
I2 = I1

ndofpernode = 6
# dof definitions:
# 1. x  ('down')
# 2. y  ('into the paper')
# 3. z  ('along the direction of the beam')
# 4. theta1
# 5. theta2
# 6. theta3

problem_number = 4
# problem_number 2:   Prismatic cantilever beam, length L, fixed at the left end
#                       with a distributed constant axial load N.
# problem_number 3:   Doubly symmetric cantilever beam of length L fixed at the
#                       left end with a constant transverse load N.
# problem_number 4:   Doubly symmetric simply supported beam (i.e., pin support at
#                       the left and roller support at the right) of length L with
#                       a linearly varying distributed load

FextForElement =
	function(self::IGAGrid1D,e::Int64)
		Fext = zeros( self.nnodesperel * self.ndofpernode )
		jacobianf = function(params)
			ans = Jacobian(self.mesh,[e],params)[1,1]
			return ans
		end
		f = function(params)
			xi = params[1]
			return basis(self.mesh,[e],[xi])' .* self.neumannBCForElement(self,e)
		end
		result = GaussQuadIntegrate(f,self.GQT1D[NumGaussPts(self.p+2)],jacobianf)
		for node_a = 1:self.nnodesperel
			Fext[ (node_a-1)*self.ndofpernode+1 : node_a*self.ndofpernode ] = result[:,node_a][:]
		end
		return reshape(Fext,self.ndofpernode,self.nnodesperel)
	end

function KpqForElement(self::IGAGrid1D,e::Int64,node_p::Int64,node_q::Int64)
	jacobianf = function(params)
			ans = Jacobian(self.mesh,[e],params)[1,1]
			return ans
		end
	f = function(params)
			xi = params[1]
			Ba = timoshenko_B_mat(self,e,node_p,xi)
			D = timoshenko_D_mat(YoungsModE, PoissonsRatNu, AreaCS, sheerCorr1, sheerCorr2, I1, I2, I1+I2 )
			Bb = timoshenko_B_mat(self,e,node_q,xi)
			ans = Ba' * D * Bb
			return ans
		end
	Kpq = GaussQuadIntegrate(f,self.GQT1D[NumGaussPts(self.p+2)],jacobianf)
	return Kpq
end

consistentTangentForElement =
	function(self::IGAGrid1D,e::Int64)
		Kel = Array{Any}(self.nnodesperel,self.nnodesperel)
		for nd_p = 1:self.nnodesperel
			for nd_q = 1:self.nnodesperel
				entry = Array{Float64,2}(KpqForElement(self,e,nd_p,nd_q))
				Kel[nd_p,nd_q] = entry
			end
		end
		return Kel
	end

NForElement =
	function(self::IGAGrid1D,e::Int64)
		## The element 'guess of the displacement' vector (including the bc dof)
		d0_el = zeros( self.nnodesperel * self.ndofpernode )
		for node_a = 1:self.nnodesperel
			node_index = self.mesh.IEN[1][node_a,e]
			d0_el[ (node_a-1)*self.ndofpernode+1 : (node_a)*self.ndofpernode ] = self.d[:,node_index]
		end

		## The element K matrix (including the bc dof)
		Kmat_el = zeros( self.nnodesperel * self.ndofpernode, self.nnodesperel * self.ndofpernode )
		for nd_p = 1:self.nnodesperel
			for nd_q = 1:self.nnodesperel
				entry = KpqForElement(self,e,nd_p,nd_q)
				Kmat_el[ (nd_p-1)*self.ndofpernode+1 : (nd_p)*self.ndofpernode, (nd_q-1)*self.ndofpernode+1 : (nd_q)*self.ndofpernode ] = entry[:,:]
			end
		end
		Fint = Kmat_el * reshape( d0_el, self.nnodesperel * self.ndofpernode, 1 )
		retval = reshape(Fint,self.ndofpernode,self.nnodesperel)
		return retval
	end

dirichletConstraint =
	function(self::IGAGrid1D,e::Int64,a::Int64,dofi::Int64)
		##
		## Specified displacements
		##
		if( problem_number == 2 )
			## Setting bc that the beam is held fixed on the far left side, for all dof.
			if( e==1 && a==1 )
				return true,0.0
			end
		elseif( problem_number == 3 )
			## Setting bc that the beam is held fixed on the far left side, for all dof.
			if( e==1 && a==1 )
				return true,0.0
			end
		elseif( problem_number == 4 )
			## Setting bc that:
			#### Pin support at the left (allows rotations but not displacements)
			if( e==1 && a==1 && (dofi==1 || dofi==2 || dofi==3) )
				return true,0.0
			end
			#### Roller support at the right (allows displacements in z but not x and y; rotations ok )
			if( e==self.nelements && a==self.nnodesperel && (dofi==1 || dofi==2) )
				return true,0.0
			end
		end
		return false,0.0
	end

neumannBCForElement =
	function(self::IGAGrid1D,e::Int64)
		##
		## Specified tractions (forces)
		##
		T = zeros(self.ndofpernode,self.nnodesperel)
		if( problem_number == 2 )
			## Setting the bc of a traction pulling to the right on all elements (distributed axial load)
			T[3,:] = 1.0
		elseif( problem_number == 3 )
			## Setting the bc of a traction pushing down for all nodes on all elements. (distributed transverse load)
			T[1,:] = 1.0
		elseif( problem_number == 4 )
			## linearly varying distributed load equal to zero at the left and a maximum value of N at the right
			#### NOTE: what I'm doing here only works for linear basis functions..
			####       what we need to do is project the linear function onto the basis to figure out what nodal values are
			####       required to represent the function.
			element_xmin = domain_X(self.mesh,[e],reshape([-1.0],1,1))[1]
			element_xmax = domain_X(self.mesh,[e],reshape([1.0],1,1))[1]
			T[1,1] = element_xmin
			T[1,2] = element_xmax
		end
		return T
	end





# Creating the grid
println( "Creating the grid" )
self = IGAGrid1D(
		geometry,
		nel,
		ndofpernode,
		dirichletConstraint,
		neumannBCForElement,
		FextForElement,
		NForElement,
		consistentTangentForElement;
		nloadsteps=0,
		max_correction_loops=10,
		epsilon=1.0e-5)

if( false )
	println("tests.")

	#=
	for e = 1:2
		thing = neumannBCForElement( self, e ); title="neumannBCForElement: ";
		println( "e: "*string(e)*"   "*title*"\n"*matprint(thing) )
	end
	=#

	#for e = 1:nel
	#=
	for e = 1:2
		thing = FextForElement( self, e ); title="FextForElement: ";
		println( "e: "*string(e)*"   "*title*"\n"*matprint(thing) )
	end
	=#

	#=
	for e = 1:1
		for node_p = 1:2
			for node_q = 1:2
				thing = KpqForElement( self, e, node_p, node_q ); title="KpqForElement: ";
				println( "e: "*string(e)*"   "*title*"\n"*matprint(thing) )
			end
		end
	end
	=#

	#=
	begin
		fill!(self.d,1)
		for e = 1:2
			thing = NForElement( self, e ); title="NForElement: ";
			println( "e: "*string(e)*"   "*title*"\n"*matprint(thing) )
		end
	end
	=#

	println("Running 'solve_prep'");
	solve_prep(self)
end


println("Running Solve")
Solve(self)

using PyPlot; plt = PyPlot;

plot_basis_funcs = false
if( plot_basis_funcs && self.nelements <= 50 )
    ### Plotting the geometry
    res = [50]
    X,t,h = SSNurbsTools.draw(self.mesh,res)
    using PyPlot; plt = PyPlot;
    closeplots = false
    plt.figure()
    plt.scatter(X[1,:],zeros(length(X)),color="b")
    plt.scatter(self.mesh.control_pts[1,:],zeros(self.mesh.control_pts),color="r")
    plt.title("Geometry")

    # Plotting all basis functions as function of global param variable t
    tmin = self.mesh.knot_v[1][1]
    tmax = self.mesh.knot_v[1][end]
    t = collect(tmin:0.001:tmax)
    plt.figure()
    ymax = 1.1
    ymin = -0.1
    for i = 1:self.nnodes
        result = nurbsBasis(self,i,t)
        if (maximum(result) > ymax)
            ymax = maximum(result)
        end
        if (minimum(result) < ymin)
            ymin = minimum(result)
        end
        plt.plot(t,result)
    end
    plt.title("nurbsBasis")
    plt.ylim((ymin,ymax))

    # Plotting one element's basis functions as a function of element param variable xi.
    xi = collect(-1:0.001:1)
    plt.figure()
    ymax = 1.1
    ymin = -0.1
    e = 1
    for a = 1:self.p+1
        result = Array{Float64}([basis(self.mesh,[e],[xi[k]])[a] for k=1:length(xi)])
        if (maximum(result) > ymax)
            ymax = maximum(result)
        end
        if (minimum(result) < ymin)
            ymin = minimum(result)
        end
        plt.plot(xi,result)
    end
    plt.ylim((ymin,ymax))
end

# Plotting the solution
for which_dof = 1:self.ndofpernode
	plt.figure()
	samplesperel = 2*p
	if(self.nelements >= 50 )
	    samplesperel = 1
	end
	z,sol = solution(self,which_dof,samplesperel)
	plt.plot(z,sol)
	solution_span = (maximum(sol) - minimum(sol))
	plt.ylim((minimum(sol)-solution_span*0.2,maximum(sol)+solution_span*0.2))
	plt.title("Timoshenko Beam Solution: n_el="*string(self.nelements)*", p="*string(self.p)*", dof="*string(which_dof))
	plt.xlabel("z (along length of beam)")
	plt.ylabel("solution: dof="*string(which_dof))
end
