

module SSNewtonMethod

function step(x0,f,dfdx,h)
    x1 = x0 - h * (f .* (1 ./ dfdx))
    return x1
end

end #module SSNewtonMethod




module SSGradientDescent

#function step(x0::Array{Float64},dfdx::Array{Float64},h::Float64)
#    x1 = x0 - h * dfdx
#    return x1
#end

function step(x0::Array{Float64},dfdx::Array{Float64},h::Float64)
    dfdx_unit = dfdx/norm(dfdx)
    x1 = x0 - h * dfdx_unit
    return x1
end

end #module SSGradientDescent





module SSGradientAscent

#function step(x0::Array{Float64},dfdx::Array{Float64},h::Float64)
#    x1 = x0 + h * dfdx
#    return x1
#end

function step(x0::Array{Float64},dfdx::Array{Float64},h::Float64)
    dfdx_unit = dfdx/norm(dfdx)
    x1 = x0 + h * dfdx_unit
    return x1
end

end #module SSGradientAscent



#
