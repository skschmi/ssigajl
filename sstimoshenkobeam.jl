

include("./ssiga2d.jl")

module SSTimoshenkoBeam

using SSIGA
import SSIGA.IGAGrid1D
import SSIGA.basis
import SSIGA.delBasis

import SSNurbsTools.Nurbs
import SSNurbsTools.draw
import SSNurbsTools.basis
import SSNurbsTools.basis_ds
import SSNurbsTools.Jacobian
import SSNurbsTools.Jacobian_ds
import SSNurbsTools.domain_dXdsdxi

import SSGaussQuad.GaussQuadIntegrate
import SSGaussQuad.NumGaussPts


"""
function timoshenko_B_mat(self::IGAGrid2D,e::Int64,a::Int64)
=====
# Parameters
* e:  The element index
* a:  The node index.
* xi: The parameter to be evaluated.

Returns the 'B' matrix for timoshenko beams, used when computing
K_ab = \int transpose(B_a) * D * B_b  dx
"""
function timoshenko_B_mat(self::IGAGrid1D,e::Int64,a::Int64,xi::Float64)
	Bmat = zeros(6,6)
	dNdx_val = delBasis(self.mesh,[e],[a],[xi])[1,1]
	N_val = basis(self.mesh,[e],[a],[xi])
	
	Bmat[1,3] = dNdx_val
	Bmat[2,1] = dNdx_val
	Bmat[2,5] = -N_val

	Bmat[3,2] = dNdx_val
	Bmat[3,4] = N_val

	Bmat[4,4] = dNdx_val
	Bmat[5,5] = dNdx_val
	Bmat[6,6] = dNdx_val

	return Bmat
end





"""
function timoshenko_D_mat(self::IGAGrid2D)
=====
# Parameters
* E:  Young's modulus
* nu:  Poisson's ratio
* A: Cross-sectional area.
* sheerCorr1:  Sheer correction.  For rectangular cross sections, just set this to 5/6
* sheerCorr2:  Sheer correction.  For rectangular cross sections, just set this to 5/6
* I1: Moment of inertia
* I2: Moment of inertia
* J:  Polar moment of inertia.  Usually just the sum of I1 and I2

Returns the D matrix for timoshenko beams. a.k.a. the stress-strain relationship
sigma = D * epsilon.  This function returns the D matrix.
"""
function timoshenko_D_mat(
	E::Float64,
	nu::Float64,
	A::Float64,
	sheerCorr1::Float64,
	sheerCorr2::Float64,
	I1::Float64,
	I2::Float64,
	J::Float64)

	A1s = A*sheerCorr1
	A2s = A*sheerCorr2
	mu = E / (2*(1+nu))

	return diagm( [ E*A, mu*A1s, mu*A2s, E*I1, E*I2, mu*J ] )
end






function dirichletConstraint_Example(self::IGAGrid1D,e::Int64,a::Int64,dofi::Int64)
	## Specified displacements
	## Setting bc that the beam is held fixed on the far left side, for all dof.
	if( e==1 && a==1 )
		return true,0.0
	end
	return false,0.0
end

function neumannBCForElement_Zeros(self::IGAGrid1D,e::Int64)
	## Specified tractions (forces)
	T = zeros(self.ndofpernode,self.nnodesperel)
	return T
end

function FextForElement_Zeros(self::IGAGrid1D,e::Int64)
	Fext = zeros(self.nnodesperel)
	return reshape(Fext,self.ndofpernode,self.nnodesperel)
end

function NForElement_Zeros(self::IGAGrid1D,e::Int64)
	Fint = zeros(self.nnodesperel)
	retval = reshape(Fint,self.ndofpernode,self.nnodesperel)
	return retval
end

function consistentTangentForElement_Zeros(self::IGAGrid1D,e::Int64)
	Kel = Array{Any}(self.nnodesperel,self.nnodesperel)
	for nd_p = 1:self.nnodesperel
		for nd_q = 1:self.nnodesperel
			Kel[nd_p,nd_q] = 0.0
		end
	end
	return Kel
end


end #module SSTimoshenkoBeam
