

module SSIGA2D

# Library for Gaussian Quadrature
include("./ssgaussquadjl/gaussquad.jl")
using .SSGaussQuad
import .SSGaussQuad.GaussQuadTable
import .SSGaussQuad.GaussQuadIntegrate
import .SSGaussQuad.NumGaussPts

# Library for NURBS
include("./ssnurbstoolsjl/nurbstools.jl")
using .SSNurbsTools
import .SSNurbsTools.Nurbs
import .SSNurbsTools.UniqueKnotArray
import .SSNurbsTools.basis
import .SSNurbsTools.basis_ds
import .SSNurbsTools.delBasis
import .SSNurbsTools.delBasis_ds
import .SSNurbsTools.bernstein_basis_vdv
import .SSNurbsTools.domain_dXdxi
import .SSNurbsTools.domain_X
import .SSNurbsTools.ConvertToBernsteinMesh
import .SSNurbsTools.RefineMesh
import .SSNurbsTools.Jacobian
import .SSNurbsTools.Jacobian_ds

# Library for indexing
using .SSNurbsTools.SSIndexingTools
import .SSNurbsTools.SSIndexingTools.KroneckerDelta

# Linear algebra library
using LinearAlgebra

# Sparse matrices and arrays library
using SparseArrays


"""
function ElasticCoeffsCijkl(E::Float64,nu::Float64,i::Int64,j::Int64,k::Int64,l::Int64)
==========

# Parameters
* E: Young's Modulus
* nu: Poisson's Ratio
* i,j,k,l: Indices into C

# Returns:
* The coefficient in Cijkl at the specified index i,j,k,l
"""
function ElasticCoeffsCijkl(E::Float64,nu::Float64,i::Int64,j::Int64,k::Int64,l::Int64)
    lambda = nu*E/((1+nu)*(1-2*nu))
    mu = E/(2*(1+nu))
    return mu * (KroneckerDelta(i,k)*KroneckerDelta(j,l) + KroneckerDelta(i,l)*KroneckerDelta(j,k)) +
                    lambda * KroneckerDelta(i,j) * KroneckerDelta(k,l)
end






"""
function IJ_to_ijkl(I::Int64,J::Int64)
=======

# Returns:
* The index conversion from D[I,J] to C[i,j,k,l]
"""
function IJ_to_ijkl(I::Int64,J::Int64)
    chart2d = [1 1; 2 2; 2 1];
    #chart3d = [1 1; 2 2; 3 3; 1 2; 1 3; 2 3];
    return chart2d[I,1],chart2d[I,2],chart2d[J,1],chart2d[J,2]
end







"""
function BuildElasticCoeffsD(E::Float64,nu::Float64)
=========

# Parameters:
* E: Young's Modulus
* nu: Poisson's Ratio

# Returns:
* The 3x3 D matrix, equivalent (with some assumptions) to the Cijkl elastic coeffs tensor.

# Note:
* See pg 102 of the Hughes FEA book.
"""
function BuildElasticCoeffsD(E::Float64,nu::Float64)
    D = zeros(3,3)
    for J = 1:3
        for I = 1:3
            i,j,k,l = IJ_to_ijkl(I,J)
            D[I,J] = ElasticCoeffsCijkl(E,nu,i,j,k,l)
        end
    end
    return D
end



















"""
IGAGrid2D struct
==============

# Constructor:
* geometry::Nurbs
* nelements::Array{Int64}
* ndofpernode::Int64
* dirichletConstraint::Function(self::IGAGrid2D,e::Array{Int64},a::Array{Int64},dofi::Int64)
* neumannBCForElementFace::Function(self::IGAGrid2D,e::Array{Int64},a::Array{Int64},dofi::Int64)
* FextForElement::Function(self::IGAGrid2D,e::Array{Int64})
* NForElement::Function(self::IGAGrid2D,e::Array{Int64})
* consistentTangentForElement::Function::IGAGrid2D,e::Array{Int64})

# Optional/Named Parameters:
* epsilon=1e-12,
* nloadsteps=5,
* max_correction_loops=50,
* max_poly_degree=-1

"""
mutable struct IGAGrid2D

    # Geometry
    geom::Nurbs
    mesh::Nurbs
    bmesh::Nurbs

    # polynomial degree of elements, in each parametric direction
    p::Array{Int64}

    # Info for each element
    # number of elements in each parametric direction
    nelements::Array{Int64}
    # number of nodes per element, in each parametric direction
    nnodesperel::Array{Int64}
    ndofpernode::Int64

    # Info for each node:
    # number of nodes in each parametric direction
    nnodes::Array{Int64}
    #Dof for each node
    ID::Array{Int64,3}

    #Contains all displacements, including BC displacements
    d::Array{Float64,3}
    d_ds::Array{Array{Float64,3}}
    #Contains all the force-vector values, including BC forces
    Fext::Array{Float64,3}
    Fext_ds::Array{Array{Float64,3}}
    #Contains all the N-vec values
    Nvec::Array{Float64,3}
    Nvec_ds::Array{Array{Float64,3}}

    # Number of dof in the system (after BC are applied)
    dof::Int64

    # For Linear Elastic Problems
    E::Float64
    nu::Float64
    D::Array{Float64,2}

    #Values we save here for efficiency
    #K::Array{Float64,2}    # Size dof-by-dof matrix
    K::SparseMatrixCSC{}    # Size dof-by-dof matrix
    K_ds::Array{SparseMatrixCSC{},1} # Array of (Size dof-by-dof matrices), of size mesh.ndesignvars.
    K_el::Array{Array{Array{}},2} # Size nel[1]-by-nel[2]. Contains each element's K_e matrix
    K_ds_el::Array{Any}
    R::Array{Float64}      # Length dof array
    old_R::Array{Float64}  # Length dof array
    d_vec::Array{Float64}  # Length dof array: contains only
                           #  those displacements that are dof.
    d_vec_ds::Array{Array{Float64}} # Length dof array
    del_d_vec::Array{Float64}  # Length dof array: The change in displacements
    Fext_vec::Array{Float64}   # Length dof array
    Fext_vec_ds::Array{Array{Float64}} # Array of (size-dof arrays), of size mesh.ndesignvars
    Nvec_vec::Array{Float64}   # Length dof array
    Nvec_vec_ds::Array{Array{Float64}} # Array of (size-dof arrays), of size mesh.ndesignvars

    # For Gaussian Quadrature:
    max_poly_degree::Int64
    GQT1D::Array{GaussQuadTable}
    GQT2D::Array{GaussQuadTable}

    # *** Callback functions for boundary conditions ***
    dirichletConstraint::Function
    neumannBCForElementFace::Function
    neumannBCDsForElementFace::Function
    bodyForceForElement::Function

    # Important for newton-raphson:
    epsilon::Float64
    nloadsteps::Int64
    load_step_i::Int64
    max_correction_loops::Int64

    # Show output at runtime
    runtimeoutput::Bool

    # *** Callback functions for Newton-Raphson algorithm ***
    FextForElement::Function
    FextDsForElement::Function
    NForElement::Function
    NDsForElement::Function
    consistentTangentForElement::Function
    consistentTangentDsForElement::Function

    function IGAGrid2D(geometry::Nurbs,
                       nelements::Array{Int64},
                       ndofpernode::Int64,
                       E::Float64,
                       nu::Float64,
                       dirichletConstraint::Function,
                       neumannBCForElementFace::Function,
                       neumannBCDsForElementFace::Function,
                       bodyForceForElement::Function,
                       FextForElement::Function,
                       FextDsForElement::Function,
                       NForElement::Function,
                       NDsForElement::Function,
                       consistentTangentForElement::Function,
                       consistentTangentDsForElement::Function;
                       epsilon=1e-12,
                       nloadsteps=5,
                       max_correction_loops=50,
                       max_poly_degree=-1,
                       runtimeoutput=true)
        self = new()

        # The original geometry
        self.geom = Nurbs(geometry)

        # Refine the Geometry
        self.mesh = Nurbs(geometry)
        RefineMesh(self.mesh,nelements)

        # Create Bernstein Mesh (performs bezier decomposition
        #    and creates new mesh)
        self.bmesh = Nurbs(self.mesh)
        ConvertToBernsteinMesh(self.bmesh)

        # degree of the curve in each parametric direction
        self.p = copy(self.mesh.degree)

        if(max_poly_degree >= 1)
            self.max_poly_degree = max_poly_degree
        else
            max_poly_degree = maximum(self.p)+1
            self.max_poly_degree = max_poly_degree
        end

        # In each parametric direction:
        self.nelements = copy(self.mesh.nbezcurves)
        self.nnodesperel = copy( self.p .+ 1 )
        self.nnodes = copy( self.mesh.spans .+ 1 )

        # Each node:
        self.ndofpernode = ndofpernode

        # Items for Linear Elastic problems
        self.E = E # Young's Mod
        self.nu = nu # Poisson's Ratio
        self.D = BuildElasticCoeffsD(self.E,self.nu)

        # Show runtime output
        self.runtimeoutput = runtimeoutput

        # **
        # *****
        # *******
        # ***********
        # These items are highly dependent on the actual problem to be solved.

        # Tracking load-step, correction loops iterations
        self.epsilon = epsilon
        self.max_correction_loops = max_correction_loops
        self.nloadsteps = nloadsteps
        self.load_step_i = 0

        # Constraints information
        self.ID = zeros(Int64,0,0,0) # to be set later (needs constraints info)

        # Displacement array (all nodes)
        self.d = zeros(self.ndofpernode,self.nnodes[1],self.nnodes[2])
        self.d_ds = Array{Array{Float64,3}}(undef,self.mesh.ndesignvars)
        for s_i = 1:self.mesh.ndesignvars
            self.d_ds[s_i] = zeros(self.ndofpernode,self.nnodes[1],self.nnodes[2])
        end
        # Force-vector array (all nodes)
        self.Fext = zeros(self.ndofpernode,self.nnodes[1],self.nnodes[2])
        self.Fext_ds = Array{Array{Float64,3}}(undef,self.mesh.ndesignvars)
        for s_i = 1:self.mesh.ndesignvars
            self.Fext_ds[s_i] = zeros(self.ndofpernode,self.nnodes[1],self.nnodes[2])
        end
        # N-vector array (all nodes)
        self.Nvec = zeros(self.ndofpernode,self.nnodes[1],self.nnodes[2])
        self.Nvec_ds = Array{Array{Float64,3}}(undef,self.mesh.ndesignvars)
        for s_i = 1:self.mesh.ndesignvars
            self.Nvec_ds[s_i] = zeros(self.ndofpernode,self.nnodes[1],self.nnodes[2])
        end

        # Number of dof in the system (after BC are applied)
        self.dof = 0                      # to be set later

        # self.K = Array{Float64,2}()         # to be set later
        self.K = spzeros(1,1)  # to be set later
        self.K_ds = Array{SparseMatrixCSC{},1}(undef,0)  # to be set later
        self.K_el = Array{Array{Array{}},2}(undef,0,0) # to be set later
        self.K_ds_el = Array{Any}(undef,0)         # to be set later
        self.R = Array{Float64,2}(undef,0,0)         # to be set later
        self.old_R = Array{Float64,2}(undef,0,0)     # to be set later
        self.d_vec = Array{Float64,2}(undef,0,0)     # to be set later
        self.d_vec_ds = Array{Array{Float64}}(undef,0)  # to be set later
        self.del_d_vec = Array{Float64,2}(undef,0,0)     # to be set later
        self.Fext_vec = Array{Float64,2}(undef,0,0)      # to be set later
        self.Fext_vec_ds = Array{Array{Float64}}(undef,0) # to be set later
        self.Nvec_vec = Array{Float64,2}(undef,0,0)      # to be set later
        self.Nvec_vec_ds = Array{Array{Float64}}(undef,0)   # to be set later


        # ***********
        # *******
        # *****
        # **

        # For Gaussian Quadrature:
        # 1D:
        self.GQT1D = Array{GaussQuadTable}(undef,NumGaussPts(self.max_poly_degree))
        for k = 1:NumGaussPts(self.max_poly_degree)
            self.GQT1D[k] = GaussQuadTable(k)
        end
        #2D:
        self.GQT2D = Array{GaussQuadTable}(undef,NumGaussPts(self.max_poly_degree^2))
        for k = 1:NumGaussPts(self.max_poly_degree^2)
            self.GQT2D[k] = GaussQuadTable(k,k)
        end

        # Callback functions for boundary conditions
        self.dirichletConstraint = dirichletConstraint
        self.neumannBCForElementFace = neumannBCForElementFace
        self.neumannBCDsForElementFace = neumannBCDsForElementFace
        self.bodyForceForElement = bodyForceForElement

        # Callback functions for Fext_vec, N, consistent tangent
        self.FextForElement = FextForElement
        self.FextDsForElement = FextDsForElement
        self.NForElement = NForElement
        self.NDsForElement = NDsForElement
        self.consistentTangentForElement = consistentTangentForElement
        self.consistentTangentDsForElement = consistentTangentDsForElement

        return self
    end

end





"""
function GetStrainDispMatB(self::IGAGrid2D,e::Array{Int64},xi::Array{Float64})
===========

* self: Pointer to the grid
* e: Element index
* xi: The xi coordinate to be evaluated

# Returns:
* The Strain-Displacement Matrix B, dimension 3-by-2
  The B matrix for basis function (a1,a2) for element (e1,e2)
"""
function GetStrainDispMatB(self::IGAGrid2D,e::Array{Int64},a::Array{Int64},xi::Array{Float64})
    delb = delBasis(self.mesh,e,a,xi)
    B = zeros(3,2)
    B[1,1] = B[3,2] = delb[1]
    B[2,2] = B[3,1] = delb[2]
    return B
end





"""
function GetStrainDispMatB_ds(self::IGAGrid2D,e::Array{Int64},xi::Array{Float64})
===========

* self: Pointer to the grid
* e: Element index
* xi: The xi coordinate to be evaluated

# Returns:
* The Derivative w.r.t. the design parameter s of the
  Strain-Displacement Matrix B, dimension 3-by-2
  The B_ds matrix for basis function (a1,a2) for element (e1,e2), w.r.t. s[s_dir]
"""
function GetStrainDispMatB_ds(self::IGAGrid2D,e::Array{Int64},a::Array{Int64},xi::Array{Float64},s_dir::Int64)
    delb_ds = delBasis_ds(self.mesh,e,a,xi,s_dir)
    B_ds = zeros(3,2)
    B_ds[1,1] = B_ds[3,2] = delb_ds[1]
    B_ds[2,2] = B_ds[3,1] = delb_ds[2]
    return B_ds
end





"""
function ElementFaceIntegral(self::IGAGrid2D,
                             e::Array{Int64},
                             face::Tuple{Int64,Int64},
                             t_function::Function)
==============

# Parameters
* self: Pointer to the grid
* e: Element index
* t_function: Whatever function also exists inside the integral that must be evaluated (e.g. traction)
    Signature:  t_function(self::IGAGrid2D,e::Array{Int64},face::Tuple{Int64,Int64},xi::Array{Float64})

# Returns:
* Matrix of dimension nelpernode[1]-by-nelpernode[2];
* Matrix[a1,a2] is for node a1,a2
"""
function ElementFaceIntegral(self::IGAGrid2D,
                             e::Array{Int64},
                             face::Tuple{Int64,Int64},
                             t_function::Function)

    # face[1]: 1={xi=-1,xi=1} faces.  2={eta=-1,eta=1} faces
    # face[2]: 1={-1 face} 2={1 face}

    # ANS first,second index: The node (a1,a2)
    ANS = zeros(self.nnodesperel[1],self.nnodesperel[2])

    # face xi value
    face_xi = convert(Float64,(face[2]-1)*2-1)*(1.0-1e-14)

    if(face[1] == 1)

        f = function(params)
                basis_val = basis(self.mesh,e,[face_xi,params[1]])
                t_func_val = t_function(self,e,face,[face_xi,params[1]])
                return basis_val * t_func_val
            end
        jacobianf(params) = norm(Jacobian(self.mesh,e,[face_xi,params[1]])[:,2])
        gq_result = GaussQuadIntegrate(f,self.GQT1D[self.p[1]],jacobianf)
        ANS[:,:] += reshape(gq_result,size(ANS[:,:]))

    elseif(face[1] == 2)

        f = function(params)
                basis_val = basis(self.mesh,e,[params[1],face_xi])
                t_func_val = t_function(self,e,face,[params[1],face_xi])
                return basis_val * t_func_val
            end
        jacobianf = function(params)
                return norm(Jacobian(self.mesh,e,[params[1],face_xi])[:,1])
            end
        gq_result = GaussQuadIntegrate(f,self.GQT1D[self.p[2]],jacobianf)
        ANS[:,:] += reshape(gq_result,size(ANS[:,:]))

    end

    return ANS
end



"""
function ElementFaceIntegrals_ds(self::IGAGrid2D,e::Array{Int64}, s_dir::Int64)
==============

# Parameters
* self: Pointer to the grid
* e: Element index
* face[1]: 1={xi=-1,xi=1} faces.  2={eta=-1,eta=1} faces
* face[2]: 1={-1 face} 2={1 face}
* t_function: Whatever function also exists inside the integral that must be evaluated (e.g. traction)
    Signature:  t_function(self::IGAGrid2D,e::Array{Int64},face::Tuple{Int64,Int64},xi::Array{Float64})
* t_function_ds: Derivative of the t_funciton w.r.t. design paramter s
    Signature:  t_function_ds(self::IGAGrid2D,e::Array{Int64},face::Tuple{Int64,Int64},xi::Array{Float64},s_dir::Int64)

# Returns:
* Matrix of dimension dim_p-by-2-by-nelpernode[1]-by-nelpernode[2];
* Matrix[dim_p,1,a1,a2] is along face xi[dim_s] = -1, for node a1,a2
* Matrix[dim_p,2,a1,a2] is along face xi[dim_s] = 1, for node a1,a2
"""
function ElementFaceIntegral_ds(self::IGAGrid2D,
                                 e::Array{Int64},
                                 face::Tuple{Int64,Int64},
                                 t_function::Function,
                                 t_function_ds::Function,
                                 s_dir::Int64)

    # face[1]: 1={xi=-1,xi=1} faces.  2={eta=-1,eta=1} faces
    # face[2]: 1={-1 face} 2={1 face}

    # ANS first,second index: The node (a1,a2)
    ANS_ds = zeros(self.nnodesperel[1],self.nnodesperel[2])

    # face xi value
    face_xi = convert(Float64,(face[2]-1)*2-1)*(1.0-1e-14)

    # First part (d/ds of basis)
    if(face[1] == 1)
        f = function(params)
                basis_val = basis_ds(self.mesh,e,[face_xi,params[1]],s_dir)
                t_func_val = t_function(self,e,face,[face_xi,params[1]])
                return basis_val * t_func_val
            end
        jacobianf(params) = norm(Jacobian(self.mesh,e,[face_xi,params[1]])[:,2])
        gq_result = GaussQuadIntegrate(f,self.GQT1D[self.p[1]],jacobianf)
        ANS_ds[:,:] += reshape(gq_result,size(ANS_ds[:,:]))

    elseif(face[1] == 2)
        f = function(params)
                basis_val = basis_ds(self.mesh,e,[params[1],face_xi],s_dir)
                t_func_val = t_function(self,e,face,[params[1],face_xi])
                return basis_val * t_func_val
            end
        jacobianf = function(params)
                return norm(Jacobian(self.mesh,e,[params[1],face_xi])[:,1])
            end
        gq_result = GaussQuadIntegrate(f,self.GQT1D[self.p[2]],jacobianf)
        ANS_ds[:,:] += reshape(gq_result,size(ANS_ds[:,:]))
    end

    # Second part (d/ds of t_function)
    if(face[1] == 1)
        f = function(params)
                basis_val = basis(self.mesh,e,[face_xi,params[1]])
                t_func_val = t_function_ds(self,e,face,[face_xi,params[1]],s_dir)
                return basis_val * t_func_val
            end
        jacobianf = function(params)
                return norm(Jacobian(self.mesh,e,[face_xi,params[1]])[:,2])
            end
        gq_result = GaussQuadIntegrate(f,self.GQT1D[self.p[1]],jacobianf)
        ANS_ds[:,:] += reshape(gq_result,size(ANS_ds[:,:]))

    elseif(face[1] == 2)
        f = function(params)
                basis_val = basis(self.mesh,e,[params[1],face_xi])
                t_func_val = t_function_ds(self,e,face,[params[1],face_xi],s_dir)
                return basis_val * t_func_val
            end
        jacobianf = function(params)
                return norm(Jacobian(self.mesh,e,[params[1],face_xi])[:,1])
            end
        gq_result = GaussQuadIntegrate(f,self.GQT1D[self.p[2]],jacobianf)
        ANS_ds[:,:] += reshape(gq_result,size(ANS_ds[:,:]))
    end

    #Third part (d/ds of Jacobian)
    if(face[1] == 1)
        f = function(params)
                basis_val = basis(self.mesh,e,[face_xi,params[1]])
                t_func_val = t_function(self,e,face,[face_xi,params[1]])
                return basis_val * t_func_val
            end
        jacobianf = function(params)
            J_vec = Jacobian(self.mesh,e,[face_xi,params[1]])[:,2]
            J_ds_vec = Jacobian_ds(self.mesh,e,[face_xi,params[1]],s_dir)[:,2]
            return dot(J_vec[:],J_ds_vec[:])/norm(J_vec)
        end
        gq_result = GaussQuadIntegrate(f,self.GQT1D[self.p[1]],jacobianf)
        ANS_ds[:,:] += reshape(gq_result,size(ANS_ds[:,:]))

    elseif(face[1] == 2)
        f = function(params)
                basis_val = basis(self.mesh,e,[params[1],face_xi])
                t_func_val = t_function(self,e,face,[params[1],face_xi])
                return basis_val * t_func_val
            end
        jacobianf = function(params)
            J_vec = Jacobian(self.mesh,e,[params[1],face_xi])[:,1]
            J_ds_vec = Jacobian_ds(self.mesh,e,[params[1],face_xi],s_dir)[:,1]
            return dot(J_vec[:],J_ds_vec[:])/norm(J_vec)
        end
        gq_result = GaussQuadIntegrate(f,self.GQT1D[self.p[2]],jacobianf)
        ANS_ds[:,:] += reshape(gq_result,size(ANS_ds[:,:]))
    end

    return ANS_ds
end







"""
function processBoundaryConditions(self::IGAGrid2D)
===========

# This function does the following:
1. Assembles the "ID" array, which contains a table which maps
       the nodal index to its corresponding dof indices.
2. Determines the number of degrees of freedom.
3. Sets the constrained nodal displacements, and zeros-out the rest.

# Parameters
* self: Pointer to the grid

# Returns:
* Reference to the ID array, the number of dof, and the nodal d array
"""
function processBoundaryConditions(self::IGAGrid2D)

    # --------------------------------------
    # Building the ID array, Setting BC info
    # --------------------------------------

    # Step 1.1: Init the ID array, set all values to 1
    # Step 1.2: Also, init the solution vector d
    self.ID = ones(Int64,self.ndofpernode,self.nnodes[1],self.nnodes[2])
    fill!(self.d,0.0)

    # Step 2.1: Mark the constrained dof by looping over elements,
    #         and settting the ID array to 0 for constrained elements
    # Step 2.2: Also, init the displacement BC in the final displacement vector d.
    for e1 = 1:self.nelements[1]
        for e2 = 1:self.nelements[2]
            for a1 = 1:self.nnodesperel[1]
                for a2 = 1:self.nnodesperel[2]
                    for dofi = 1:self.ndofpernode
                        hasDisplacement,d_val = self.dirichletConstraint(self,[e1,e2],[a1,a2],dofi)
                        if( hasDisplacement )
                            self.ID[dofi,self.mesh.IEN[1][a1,e1],self.mesh.IEN[2][a2,e2]] = 0
                            self.d[dofi,self.mesh.IEN[1][a1,e1],self.mesh.IEN[2][a2,e2]] = d_val
                        end
                    end
                end
            end
        end
    end

    # Step 3: Specify the dof index for each non-constrained dof.
    dof_count = 0
    for n2 = 1:self.nnodes[2]
        for n1 = 1:self.nnodes[1]
            for dofi = 1:self.ndofpernode
                if( self.ID[dofi,n1,n2] == 1 )
                    dof_count += 1
                    self.ID[dofi,n1,n2] = dof_count
                end
            end
        end
    end

    # Step 4: Set the number of degrees of freedom
    self.dof = dof_count

    return self.ID,self.dof,self.d

end






"""
function ElementDofLookup(self::IGAGrid2D,e::Array{Int64},a::Array{Int64},dofi::Int64)
==================
Sometimes called "LM array".

# Parameters:
* e: The element index
* a: The node of the element
* dofi: The index of the degree of freedom (1=x, 2=y)

# Returns:
* The index of the degree of freedom of a node in an element
"""
function ElementDofLookup(self::IGAGrid2D,e::Array{Int64},a::Array{Int64},dofi::Int64)
    return self.ID[dofi,self.mesh.IEN[1][a[1],e[1]],self.mesh.IEN[2][a[2],e[2]]]
end




"""
function updateD(self::IGAGrid2D)
=======

Copies the values that are degrees of freedom (in 'd_vec') into
the data structure that includes all values, including the ones
that are constrained (in 'd')
"""
function updateD(self::IGAGrid2D,d::Array{Float64,3},d_vec::Array{Float64})
    for e2 = 1:self.nelements[2]
        for e1 = 1:self.nelements[1]
            for a2 = 1:self.nnodesperel[2]
                for a1 = 1:self.nnodesperel[1]
                    for dofi = 1:self.ndofpernode
                        glob_dof_i = ElementDofLookup(self,[e1,e2],[a1,a2],dofi)
                        if(glob_dof_i > 0)
                            d[dofi,self.mesh.IEN[1][a1,e1],self.mesh.IEN[2][a2,e2]] = d_vec[glob_dof_i]
                        end
                    end
                end
            end
        end
    end
end









"""
function assembleGlobalVector(self::IGAGrid2D,
                              vector::Array{Float64},
                              VecForElement::Function)
=======
Assembles a global vector using function 'VecForElement' (2D)

# Parameters:
* self: Pointer to the grid
* vector: Pointer to the vector to be populated.
* VecForElement: A function with signature (self::IGAGrid2D,e::Array{Int64})
                 for grid "self" and element index "e".
"""
function assembleGlobalVector(self::IGAGrid2D,
                              vector::Array{Float64},
                              complete_vector::Array{Float64,3},
                              VecForElement::Function)
    # Assumes the "vector" array already exists

    # Initialize the residual array
    fill!(vector,0.0)
    fill!(complete_vector,0.0)

    # Loop over each element
    for e2 = 1:self.nelements[2]
        for e1 = 1:self.nelements[1]
            if(self.runtimeoutput) println("assembleGlobalVector Element ",[e1,e2]) end
            begin
                # (1) Call "VecForElement"
                # (2) Add the result into the global value.
                vec_el = VecForElement(self,[e1,e2])
                for a2 = 1:self.nnodesperel[2]
                    for a1 = 1:self.nnodesperel[1]
                        for dofi = 1:self.ndofpernode
                            glob_dof_i = ElementDofLookup(self,[e1,e2],[a1,a2],dofi)
                            if(glob_dof_i > 0)
                                vector[glob_dof_i] += vec_el[dofi,a1,a2]
                            end
                            complete_vector[dofi,self.mesh.IEN[1][a1,e1],self.mesh.IEN[2][a2,e2]] += vec_el[dofi,a1,a2]
                        end
                    end
                end
            end
        end
    end

    return
end






"""
function assembleGlobalFext(self::IGAGrid2D)
=========
Assembles the Global Fext_vec vector.
"""
function assembleGlobalFext(self::IGAGrid2D)
    # Assumes the "Fext_vec" array already exists
    if(self.runtimeoutput) println("assembleGlobalFext") end
    fill!(self.Fext_vec,0.0)
    assembleGlobalVector(self,self.Fext_vec,self.Fext,self.FextForElement)

    # Assumes the "Fext_vec_ds" array already exists
    if(self.runtimeoutput) println("assembleGlobalFext_ds") end
    for s_i = 1:self.mesh.ndesignvars
        FdsForEl(self::IGAGrid2D,e::Array{Int64}) = self.FextDsForElement(self,e,s_i)
        fill!(self.Fext_vec_ds[s_i],0.0)
        assembleGlobalVector(self,self.Fext_vec_ds[s_i],self.Fext_ds[s_i],FdsForEl)
    end

    return
end





"""
function assembleGlobalN(self::IGAGrid2D)
=========
Assembles the global N vector
"""
function assembleGlobalN(self::IGAGrid2D)
    # Assumes the "Nvec_vec" array already exists
    # Assumes "K_el" array of matrices already constructed
    if(self.runtimeoutput) println("assembleGlobalN") end
    fill!(self.Nvec_vec,0.0)
    assembleGlobalVector(self,self.Nvec_vec,self.Nvec,self.NForElement)

    # Assumes the "Nvec_vec_ds" array already exists
    # Assumes "K_ds_el" array of matrices already constructed
    if(self.runtimeoutput) println("assembleGlobalN_ds") end
    for s_i = 1:self.mesh.ndesignvars
        NdsForEl(self::IGAGrid2D,e::Array{Int64}) = self.NDsForElement(self,e,s_i)
        fill!(self.Nvec_vec_ds[s_i],0.0)
        assembleGlobalVector(self,self.Nvec_vec_ds[s_i],self.Nvec_ds[s_i],NdsForEl)
    end

    return
end







"""
function assembleGlobalResidual(self::IGAGrid2D)
=====
Assembles the Global Residual vector (2D)
"""
function assembleGlobalResidual(self::IGAGrid2D)
    # Assumes the "R" array already exists

    # Initialize the residual array
    fill!(self.R,0.0)

    assembleGlobalFext(self)
    assembleGlobalN(self)

    # Use a fraction of the actual Fext_vec, depending on the load-step
    loadloopmult = 1.0
    if(self.nloadsteps > 0)
        loadloopmult = (self.load_step_i/self.nloadsteps)
    end

    self.R = self.Fext_vec*loadloopmult - self.Nvec_vec

    return self.R
end




"""
function assembleGlobalConsistentTangent(self::IGAGrid2D)
=====
Assembles the Global Consistent Tangent vector (2D)
"""
function assembleGlobalConsistentTangent(self::IGAGrid2D)
    # Assumes the "K" matrix already exists

    if(self.runtimeoutput) println("assembleGlobalConsistentTangent") end

    # Initialize the K matrix
    fill!(self.K,0.0)
    for s_i = 1:self.mesh.ndesignvars
        fill!(self.K_ds[s_i],0.0)
    end

    # Loop over each of element
    for e2 = 1:self.nelements[2]
        for e1 = 1:self.nelements[1]
            if(self.runtimeoutput) println("assembleGlobalConsistentTangent element: ",[e1,e2]) end
            begin
                self.K_el[e1,e2] = self.consistentTangentForElement(self,[e1,e2])
                for s_i = 1:self.mesh.ndesignvars
                    self.K_ds_el[s_i][e1,e2] = self.consistentTangentDsForElement(self,[e1,e2],s_i)
                end
            end
            begin
                for a2i = 1:self.nnodesperel[2]
                    for a1i = 1:self.nnodesperel[1]
                        for a2j = 1:self.nnodesperel[2]
                            for a1j = 1:self.nnodesperel[1]
                                for nd_dof_j = 1:self.ndofpernode
                                    for nd_dof_i = 1:self.ndofpernode
                                        k_i = ElementDofLookup(self,[e1,e2],[a1i,a2i],nd_dof_i)
                                        k_j = ElementDofLookup(self,[e1,e2],[a1j,a2j],nd_dof_j)
                                        if(k_i > 0 && k_j > 0)
                                            self.K[k_i,k_j] += self.K_el[e1,e2][a1i,a2i,a1j,a2j][nd_dof_i,nd_dof_j]
                                            for s_i = 1:self.mesh.ndesignvars
                                                self.K_ds[s_i][k_i,k_j] = self.K_ds_el[s_i][e1,e2][a1i,a2i,a1j,a2j][nd_dof_i,nd_dof_j]
                                            end
                                        end
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end
    end

    return self.K
end





"""
function Solve(self::IGAGrid2D)
======
Solve the IGA system using Newton-Raphson (2D)
"""
function Solve(self::IGAGrid2D)

    if(self.runtimeoutput) println("") end

    # Zeroing-out the "d" array
    fill!(self.d,0.0)
    for s_i = 1:self.mesh.ndesignvars
        fill!(self.d_ds[s_i],0.0)
    end

    # Process the Boundary Conditions, determining global number of dof
    # Also, sets the values of self.d on boundaries.
    #println("processing boundary conditions..")
    processBoundaryConditions(self)

    # Finish initializing all the other important arrays
    begin
        self.d_vec = zeros(self.dof)
        self.d_vec_ds = Array{Array{Float64}}(undef,self.mesh.ndesignvars)
        for s_i = 1:self.mesh.ndesignvars
            self.d_vec_ds[s_i] = zeros(self.dof)
        end
        self.del_d_vec = zeros(self.dof)
        self.R = zeros(self.dof)
        self.old_R = zeros(self.dof)
        self.K = spzeros(self.dof,self.dof)
        self.K_ds = Array{SparseMatrixCSC{},1}(undef,self.mesh.ndesignvars)
        for s_dir_i = 1:self.mesh.ndesignvars
            self.K_ds[s_dir_i] = spzeros(self.dof,self.dof)
        end
        self.K_el = Array{Array{Array{}},2}(undef,self.nelements[1],self.nelements[2])
        self.K_ds_el = Array{Any}(undef,self.mesh.ndesignvars)
        for s_i = 1:self.mesh.ndesignvars
            self.K_ds_el[s_i] = Array{Array{Array{}},2}(undef,self.nelements[1],self.nelements[2])
        end
        self.Nvec_vec = zeros(self.dof)
        self.Nvec_vec_ds = Array{Array{Float64}}(undef,self.mesh.ndesignvars)
        for s_dir_i = 1:self.mesh.ndesignvars
            self.Nvec_vec_ds[s_dir_i] = zeros(self.dof)
        end
        self.Fext_vec = zeros(self.dof)
        self.Fext_vec_ds = Array{Array{Float64}}(undef,self.mesh.ndesignvars)
        for s_dir_i = 1:self.mesh.ndesignvars
            self.Fext_vec_ds[s_dir_i] = zeros(self.dof)
        end
        GC.gc()
    end

    MAX_corrloops = self.max_correction_loops

    # Load-step loop
    begin
        self.load_step_i = 0
        while(self.load_step_i <= self.nloadsteps)

            if(self.runtimeoutput) println("Load step: ",self.load_step_i," out of ",self.nloadsteps) end

            #Copy the most recent 'd_vec' values into 'd'.
            updateD(self,self.d,self.d_vec)

            # Initialize the Global Consistent Tangent (K)
            assembleGlobalConsistentTangent(self)

            # Initialize the residual (R)
            assembleGlobalResidual(self)
            if(self.runtimeoutput) println("     Correction step: ","0", "   R:     ",maximum(abs.(self.R))) end

            # Correction loop.
            corrloop_k = 1
            while(true)

                # Compute new del_d_vec
                self.del_d_vec[:] = self.K\self.R     # run = rise/(rise/run)
                # Update displacement guess
                self.d_vec[:] += self.del_d_vec[:]

                # Copy the most recent 'd_vec' values into 'd'.
                updateD(self,self.d,self.d_vec)

                # Assemble the global consistent tangent (K)
                assembleGlobalConsistentTangent(self)
                # Update the residual
                assembleGlobalResidual(self)

                if(self.runtimeoutput) println("     Correction step: ",corrloop_k, "   R:     ",maximum(abs.(self.R))) end

                # Break out of loop if residual is small enough.
                if(maximum(abs.(self.R)) <= self.epsilon || corrloop_k >= MAX_corrloops)
                    break
                end

                corrloop_k += 1
            end
            self.load_step_i += 1
        end
    end

    # Now, solve for derivative of d w.r.t. s.
    # "Direct" method
    begin
        for s_i = 1:self.mesh.ndesignvars
            self.d_vec_ds[s_i] = self.K\(self.Fext_vec_ds[s_i] - self.Nvec_vec_ds[s_i])
            updateD(self,self.d_ds[s_i],self.d_vec_ds[s_i])
        end
    end

    if(self.runtimeoutput) println("Solve done!") end

end






"""
function retrieveQuantityForElement(self::IGAGrid2D,e::Array{Float64},quantity::Array{Float64})
===========

Returns the values for just the element, shape ndof-by-nnodesperel[1]-nnodesperel[2]
"""
function retrieveQuantityForElement(self::IGAGrid2D,e::Array{Int64},dof::Int64,quantity::Array{Float64})
    e1 = e[1]
    e2 = e[2]
    return reshape(
        quantity[dof,
                 self.mesh.IEN[1][1,e1]:self.mesh.IEN[1][end,e1],
                 self.mesh.IEN[2][1,e2]:self.mesh.IEN[2][end,e2]],
           self.nnodesperel[1],self.nnodesperel[2])
end








"""
function compute_volume(self::IGAGrid2D)
=====

Computes the integral _int 1 d_sigma
"""
function compute_volume(self::IGAGrid2D)
    # Number of gauss points
    ngp = NumGaussPts(self.max_poly_degree^2)
    volume = 0.0
    # Loop over elements
    for e1 = 1:self.nelements[1]
        for e2 = 1:self.nelements[2]
            jacobianf = function(params)
                jac = Jacobian(self.mesh,[e1,e2],params)
                return det(jac)
            end
            f = function(params)
                R = basis(self.mesh,[e1,e2],params)
                val = 0.0
                for i = 1:length(R)
                    val += R[i]
                end
                return val
            end
            # Integrate over the element
            volume += GaussQuadIntegrate(f,self.GQT2D[ngp],jacobianf)
        end
    end
    return volume
end


"""
function compute_volume_ds(self::IGAGrid2D)
=====

Computes the derivative of the material volume
with respect to the design paramters (s)
"""
function compute_volume_ds(self::IGAGrid2D)
    # Number of gauss points
    ngp = NumGaussPts(self.max_poly_degree^2)
    volume_ds = zeros(self.mesh.ndesignvars)
    # Loop over elements
    for e1 = 1:self.nelements[1]
        for e2 = 1:self.nelements[2]

            jacobianf = function(params)
                jac = Jacobian(self.mesh,[e1,e2],params)
                return det(jac)
            end
            f = function(params)
                R = basis(self.mesh,[e1,e2],params)
                val = 0.0
                for i = 1:length(R)
                    val += R[i]
                end
                return val
            end

            for s_dir = 1:self.mesh.ndesignvars

                jacobian_ds_f = function(params)
                    # det(J)_ds = det(J)*tr(J_inv * J_ds)
                    J = Jacobian(self.mesh,[e1,e2],params)
                    J_ds = Jacobian_ds(self.mesh,[e1,e2],params,s_dir)
                    J_inv = inv(J)
                    return det(J)*tr(J_inv*J_ds)
                end
                f_ds = function(params)
                    R_ds = basis_ds(self.mesh,[e1,e2],params,s_dir)
                    val = 0.0
                    for i = 1:length(R_ds)
                        val += R_ds[i]
                    end
                    return val
                end

                # Integrate over the element
                volume_ds[s_dir] += GaussQuadIntegrate(f,self.GQT2D[ngp],jacobian_ds_f)
                volume_ds[s_dir] += GaussQuadIntegrate(f_ds,self.GQT2D[ngp],jacobianf)
            end
        end
    end
    return volume_ds
end









"""
function solution(self::IGAGrid2D,dof::Int64,samplesperel::Array{Int64})
=====

# Returns:
* X: The X spacial coordinate values of each data point returned
* Y: The Y spacial coordinate values of each data point returned
* F: The function values at each point X,Y.
"""
function solution(self::IGAGrid2D,dof::Int64,samplesperel::Array{Int64},quantity=nothing)
    if(quantity == nothing)
        quantity = self.d
    end
    F = zeros(samplesperel[1]*self.nelements[1],samplesperel[2]*self.nelements[2])
    X = zeros(samplesperel[1]*self.nelements[1],samplesperel[2]*self.nelements[2])
    Y = zeros(samplesperel[1]*self.nelements[1],samplesperel[2]*self.nelements[2])
    val = 0.0
    # Loop over elements
    for e1 = 1:self.nelements[1]
        for e2 = 1:self.nelements[2]

            cpmin1 = self.mesh.IEN[1][1,e1]
            cpmax1 = self.mesh.IEN[1][end,e1]
            cpmin2 = self.mesh.IEN[2][1,e2]
            cpmax2 = self.mesh.IEN[2][end,e2]
            cpsup = Array[collect(cpmin1:cpmax1),collect(cpmin2:cpmax2)]

            # Loop over sample points
            for sp1 = 1:samplesperel[1]
                for sp2 = 1:samplesperel[2]
                    #We aim for the centers of each element segment.
                    #[-h--h-][-h--h-][-h--h-] for npernode=3
                    h1 = 2.0/(2*samplesperel[1])
                    h2 = 2.0/(2*samplesperel[2])

                    #[  *    *    *  ] <-- Those are the sample points, for npernode=3
                    xi1 = -1.0 + h1 + 2*h1*(sp1-1)
                    xi2 = -1.0 + h2 + 2*h2*(sp2-1)

                    # compute the value
                    N = basis(self.mesh,[e1,e2],[xi1,xi2])
                    d0 = retrieveQuantityForElement(self,[e1,e2],dof,quantity)
                    val = dot(N[:],d0[:])

                    #set the value in the array
                    xy = domain_X(self.mesh,[e1,e2],reshape([xi1,xi2],2,1))
                    i1 = (e1-1)*samplesperel[1] + sp1
                    i2 = (e2-1)*samplesperel[2] + sp2
                    X[i1,i2] = xy[1]
                    Y[i1,i2] = xy[2]
                    F[i1,i2] = val
                end
            end
        end
    end
    return X,Y,F
end











end ## module SSIGA2D
